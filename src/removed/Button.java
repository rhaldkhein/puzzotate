package removed;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

public class Button extends SpriteButton {
	public Button(Sprite sprite) {
		super(sprite);
	}

	@Override
	public void onTouchDown(InputEvent event,
							float x,
							float y,
							int pointer,
							int button) {
		spriteUp.setScale(0.9f);
	}

	public void onTouchUp(	InputEvent event,
							float x,
							float y,
							int pointer,
							int button) {
		spriteUp.setScale(1);
	}
}
