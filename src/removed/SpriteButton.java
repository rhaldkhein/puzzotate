package removed;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;

public class SpriteButton extends Actor {

	protected boolean isPressed;
	protected boolean isDisabled;
	protected Sprite spriteUp;
	protected Sprite spriteDown;

	public SpriteButton() {
	}

	public SpriteButton(Sprite spriteUp) {
		this(spriteUp, null);
	}

	public SpriteButton(Sprite spriteUp, Sprite spriteDown) {
		this.spriteUp = spriteUp;
		this.spriteDown = spriteDown;
		isDisabled = false;
		setWidth(spriteUp.getWidth());
		setHeight(spriteUp.getHeight());
		setX(spriteUp.getX());
		setY(spriteUp.getY());
		// spriteDown.setScale(2);
		setListener();
	}

	public void setSpriteUp(Sprite spriteUp) {
		this.spriteUp = spriteUp;
	}

	public void setSpriteDown(Sprite spriteDown) {
		this.spriteDown = spriteDown;
	}

	public Sprite getSpriteUp() {
		return spriteUp;
	}

	public Sprite getSpriteDown() {
		return spriteDown;
	}
	
	public void update() {
		Gdx.app.debug("tag", "" + getX() + " | " + getY());
	}
	
	@Override
	public void setX(float x) {
		super.setX(x);
		spriteUp.setX(x);
		if (spriteDown != null)
			spriteDown.setX(x);
	}

	@Override
	public void setY(float y) {
		super.setY(y);
		spriteUp.setY(y);
		if (spriteDown != null)
			spriteDown.setY(y);
	}

	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		spriteUp.setSize(width, spriteUp.getHeight());
		spriteUp.setOriginCenter();
		if (spriteDown != null)
			spriteDown.setSize(width, spriteDown.getHeight());
	}

	@Override
	public void setHeight(float height) {
		super.setHeight(height);
		spriteUp.setSize(spriteUp.getWidth(), height);
		spriteUp.setOriginCenter();
		if (spriteDown != null)
			spriteDown.setSize(spriteDown.getWidth(), height);
	}

	private void setListener() {
		addListener(new InputListener() {
			@Override
			public boolean touchDown(	InputEvent event,
										float x,
										float y,
										int pointer,
										int button) {
				isPressed = true;
				onTouchDown(event, x, y, pointer, button);
				return true;
			}

			@Override
			public void touchUp(InputEvent event,
								float x,
								float y,
								int pointer,
								int button) {
				isPressed = false;
				onTouchUp(event, x, y, pointer, button);
			}
		});
	}

	public void onTouchDown(InputEvent event,
							float x,
							float y,
							int pointer,
							int button) {

	}

	public void onTouchUp(	InputEvent event,
							float x,
							float y,
							int pointer,
							int button) {

	}

	public void setDisabled(boolean disabled) {
		isDisabled = disabled;
	}

	@Override
	public void draw(Batch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);
		spriteUp.draw(batch, parentAlpha);
	}
}
