package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Panel;

public class TestView extends View {

	public TestView() {

	}

	@Override
	public void create() {
		super.create();

		AtlasManager atlas = Resource.getAtlasManager();
		// Array<TextureRegion> arrTextures = new Array<TextureRegion>(true, 9);
		TextureRegion[] arrTextures = new TextureRegion[9];
		arrTextures[0] = atlas.getRegion("p-default-c1");
		arrTextures[1] = atlas.getRegion("p-default-c2");
		arrTextures[2] = atlas.getRegion("p-default-c3");
		arrTextures[3] = atlas.getRegion("p-default-c4");
		arrTextures[4] = atlas.getRegion("p-default-e1");
		arrTextures[5] = atlas.getRegion("p-default-e1");
		arrTextures[6] = atlas.getRegion("p-default-e1");
		arrTextures[7] = atlas.getRegion("p-default-e1");
		arrTextures[8] = atlas.getRegion("p-default-e1");
		// PanelFlex.defaultTextureRegions = arrTextures;
		Panel.addSkin("default", arrTextures);
		
		Panel pf = new Panel();
		pf.setSize(200, 200);
		// pf.setScale(2);
		pf.setX(100);
		pf.setY(100);
		addActor(pf);
		pf.setAlpha(0.5f);
		
	}
}
