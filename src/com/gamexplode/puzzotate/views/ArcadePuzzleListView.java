package com.gamexplode.puzzotate.views;

import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.puzzotate.data.PuzzleManager;

public class ArcadePuzzleListView extends PuzzleListView {

	private Settings pSettings;

	public ArcadePuzzleListView() {
		this(0);
	}

	public ArcadePuzzleListView(int currentPuzzleId) {
		super(PuzzleManager.getPuzzles(PuzzleManager.ARCADE), currentPuzzleId);
		pSettings = Resource.getSettings(Config.SETTINGS_ARCADE);
	}

	@Override
	public void create() {
		super.create();
		setListName("Arcade");
	}

	@Override
	protected boolean getLock(int id) {
		
		// If premium level 2
		if (Resource.getSettings().getInteger("up_pr") >= 2) {
			return false;
		}

		if (id == 1) {
			if (pSettings.getBoolean("ar_ul" + id) != true) {
				pSettings.setBoolean("ar_ul" + id, true);
			}
			return false;
		}
		return !pSettings.getBoolean("ar_ul" + id);
	}

	@Override
	protected int getStar(int id) {
		return pSettings.getInteger("ar_s" + id);
	}

	@Override
	protected String getScoreLabel(int id) {
		int best = pSettings.getInteger("ar_b" + id);
		return best > 0 ? String.valueOf(best) : "";
	}

	@Override
	protected void itemClicked(int id) {
		getScene().setView(new ArcadePuzzleView(getPuzzles().getPuzzle(id)));
	}

}
