package com.gamexplode.puzzotate.views;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.core.Utils;
import com.gamexplode.core.facebook.IFacebook;
import com.gamexplode.core.gameservice.GameService;
import com.gamexplode.core.gameservice.Response;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.PanelTable;
import com.gamexplode.puzzotate.controls.ErrorDisplay;
import com.gamexplode.puzzotate.controls.LeaderList;
import com.gamexplode.puzzotate.controls.LeaderListItem;
import com.gamexplode.puzzotate.controls.Loading;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.ExtremePuzzles;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.data.Puzzles;

public class PuzzleScoreView extends View {

	private static final int GROUP_FRIENDS = 1001;
	private static final int GROUP_GLOBAL = 1002;

	private static final String FB_APP_ID = "447699735377359";

	private PuzzleData origPuzzle;
	private PuzzleData currentPuzzle;
	private int currentGroup;
	private LeaderList leaderList;
	private Label labelTopLeft;
	private Label labelTopRight;
	private int panelWidth;
	private int itemHeight;
	private Loading loading;
	private Group groupMain;
	private ErrorDisplay errDisplay;
	private GameService gameService;
	private TopBarControl tbc;
	private boolean requesting;
	private Puzzles puzzles;

	public PuzzleScoreView(PuzzleData origPuzzle) {
		this(origPuzzle, null, 0);
	}

	public PuzzleScoreView(PuzzleData origPuzzle, PuzzleData showPuzzle, int showGroup) {
		this.origPuzzle = origPuzzle;
		this.currentPuzzle = showPuzzle == null ? this.origPuzzle : showPuzzle;
		this.currentGroup = showGroup > 0 ? showGroup : GROUP_FRIENDS;
		requesting = false;
		itemHeight = 80;
	}

	@Override
	public void create() {
		super.create();

		// Get puzzles
		puzzles = PuzzleManager.getPuzzles(PuzzleManager.EXTREME);

		// Before any thing else, check if user have already used login button
		Settings settings = Resource.getSettings();
		if (!settings.contains("fbpermitted")) {
			// Redirect to login view
			getScene().setView(new LoginView(origPuzzle));
			return;
		}

		// Proceed to leaderboard
		gameService = Resource.getGameService();

		setupTopBarControl();
		Table tableMainLayout = new Table();
		tableMainLayout.setWidth(Config.WIDTH);
		tableMainLayout.setHeight(Config.HEIGHT);
		addActor(tableMainLayout);

		PanelTable panelMain = new PanelTable();
		panelWidth = 640;
		panelMain.setSize(panelWidth, 960);
		tableMainLayout.add(panelMain);
		panelMain.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				getScene().setView(new PuzzleScoreView(origPuzzle, currentPuzzle, currentGroup));
			}
		});

		groupMain = new Group();
		groupMain.setSize(panelMain.getContainerWidth(), panelMain.getContainerHeight());
		panelMain.setActor(groupMain);

		LabelStyle lsWhiteOutlinedLarge = new LabelStyle();
		lsWhiteOutlinedLarge.font = Resource.getFont("white-outlined-large");
		lsWhiteOutlinedLarge.font.getData().setScale(1);

		LabelStyle lsRedOutline = new LabelStyle();
		lsRedOutline.font = Resource.getFont("red-outlined-normal");
		lsRedOutline.font.getData().setScale(1);

		labelTopLeft = new Label("Friends", lsWhiteOutlinedLarge);
		labelTopLeft.setFontScale(0.8f);
		labelTopLeft.setWidth(groupMain.getWidth() / 2);
		labelTopLeft.setHeight(groupMain.getHeight() * 0.07f);
		labelTopLeft.setX(0);
		labelTopLeft.setY(groupMain.getHeight() - (labelTopLeft.getHeight() * 0.8f));

		labelTopRight = new Label("Puzzle " + origPuzzle.id, lsRedOutline);
		labelTopRight.setWidth(groupMain.getWidth() / 2);
		labelTopRight.setHeight(groupMain.getHeight() * 0.07f);
		labelTopRight.setAlignment(Align.right);
		labelTopRight.setFontScale(1f);
		labelTopRight.setX(groupMain.getWidth() - labelTopLeft.getWidth());
		labelTopRight.setY(groupMain.getHeight() - (labelTopLeft.getHeight() * 0.8f));

		groupMain.addActor(labelTopLeft);
		groupMain.addActor(labelTopRight);

		leaderList = new LeaderList();
		leaderList.setSize(groupMain.getWidth(), groupMain.getHeight() - labelTopLeft.getHeight());
		groupMain.addActor(leaderList);

		loading = new Loading();
		loading.setScale(0.7f);
		loading.setX((groupMain.getWidth() * 0.52f) - (loading.getWidth() / 2));
		loading.setY(groupMain.getHeight() * 0.55f);
		loading.setVisible(false);
		groupMain.addActor(loading);

		errDisplay = new ErrorDisplay();
		errDisplay.setX((groupMain.getWidth() * 0.5f) - (errDisplay.getWidth() / 2));
		errDisplay.setY(groupMain.getHeight() * 0.55f);
		errDisplay.setVisible(false);
		groupMain.addActor(errDisplay);
		// errDisplay.addListener(new ClickListener() {
		// @Override
		// public void clicked(InputEvent event, float x, float y) {
		// super.clicked(event, x, y);
		// getScene().setView(new PuzzleScoreView(origPuzzle, currentPuzzle, currentGroup));
		// }
		// });

		updateLabels();

		// Let the view construct its parts and then open game service
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				if (!gameService.isDone()) {
					showLoading("Connecting...");
					gameService.open(FB_APP_ID, (IFacebook) Resource.getInterface("facebook"), new Callback<String>() {
						@Override
						public void apply(String status) {
							// Gdx.app.debug("PuzzleScoreView", "Status | " + status);
							// OPENED for Android, Open for iOS
							if (status == "OPENED") {
								gameService.login(new Callback<Boolean>() {
									@Override
									public void apply(Boolean t) {
										// Gdx.app.debug("PuzzleScoreView", "Login | " + t);
										if (t) {
											gameService.fetchMe(new Callback<Integer>() {
												@Override
												public void apply(Integer acc) {
													// Gdx.app.debug("PuzzleScoreView", "Fetch Me | " + acc);
													if (acc == GameService.ACCOUNT_REPLACE) {
														// Replace Account
														replaceScores(new Callback<Boolean>() {
															@Override
															public void apply(Boolean success) {
																if (success) {
																	gameService.fetchFriends(new Callback<Boolean>() {
																		@Override
																		public void apply(Boolean success) {
																			if (success) {
																				gameServiceDone();
																			} else {
																				// Fetch friends error
																				showError("Sorry, can't connect!");
																			}
																		}
																	});
																} else {
																	// Score replacement error
																	showError("Sorry, can't connect!");
																}
															}
														});
													} else if (acc == GameService.ACCOUNT_OLD) {
														// Old Account
														gameService.fetchFriends(new Callback<Boolean>() {
															@Override
															public void apply(Boolean success) {
																if (success) {
																	gameServiceDone();
																} else {
																	// Fetch friends error
																	showError("Sorry, can't connect!");
																}
															}
														});
													} else if (acc == GameService.ACCOUNT_NEW) {
														if (gameService.getMe().new_player) {
															// If no, continue and upload local scores
															gameService.fetchFriends(new Callback<Boolean>() {
																@Override
																public void apply(Boolean success) {
																	if (success) {
																		gameServiceDone();
																	} else {
																		// Fetch friends error
																		showError("Sorry, can't connect!");
																	}
																}
															});
														} else {
															// If yes, replace scores
															replaceScores(new Callback<Boolean>() {
																@Override
																public void apply(Boolean success) {
																	if (success) {
																		gameService.fetchFriends(new Callback<Boolean>() {
																			@Override
																			public void apply(Boolean success) {
																				if (success) {
																					gameServiceDone();
																				} else {
																					// Fetch friends error
																					showError("Sorry, can't connect!");
																				}
																			}
																		});
																	} else {
																		// Score replacement error
																		showError("Sorry, can't connect!");
																	}
																}
															});
														}
													} else {
														// Account error
														showError("Sorry, can't connect!");
													}
												}
											});
										} else {
											// Login error
											showError("Sorry, can't connect!");
										}
									}
								});
							} else if (status == "OPENING") {
								// Do nothing
							} else {
								// Facebook status not open
								showError("Sorry, can't connect!");
							}
						}
					});
				} else {
					// Request to game service directly.
					changeGroup(currentGroup);
				}
			}
		}, 0.5f);
	}

	private void gameServiceDone() {
		clearOverlays();
		changeGroup(currentGroup);
	}

	private void changePuzzle(PuzzleData data) {
		if (!requesting) {
			currentPuzzle = data;
			updateLabels();
			request();
		}
	}

	private void changeGroup(int group) {
		if (!requesting) {
			currentGroup = group;
			updateLabels();
			request();
		}
	}

	private void updateLabels() {
		labelTopLeft.setText(currentGroup == GROUP_FRIENDS ? "Friends" : "Global");
		labelTopRight.setText("Puzzle " + currentPuzzle.id);
	}

	private void request() {
		// Gdx.app.debug("PuzzleScoreView", "Request Score | " + gameService.isDone());
		if (!gameService.isDone()) {
			showError("Sorry, error has occurred!");
			return;
		}
		showLoading("Fetching Scores...");
		requesting = true;
		leaderList.clear();
		final int currPuzzleId = currentPuzzle.id;
		Map<String, String> parameters = new HashMap<String, String>();
		parameters.put("type", String.valueOf(currentPuzzle.id));
		Settings settings = Resource.getSettings(Config.SETTINGS_EXTREME);
		int stars = settings.getInteger("s" + currPuzzleId);
		int score = settings.getInteger("b" + currPuzzleId);
		String patt = settings.getString("p" + currPuzzleId);

		parameters.put("stars", String.valueOf(stars));
		parameters.put("score", String.valueOf(score));
		parameters.put("pattern", Utils.md5(patt));

		if (currentGroup == GROUP_FRIENDS) {
			parameters.put("players", gameService.getFriends().toString());
		}

		gameService.getScores(parameters, new Callback<Response>() {
			@Override
			public void apply(Response t) {
				// Gdx.app.debug("PuzzleScoreView", "Request Score Response | ");
				if (!t.error()) {
					clearOverlays();
					// JsonValue root = new JsonReader().parse(t.dataString());
					JsonValue root = t.dataJsonValue();
					JsonValue score;
					JsonValue tmpPlayer;
					leaderList.clear();
					for (int i = root.size - 1; i >= 0; i--) {
						score = root.get(i);
						tmpPlayer = score.get("player");
						String name = tmpPlayer.getString("first_name") + " " + tmpPlayer.getString("last_name");
						leaderList.addItem(new LeaderListItem(	name,
																tmpPlayer.getString("picture"),
																score.getInt("stars"),
																score.getInt("score"),
																panelWidth,
																itemHeight));
					}
				} else {
					if (t.code() == 110) {
						// Server ordered to sync scores
						replaceScores(new Callback<Boolean>() {
							@Override
							public void apply(Boolean success) {
								if (!success) {
									showError("Sorry, error has occurred!");
								} else {
									request();
								}
							}
						});
					} else {
						showError("Sorry, error has occured!");
					}
				}
				requesting = false;
			}
		});
	}

	private void replaceScores(final Callback<Boolean> callback) {
		// Gdx.app.debug("PuzzleScoreView", "Replace Scores");
		gameService.getMyScores(new Callback<Response>() {
			@Override
			public void apply(Response t) {
				if (!t.error()) {
					// JsonValue root = new JsonReader().parse(t.dataString());
					JsonValue root = t.dataJsonValue();
					((ExtremePuzzles) puzzles).replaceScores(root.get("scores"));
					callback.apply(true);
				} else {
					callback.apply(false);
				}
			}
		});
	}

	private void setupTopBarControl() {
		tbc = new TopBarControl(this);
		tbc.setSpace(TopBarControl.POS_LEFT, 7, false);

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new ExtremePuzzleView(origPuzzle));
			}
		});

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_FRIENDS, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeGroup(GROUP_FRIENDS);
			}
		});

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_GLOBAL, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changeGroup(GROUP_GLOBAL);
			}
		});

		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_NEXT, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changePuzzle(puzzles.getNextOf(currentPuzzle.id));
			}
		});

		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_PREV, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				changePuzzle(puzzles.getPrevOf(currentPuzzle.id));
			}
		});

		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_PLAY, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new ExtremePuzzleView(currentPuzzle));
			}
		});
	}

	private void clearOverlays() {
		loading.setVisible(false);
		errDisplay.setVisible(false);
	}

	private void showLoading(String text) {
		errDisplay.setVisible(false);
		loading.setVisible(true);
		loading.setText(text);
	}

	private void showError(String text) {
		leaderList.clear();
		loading.setVisible(false);
		errDisplay.setVisible(true);
		errDisplay.setText(text + "\n\nTap to Retry");
	}

	@Override
	public void back() {
		super.back();
		tbc.triggerButton(TopBarControl.POS_LEFT, 0);
	}

}
