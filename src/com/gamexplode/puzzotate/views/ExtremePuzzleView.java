package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.controls.ExtremeMatchOverlay;
import com.gamexplode.puzzotate.controls.PreUpgradeOverlay;
import com.gamexplode.puzzotate.controls.StarGroup;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleState;

public class ExtremePuzzleView extends PuzzleView {

	private int pSeconds;
	private Label pLabelTimeValue;
	private Label pLabelBestValue;
	private StarGroup pGroupStars;
	private Timer pTimer;
	private ExtremeMatchOverlay matchOverlay;
	private int pBest;
	private int pStar;
	private int[] starScope;

	private Settings settingsExtreme;
	private PreUpgradeOverlay pPreUpgradeOverlay;

	private Timer.Task taskIncSeconds = new Timer.Task() {
		@Override
		public void run() {
			pSeconds++;
			// Gdx.app.debug("ExtremePuzzleView", "Seconds " + pSeconds);
			updateScore();
		}
	};

	public ExtremePuzzleView(PuzzleData data) {
		super(data);
	}

	private void showMatchOverlay() {
		if (matchOverlay == null) {
			matchOverlay = new ExtremeMatchOverlay();
			addActor(matchOverlay);
		}
		matchOverlay.show(pSeconds, false, getData().id);
	}

	@Override
	public void create() {

		settingsExtreme = Resource.getSettings(Config.SETTINGS_EXTREME);

		super.create();

		getLabelName().setText("Extreme " + getData().id);

		// pTimer = new Timer();
		// pTimer.stop();
		// pTimer.scheduleTask(new Timer.Task() {
		// @Override
		// public void run() {
		// pSeconds++;
		// Gdx.app.debug("ExtremePuzzleView", "Seconds " + pSeconds);
		// updateScore();
		// }
		// }, 0, 1);

		starScope = getData().extreme_star_scope != null ? getData().extreme_star_scope : new int[] { 60, 80, 100 };

		// Check puzzle state
		String lsState = settingsExtreme.getString("a" + getData().id);
		if (lsState.isEmpty()) {
			// Reset the puzzle
			drawPuzzle();
			reset();
		} else {
			// Use saved state
			Json json = new Json();
			PuzzleState ps = json.fromJson(PuzzleState.class, lsState);
			// Gdx.app.debug("PuzzleView", "Load State | " + ps.id + " | " + ps.seconds);
			try {
				pSeconds = ps.seconds;
				getData().shuffle_preset = ps.puzzle;
				getData().grid_objects_preset = ps.turnstile;
				updateScore();
				updateBest();
				drawPuzzle();
				// If match, show match overlay
				if (!isMatched() && pSeconds > 0) {
					start();
				}
			} catch (Exception e) {
				// Reset the puzzle
				drawPuzzle();
				reset();
			}
		}
	}

	@Override
	public void reset() {

		if (matchOverlay != null) {
			matchOverlay.hide();
		}

		pSeconds = 0;
		if (pTimer != null) {
			pTimer.stop();
		}
		updateBest();

		int star = settingsExtreme.getInteger("s" + getData().id);

		if (!(Resource.getSettings().getInteger("up_pr") >= 1) && star >= 3) {
			if (pPreUpgradeOverlay == null) {
				pPreUpgradeOverlay = new PreUpgradeOverlay(this);
			}
			pPreUpgradeOverlay.show();
			pPreUpgradeOverlay.setHeaders("Upgrade Required", "to shuffle a solved extreme puzzle.");
			return;
		}

		super.reset();
		updateScore();

		if (!getPuzzle().isShuffling()) {
			getPuzzle().shuffle();
		}
	}

	@Override
	protected void start() {
		super.start();
		// Gdx.app.debug("ExtremePuzzleView", "start");
		if (pTimer != null) {
			pTimer.stop();
			pTimer.clear();
		}
		pTimer = new Timer();
		pTimer.scheduleTask(taskIncSeconds, 0, 1);
		pTimer.start();
	}

	@Override
	protected void puzzleMatch() {
		super.puzzleMatch();
		if (pTimer != null) {
			pTimer.stop();
		}
		// Gdx.app.debug("ExtremePuzzleView", "" + pSeconds);
		if (pSeconds > 0) {
			showMatchOverlay();
		}
	}

	@Override
	protected void changePuzzle(PuzzleData value) {
		super.changePuzzle(value);
		getScene().setView(new ExtremePuzzleView(value));
	}

	@Override
	protected void puzzleStatus(int percent) {
		super.puzzleStatus(percent);
		// Gdx.app.debug("PuzzleView", "Status " + percent);
		if (percent >= starScope[2]) {
			// 3 Stars
			star(3);
		} else if (percent >= starScope[1]) {
			// 2 Star
			star(2);
		} else if (percent >= starScope[0]) {
			// 1 Star
			star(1);
		}
	}

	@Override
	protected void drawTopBarControl() {
		super.drawTopBarControl();
		getTopBar().addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_LEADERBOARD, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new PuzzleScoreView(getData()));
			}
		});

		getTopBar().addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_RESET, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				reset();
			}
		});
	}

	@Override
	protected void drawDisplay() {
		super.drawDisplay();

		// Label Styles
		LabelStyle lsWhiteOut = new LabelStyle();
		lsWhiteOut.font = Resource.getFont("white-outlined-normal");
		lsWhiteOut.font.getData().setScale(1);

		LabelStyle lsBlueOut = new LabelStyle();
		lsBlueOut.font = Resource.getFont("blue-outlined-normal");
		lsBlueOut.font.getData().setScale(1);

		LabelStyle lsWhiteOutLarge = new LabelStyle();
		lsWhiteOutLarge.font = Resource.getFont("white-outlined-large");
		lsWhiteOutLarge.font.getData().setScale(1);

		// Time Panel
		Panel pnlTime = new Panel();
		pnlTime.setSize(170, 162);
		addActor(pnlTime);
		pnlTime.alignMiddle();
		pnlTime.setYPercent(0.656f);
		pnlTime.setXPercent(0.026f);

		Label labelTime = new Label("Time", lsBlueOut);
		labelTime.setAlignment(Align.center);
		labelTime.setFontScale(0.8f);
		labelTime.setX(pnlTime.getHalfWidth() - (labelTime.getWidth() / 2));
		labelTime.setY(pnlTime.getHeight() * 0.68f);
		pnlTime.addActor(labelTime);

		pLabelTimeValue = new Label(formatTime(1500, "\n"), lsWhiteOut);
		pnlTime.addActor(pLabelTimeValue);
		pLabelTimeValue.setAlignment(Align.center);
		pLabelTimeValue.setX(pnlTime.getHalfWidth() - (pLabelTimeValue.getWidth() / 2));
		pLabelTimeValue.setY(pnlTime.getHeight() * 0.12f);

		// Best Panel
		Panel pnlBest = new Panel();
		pnlBest.setSize(220, 162);
		addActor(pnlBest);
		pnlBest.alignMiddle();
		pnlBest.setYPercent(0.656f);
		pnlBest.setXPercent(0.28f);

		Label labelBest = new Label("Best", lsBlueOut);
		labelBest.setAlignment(Align.center);
		labelBest.setFontScale(0.8f);
		labelBest.setX(pnlBest.getHalfWidth() - (labelBest.getWidth() / 2));
		labelBest.setY(pnlBest.getHeight() * 0.68f);
		pnlBest.addActor(labelBest);

		pLabelBestValue = new Label(formatTime(1500, " "), lsWhiteOut);
		pnlBest.addActor(pLabelBestValue);
		pLabelBestValue.setAlignment(Align.center);
		pLabelBestValue.setX(pnlBest.getHalfWidth() - (pLabelBestValue.getWidth() / 2));
		pLabelBestValue.setY(pnlBest.getHeight() * 0.05f);
		pnlBest.addActor(pLabelBestValue);

		pGroupStars = new StarGroup();
		pGroupStars.setAnim(true);
		pnlBest.addActor(pGroupStars);
		pGroupStars.setOrigin(Align.center);
		pGroupStars.setScale(0.6f);
		pGroupStars.alignMiddle();
		pGroupStars.setYPercent(0.23f);

	}

	@Override
	public void dispose() {
		super.dispose();
		if (getPuzzle() != null) {
			if (pTimer != null) {
				pTimer.clear();
			}
			saveState();
		}
	}

	private void saveState() {
		// Save puzzle state, time and puzzle
		if (getPuzzle().isShuffling()) {
			settingsExtreme.remove("a" + getData().id);
		} else {
			PuzzleState ps = new PuzzleState();
			ps.id = getData().id;
			ps.seconds = pSeconds;
			ps.puzzle = getPuzzle().getCurrentStateBlocks();
			ps.turnstile = getPuzzle().getCurrentStateTurnstiles();
			Json json = new Json();
			settingsExtreme.setString("a" + ps.id, json.toJson(ps));
		}
	}

	private String formatTime(int sec, String sep) {
		if (sec >= 3600) {
			int h = sec / 3600;
			int m = (sec % 3600) / 60;
			// Hours
			return String.format("%d.%02d" + sep + "h", h, m);
		} else if (sec >= 60) {
			int m = (sec % 3600) / 60;
			int s = sec % 60;
			// Minutes
			return String.format("%d.%02d" + sep + "m", m, s);
		} else {
			int s = sec % 60;
			// Seconds
			return s + sep + "s";
		}
	}

	private void updateScore() {
		pLabelTimeValue.setText(formatTime(pSeconds, "\n"));
	}

	private void updateBest() {
		pBest = settingsExtreme.getInteger("b" + getData().id);
		pStar = settingsExtreme.getInteger("s" + getData().id);
		if (pBest == 0) {
			pBest = -1;
			pLabelBestValue.setText("");
		} else {
			pLabelBestValue.setText(formatTime(pBest, " "));
		}
		pGroupStars.setStars(pStar);
	}

	private void star(int star) {
		if (isUserMoved()) {
			// Gdx.app.debug("PuzzleView", "Star " + star + " | " + pStar + " | " + pSeconds);
			Json json = new Json();
			PuzzleData data = getData();
			if (star == pStar) {
				// Same star, check seconds
				if (pBest > 0 && pSeconds < pBest) {
					settingsExtreme.setInteger("b" + data.id, pSeconds);
					settingsExtreme.setString("p" + data.id, json.toJson(data.pattern));
					pBest = pSeconds;
					pLabelBestValue.setText(formatTime(pSeconds, " "));
					pGroupStars.setStars(star);
					Resource.getSoundManager().playSound("star");
				}
			} else if (star > pStar && pSeconds > 0) {
				// New star, no need to check seconds, add star directly
				settingsExtreme.setInteger("s" + data.id, star);
				settingsExtreme.setInteger("b" + data.id, pSeconds);
				settingsExtreme.setString("p" + data.id, json.toJson(data.pattern));
				pStar = star;
				pBest = pSeconds;
				pLabelBestValue.setText(formatTime(pSeconds, " "));
				pGroupStars.setStars(star);
				Resource.getSoundManager().playSound("star");
			}
		}
	}

	@Override
	protected void showPuzzleList() {
		super.showPuzzleList();
		getScene().setView(new ExtremePuzzleListView(getData().id));
	}

}
