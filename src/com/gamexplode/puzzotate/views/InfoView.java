package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.ObjectMap;
import com.gamexplode.core.Config;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.PanelTable;
import com.gamexplode.puzzotate.controls.Puzzle;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;

public class InfoView extends View {

	private static final int INFO_GOAL = 1;
	private static final int INFO_MOVE = 2;
	private static final int INFO_TURNSTILE = 3;
	private static final int INFO_BOLT = 4;
	private static final int INFO_ICONS = 5;
	private Stack stackPuzzle;
	private Label labelDesc;
	private Label labelHead;
	private boolean panning;
	private Puzzle puzzle;
	private int currIndex;

	private ObjectMap<String, PuzzleData> mapPuzzles;
	private Button btnPrev;
	private Button btnNext;
	private Button btnPlay;
	private TopBarControl tbc;
	private int from;
	private Label labelTry;

	private final RepeatAction actBlink = Actions.forever(new SequenceAction(Actions.alpha(1, 0.4f), Actions.delay(1f), Actions.alpha(0.3f, 0.4f)));
	private Label labelSize;

	public InfoView() {
		this(0);
	}

	public InfoView(int from) {
		this.from = from;
	}

	@Override
	public void create() {
		super.create();
		mapPuzzles = new ObjectMap<String, PuzzleData>();
		drawPanels();
		setupTopBarControl();
		Resource.getSettings().setBoolean("howtoplay", true);
	}

	private void setupNavigation() {

		btnPrev = new Button(Resource.getAtlasManager().getRegion("icon-next"));
		btnPrev.setX(getWidth() * 0.05f);
		btnPrev.setY(getHeight() * 0.81f);
		btnPrev.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// setInfo(--currIndex);
				prev();
			}
		});

		TextureRegion t = new TextureRegion(Resource.getAtlasManager().getRegion("icon-next"));
		t.flip(true, false);
		btnNext = new Button(t);
		btnNext.setX(getWidth() * 0.85f);
		btnNext.setY(getHeight() * 0.81f);
		btnNext.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				// setInfo(++currIndex);
				next();
			}
		});

		btnPlay = new Button(Resource.getAtlasManager().getRegion("icon-play"));
		btnPlay.setX(getWidth() * 0.85f);
		btnPlay.setY(getHeight() * 0.81f);
		btnPlay.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});
		addActor(btnPrev);
		addActor(btnNext);
		addActor(btnPlay);
	}

	private void next() {
		setInfo(++currIndex);
	}

	private void prev() {
		setInfo(--currIndex);
	}

	private void drawPanels() {

		Table tablePanel = new Table();
		tablePanel.setDebug(getDebug());
		tablePanel.setWidth(Config.WIDTH);
		tablePanel.setHeight(Config.HEIGHT);

		PanelTable panelPuzzle = new PanelTable();

		PanelTable panelName = new PanelTable();
		PanelTable panelSize = new PanelTable();

		LabelStyle lsWhiteOut = new LabelStyle();
		lsWhiteOut.font = Resource.getFont("white-outlined-normal");

		LabelStyle lsBlueOut = new LabelStyle();
		lsBlueOut.font = Resource.getFont("blue-outlined-normal");

		LabelStyle lsWhiteOutLarge = new LabelStyle();
		lsWhiteOutLarge.font = Resource.getFont("white-outlined-large");

		// Main table
		tablePanel.add(panelName).padBottom(20);
		tablePanel.row();
		tablePanel.add(panelSize).padBottom(20);
		tablePanel.row();
		tablePanel.add(panelPuzzle);

		panelPuzzle.setSize(640, 640);
		panelName.setSize(640, 80);
		panelSize.setSize(640, 120);

		labelHead = new Label("Head", lsWhiteOutLarge);
		panelName.setActor(labelHead);

		// Main puzzle panel
		stackPuzzle = new Stack();
		panelPuzzle.setActor(stackPuzzle);

		labelDesc = new Label("Description", lsWhiteOut);
		labelDesc.setAlignment(Align.center);
		panelSize.setActor(labelDesc);

		labelTry = new Label("Try it now!", Resource.getLabelStyle("red-outlined-normal"));
		labelTry.setX((640 * 0.35f) - (labelTry.getWidth() / 2));
		labelTry.setY(640 * 0.94f);
		labelTry.addAction(actBlink);
		labelTry.setVisible(false);
		panelPuzzle.addActor(labelTry);
		
		labelSize = new Label("Group Size: 2", Resource.getLabelStyle("white-small"));
		labelSize.setX((640 * 0.76f) - (labelSize.getWidth() / 2));
		labelSize.setY(640 * 0.95f);
		labelSize.setFontScale(0.8f);
		labelSize.setVisible(false);
		panelPuzzle.addActor(labelSize);
		
		
		addActor(tablePanel);

		makePuzzles();

		setupNavigation();

		setInfo(INFO_GOAL);

	}

	private void makePuzzles() {

		PuzzleData puzzDataMove = new PuzzleData();
		puzzDataMove.id = 2;
		puzzDataMove.name = "Puzzle 1";
		puzzDataMove.width = 3;
		puzzDataMove.height = 3;
		puzzDataMove.rotate_size = 2;
		puzzDataMove.shuffle_level = 2;
		puzzDataMove.pattern = new int[] { 6111, 6211, 6311, 6111, 6211, 6311, 6111, 6211, 6311 };
		// puzzDataMove.grid_objects = new int[] { 111, 111, 111, 111, 251, 111, 111, 111, 111 };
		mapPuzzles.put("move", puzzDataMove);

		PuzzleData puzzDataBolt = new PuzzleData();
		puzzDataBolt.id = 2;
		puzzDataBolt.name = "Puzzle 1";
		puzzDataBolt.width = 4;
		puzzDataBolt.height = 3;
		puzzDataBolt.rotate_size = 2;
		puzzDataBolt.shuffle_level = 2;
		puzzDataBolt.pattern = new int[] { 6111, 6211, 6311, 6411, 6111, 6211, 6311, 6411, 6111, 6211, 6311, 6421 };
		puzzDataBolt.grid_objects = new int[] { 111, 111, 111, 111, 111, 313, 111, 251, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		mapPuzzles.put("bolt", puzzDataBolt);

		PuzzleData puzzDataTurnstile = new PuzzleData();
		puzzDataTurnstile.id = 2;
		puzzDataTurnstile.name = "Puzzle 1";
		puzzDataTurnstile.width = 4;
		puzzDataTurnstile.height = 3;
		puzzDataTurnstile.rotate_size = 2;
		puzzDataTurnstile.shuffle_level = 2;
		puzzDataTurnstile.pattern = new int[] { 6111, 6211, 6311, 6411, 6111, 6211, 6311, 6411, 6111, 6211, 6311, 6411 };
		puzzDataTurnstile.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		mapPuzzles.put("turnstile", puzzDataTurnstile);

	}

	private void setupTopBarControl() {

		tbc = new TopBarControl(this);

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (from != 0) {
					if (from == PuzzleManager.EXTREME) {
						getScene().setView(new ExtremePuzzleListView());
					} else {
						getScene().setView(new ArcadePuzzleListView());
					}
				} else {
					getScene().setView(new HomeView());
				}
				// getScene().setView(new HomeView());
			}
		});

		tbc.setSpace(TopBarControl.POS_RIGHT, 40);
		tbc.addText(TopBarControl.POS_RIGHT, "How To Play");
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		panning = false;
		return super.panStop(x, y, pointer, button);
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		// if (puzzle != null && !panning) {
		if (!panning) {
			boolean move = false;
			int dir = 0;
			if (deltaX > deltaY) {
				if (deltaX > Math.abs(deltaY)) {
					// Gdx.app.debug("tag", "Right");
					if (puzzle != null)
						move = puzzle.move(Direction.RIGHT, null);
					dir = 2;
				} else {
					// Gdx.app.debug("tag", "Up");
					if (puzzle != null)
						move = puzzle.move(Direction.UP, null);
				}
			} else {
				if (deltaY > Math.abs(deltaX)) {
					// Gdx.app.debug("tag", "Down");
					if (puzzle != null)
						move = puzzle.move(Direction.DOWN, null);
				} else {
					// Gdx.app.debug("tag", "Left");
					if (puzzle != null)
						move = puzzle.move(Direction.LEFT, null);
					dir = 1;
				}
			}
			// Gdx.app.debug("InfoView", "Move | " + move + " | " + dir);
			if (!move) {
				// Navigate page
				if (dir == 1) {
					next();
				} else if (dir == 2) {
					prev();
				}
			}
		}
		panning = true;
		return super.pan(x, y, deltaX, deltaY);
	}

	private void setInfo(int info) {
		if (INFO_GOAL <= info && info <= INFO_ICONS) {
			stackPuzzle.clear();
			labelDesc.setFontScale(1f);
			// stackPuzzle.setDebug(true);
			labelTry.setVisible(false);
			labelSize.setVisible(false);
			switch (info) {
				case INFO_GOAL:
					labelHead.setText("Goal");
					labelDesc.setText("Simply arrange blocks to match\nwith given pattern.");
					Image img = new Image(new Texture("images/info-image-goal.png"));
					// Image img = new Image(Resource.getAtlasManager().getRegionFromFile("images/info-image-goal"));
					stackPuzzle.add(img);
					// img.setOrigin(Align.center);
					// img.setScale(1.2f);
					break;
				case INFO_MOVE:
					labelHead.setText("Rotate");
					labelDesc.setFontScale(0.88f);
					labelDesc.setText("Arrange by rotating group of blocks.\nSwipe to left or right to rotate a group.\nGroup size varies in each puzzle.");
					stackPuzzle.add(puzzle = new Puzzle(mapPuzzles.get("move")));
					labelTry.setVisible(true);
					labelSize.setVisible(true);
					break;
				case INFO_BOLT:
					labelHead.setText("Bolt");
					labelDesc.setFontScale(0.88f);
					labelDesc.setText("Bolted objects are not movable\nand prevents group from rotating.");
					stackPuzzle.add(puzzle = new Puzzle(mapPuzzles.get("bolt")));
					// labelTry.setVisible(true);
					break;
				case INFO_TURNSTILE:
					labelHead.setText("Turnstile");
					labelDesc.setFontScale(0.88f);
					labelDesc.setText("Turnstile chains rotation.\nIt also prevent rotation if affecting\ngroup is not movable.");
					stackPuzzle.add(puzzle = new Puzzle(mapPuzzles.get("turnstile")));
					// labelTry.setVisible(true);
					break;
				case INFO_ICONS:
					labelHead.setText("Icons");
					labelDesc.setText("Familiarize key icons");
					Texture tx2 = new Texture("images/info-image-icons.png");
					tx2.setFilter(TextureFilter.Linear, TextureFilter.Linear);
					Image img2 = new Image(tx2);
					// img2.setDebug(true);
					img2.setOrigin(Align.center);
					img2.setScale(1.2f);
					stackPuzzle.add(img2);
					break;
				default:
					break;
			}
			if (puzzle != null)
				puzzle.setAnimate(true);
			currIndex = info;

			if (info == INFO_GOAL) {
				btnPlay.setVisible(false);
				btnPrev.setVisible(false);
				btnNext.setVisible(true);
			} else if (info == INFO_ICONS) {
				btnPlay.setVisible(true);
				btnPrev.setVisible(true);
				btnNext.setVisible(false);
			} else {
				btnPlay.setVisible(false);
				btnPrev.setVisible(true);
				btnNext.setVisible(true);
			}
		}
	}

	@Override
	public void back() {
		super.back();
		tbc.triggerButton(TopBarControl.POS_LEFT, 0);
	}

}
