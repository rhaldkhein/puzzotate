package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.controls.Overlay;
import com.gamexplode.puzzotate.controls.PuzzleListItem;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.controls.UpgradeOverlay;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.Puzzles;

public class PuzzleListView extends View {

	private ClickListener clickListener = new ClickListener() {
		@Override
		public void clicked(InputEvent event, float x, float y) {
			if (!pScolling) {
				Resource.getSoundManager().playSound("click");
				PuzzleListItem item = (PuzzleListItem) event.getListenerActor();
				int id = item.getPuzzleId();
				if (item.isLock()) {
					showOverlayLock();
				} else {
					itemClicked(id);
				}
			}
		}
	};

	private int pColumns;
	private int pRows;
	private int pIndex;
	private Puzzles pPuzzles;
	private int pTotalItems;
	private int pItemSpace;
	private Table[] pTables;
	private float pItemLength;
	private TopBarControl tbc;
	private int pScrollPointer;
	private Label lblPages;
	private Label lblPuzzleName;
	private boolean pScolling;
	private Overlay overlayLock;
	private Panel pnlTables;
	private int pStarsTotal;
	private UpgradeOverlay pUpgradeOverlay;
	

	public PuzzleListView(Puzzles puzzles) {
		this(puzzles, 0);
	}

	public PuzzleListView(Puzzles puzzles, int initialPageIndex) {
		super();
		setup(puzzles, initialPageIndex);
	}

	private void setup(Puzzles puzzles, int initialPageIndex) {
		initialPageIndex = initialPageIndex == 0 ? Resource.getSettings().getInteger("lli_" + puzzles.getType()) : initialPageIndex;
		pIndex = initialPageIndex <= 0 ? 1 : initialPageIndex;
		pPuzzles = puzzles;
		setSizePerPage(3, 4);
	}

	public void setSizePerPage(int w, int h) {
		pColumns = w;
		pRows = h;
	}

	@Override
	public void create() {
		super.create();
		if (!Resource.getSettings().getBoolean("howtoplay")) {
			getScene().setView(new InfoView(pPuzzles.getType()));
			return;
		}
		pTotalItems = pColumns * pRows;
		pItemSpace = 25;
		pTables = new Table[MathUtils.ceilPositive((float) pPuzzles.getPuzzles().size / (3 * 4)) + 1];
		pItemLength = getWidth() * 0.20f;
		display();
	}

	protected void display() {

		pnlTables = new Panel();
		pnlTables.setSize(680, 1000);
		pnlTables.setPosition((getWidth() / 2) - (pnlTables.getWidth() / 2), (getHeight() * 0.5f) - (pnlTables.getHeight() / 2));
		addActor(pnlTables);

		int pPageIndex = MathUtils.ceilPositive((float) pIndex / pTotalItems);
		pScrollPointer = pPageIndex - 1;

		// Initialize all tables
		for (int i = pTables.length - 1; i >= 0; i--) {
			pTables[i] = new Table();
		}

		IntMap<PuzzleData> allPuzzles = pPuzzles.getPuzzles();
		IntArray keys = allPuzzles.keys().toArray();
		keys.sort();
		int i = 1, pointer = 0, currTable = 0, star;
		boolean lock;
		pStarsTotal = 0;
		// pPageIndex = 2;
		// keys.add(0);
		// float xStart = (pnl.getWidth() / 2) - ((pPageIndex - 1) * getStage().getWidth());
		// Gdx.app.debug("PuzzleListView", "X Start " + xStart + " | Page Index " + pPageIndex);
		for (int key : keys.items) {
			if (pointer == 0) {
				Table tbl = pTables[currTable];
				// tbl.setPosition(((xStart) - (tbl.getWidth() / 2)) + (currTable * getStage().getWidth()), (pnl.getHeight() / 2)
				// - (tbl.getHeight() / 2));
				tbl.setPosition(pnlTables.getHalfWidth(), pnlTables.getHalfHeight());
				if (pPageIndex - 1 != currTable) {
					tbl.setVisible(false);
				}
				pnlTables.addActor(tbl);
			}

			PuzzleData data = allPuzzles.get(key);
			star = getStar(data.id);
			PuzzleListItem item = new PuzzleListItem(star < 3 ? 1 : 2);
			item.setSize(pItemLength, pItemLength);
			item.init();
			item.setStarSpace(item.getWidth() * 0.35f);
			pTables[currTable].add(item).width(pItemLength).height(pItemLength).pad(pItemSpace).padBottom(pItemSpace * 0.5f);
			item.setPuzzleId(data.id);
			pStarsTotal += star;
			item.setStar(star);
			lock = getLock(data.id);
			item.setLock(getLock(data.id));
			item.setLabel(lock ? "" : String.valueOf(data.id));
			item.setScore(getScoreLabel(data.id));
			item.addListener(clickListener);
			if (i > 0 && i % (pColumns) == 0) {
				pTables[currTable].row();
			}
			i++;
			pointer++;
			if (pointer >= pTotalItems) {
				pointer = 0;
				currTable++;
			}
		}

		// Gdx.app.debug("PuzzleListView", "" + pTotalItems);

		// Adding coming soon table
		Table tbl = pTables[pTables.length - 1];
		tbl.setPosition(pnlTables.getHalfWidth(), pnlTables.getHalfHeight());
		pnlTables.addActor(tbl);
		tbl.setVisible(false);
		// tbl.getColor().a = 0.5f;

		Image imgSoon = new Image(Resource.getAtlasManager().getRegion("image-gears"));
		// imgSoon.setOrigin(Align.center);
		// imgSoon.setRotation(45);

		Label lblComing = new Label("More To Come", Resource.getLabelStyle("blue-outlined-normal"));
		tbl.add(imgSoon).width(imgSoon.getWidth()).height(imgSoon.getHeight()).padBottom(50);
		tbl.row();
		tbl.add(lblComing).width(lblComing.getWidth()).height(lblComing.getHeight()).padBottom(50);

		// for (int i1 = 1; i1 <= pTotalItems; i1++) {
		// PuzzleListItem item = new PuzzleListItem(1);
		// item.setSize(pItemLength, pItemLength);
		// item.init();
		// item.setStarSpace(item.getWidth() * 0.35f);
		// item.clearListeners();
		// tbl.add(item).width(pItemLength).height(pItemLength).pad(pItemSpace).padBottom(pItemSpace * 0.5f);
		// item.setLabel("");
		// item.setScore("");
		// if (i1 > 0 && i1 % (pColumns) == 0) {
		// tbl.row();
		// }
		// }

		lblPuzzleName = new Label("Puzzles", Resource.getLabelStyle("white-outlined-large"));
		lblPuzzleName.setAlignment(Align.center);
		lblPuzzleName.setOrigin(Align.center);
		lblPuzzleName.setFontScale(0.9f);
		lblPuzzleName.setPosition((getWidth() / 2) - (lblPuzzleName.getWidth() / 2), getHeight() * 0.8f);
		addActor(lblPuzzleName);

		lblPages = new Label("0 / 0", Resource.getLabelStyle("white-outlined-normal"));
		lblPages.setAlignment(Align.left);
		// lblPages.setOrigin(Align.center);
		lblPages.setPosition(getWidth() * 0.7f, getHeight() * 0.16f);
		addActor(lblPages);

		Image imgStarsTotal = new Image(Resource.getAtlasManager().getRegion("icon-star-on"));
		imgStarsTotal.setOrigin(Align.center);
		imgStarsTotal.setPosition(getWidth() * 0.14f, getHeight() * 0.143f);
		imgStarsTotal.setScale(0.5f);
		addActor(imgStarsTotal);

		Label lblStarsTotal = new Label(pStarsTotal + " / " + (keys.size * 3), Resource.getLabelStyle("white-outlined-normal"));
		lblStarsTotal.setAlignment(Align.right);
		lblStarsTotal.setPosition(getWidth() * 0.26f, getHeight() * 0.16f);
		addActor(lblStarsTotal);

		updatePages();
		setupTopBarControl();
	}

	protected String getScoreLabel(int id) {
		// Override this
		return " ";
	}

	protected boolean getLock(int id) {
		// Override this
		return true;
	}

	// protected String getLabel(int id) {
	// // Override this
	// return " ";
	// }

	// protected int getBest(int id) {
	// // Override this
	// return 0;
	// }

	protected int getStar(int id) {
		// Override this
		return 0;
	}

	protected void itemClicked(int id) {
		// Override this
	}

	public Puzzles getPuzzles() {
		return pPuzzles;
	}

	protected void setListName(String label) {
		if (lblPuzzleName != null)
			lblPuzzleName.setText(label);
	}

	private void updatePages() {
		lblPages.setText("" + (pScrollPointer + 1) + " / " + pTables.length);
	}

	private void showOverlayLock() {
		if (overlayLock == null) {
			overlayLock = new Overlay();
			addActor(overlayLock);
			overlayLock.setSize(500, 550);
			overlayLock.alignCenter();
			// Close button
			Button btnClose = new Button(Resource.getAtlasManager().getRegion("icon-cancel"));
			btnClose.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					overlayLock.hide();
				}
			});
			// overlayLock.alignChildMiddle(btnClose);
			overlayLock.addActor(btnClose);
			btnClose.setY(overlayLock.getHeight() * 0.12f);
			btnClose.setX(overlayLock.getWidth() * 0.12f);

			// Close button
			Button btnUpgrade = new Button(Resource.getAtlasManager().getRegion("icon-check"));
			btnUpgrade.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					overlayLock.hide();
					if (pUpgradeOverlay == null) {
						pUpgradeOverlay = new UpgradeOverlay(PuzzleListView.this);
					}
					pUpgradeOverlay.show();
				}
			});
			// overlayLock.alignChildMiddle(btnUpgrade);
			overlayLock.addActor(btnUpgrade);
			btnUpgrade.setY(overlayLock.getHeight() * 0.12f);
			btnUpgrade.setX(overlayLock.getWidth() * 0.68f);

			// Label head
			Label lblHead = new Label("Locked!", Resource.getLabelStyle("white-outlined-large"));
			overlayLock.alignChildMiddle(lblHead);
			lblHead.setY(overlayLock.getHeight() * 0.75f);
			overlayLock.addActor(lblHead);

			// Label desc
			Label lblDesc = new Label("Solve previous puzzle\nto unlock", Resource.getLabelStyle("red-outlined-normal"));
			lblDesc.setAlignment(Align.center);
			overlayLock.alignChildMiddle(lblDesc);
			lblDesc.setY(overlayLock.getHeight() * 0.52f);
			overlayLock.addActor(lblDesc);

			// Label desc
			Label lblDescUpgrade = new Label("or upgrade", Resource.getLabelStyle("blue-outlined-normal"));
			lblDescUpgrade.setAlignment(Align.center);
			overlayLock.alignChildMiddle(lblDescUpgrade);
			lblDescUpgrade.setY(overlayLock.getHeight() * 0.35f);
			overlayLock.addActor(lblDescUpgrade);

		}
		overlayLock.show();
	}

	@Override
	public void panMove(Direction direction) {
		super.panMove(direction);
		if (direction == Direction.LEFT) {
			if (pScrollPointer < pTables.length - 1) {
				scroll(true);
			}
		} else if (direction == Direction.RIGHT) {
			if (pScrollPointer > 0) {
				scroll(false);
			}
		}
	}

	private void scroll(boolean dir) {
		if (pTables.length > 1 && !pScolling) {
			if (dir) {
				// right
				pScrollPointer++;
			} else {
				// left
				pScrollPointer--;
			}
			for (int i = pTables.length - 1; i >= 0; i--) {
				pTables[i].setVisible(false);
			}

			Table tblA, tblB;
			float a;
			if (dir) {
				tblA = pTables[pScrollPointer - 1];
				a = pnlTables.getHalfWidth() - getStage().getWidth();
				tblB = pTables[pScrollPointer];
				tblB.setX(pnlTables.getHalfWidth() + getStage().getWidth());
			} else {
				tblA = pTables[pScrollPointer + 1];
				a = pnlTables.getHalfWidth() + getStage().getWidth();
				tblB = pTables[pScrollPointer];
				tblB.setX(pnlTables.getHalfWidth() - getStage().getWidth());
			}

			tblA.setVisible(true);
			tblB.setVisible(true);

			tblA.addAction(Actions.sequence(Actions.moveTo(a, tblA.getY(), 0.2f), new Action() {
				@Override
				public boolean act(float delta) {
					pScolling = false;
					updatePages();
					return true;
				}
			}));

			tblB.addAction(Actions.sequence(Actions.moveTo(pnlTables.getHalfWidth(), tblB.getY(), 0.2f), new Action() {
				@Override
				public boolean act(float delta) {
					pScolling = false;
					updatePages();
					return true;
				}
			}));

			pScolling = true;

		}
	}

	protected TopBarControl getTopBar() {
		return tbc;
	}

	protected void setupTopBarControl() {
		tbc = new TopBarControl(this);
		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new HomeView());
			}
		});
		tbc.setSpace(TopBarControl.POS_RIGHT, 40);
		tbc.addText(TopBarControl.POS_RIGHT, "Select A Puzzle");
	}

	@Override
	public void back() {
		super.back();
		tbc.triggerButton(TopBarControl.POS_LEFT, 0);
	}

	@Override
	public void dispose() {
		super.dispose();
		Resource.getSettings().setInteger("lli_" + pPuzzles.getType(), pIndex);
	}

}
