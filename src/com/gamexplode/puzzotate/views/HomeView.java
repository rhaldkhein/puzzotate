package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.SoundManager;
import com.gamexplode.core.gameservice.Player;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.controls.UpgradeOverlay;

public class HomeView extends View {

	private Image imgAccount;
	private Label labelAccount;
	private SoundManager soundManager;
	private TopBarControl tbc;
	private UpgradeOverlay pUpgradeOverlay;

	public HomeView() {

	}

	@Override
	public void create() {
		super.create();
		soundManager = Resource.getSoundManager();
		display();
	}

	@Override
	public void dispose() {
		super.dispose();
		// No need to dispose sound manager, master class disposes this
		// soundManager.dispose();
	}

	private void display() {

		AtlasManager atlas = Resource.getAtlasManager();

		// Create background
		// Image bg = new Image(atlas.getRegionFromFile("images/bg-home"));
		// bg.setWidth(Config.WIDTH);
		// bg.setHeight(Config.HEIGHT);
		// addActor(bg);

		Image logo = new Image(atlas.getRegion("view-home-logo-icon"));
		logo.setX((getWidth() / 2) - (logo.getWidth() / 2));
		logo.setY(getHeight() * 0.65f);
		if (Config.DEVEL) {
			logo.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					Resource.clearSettings();
				}
			});
		}
		addActor(logo);

		LabelStyle labelStyleA = new LabelStyle();
		labelStyleA.font = Resource.getFont("white-outlined-normal");

		LabelStyle labelStyleB = new LabelStyle();
		labelStyleB.font = Resource.getFont("white-small");

		Label labelVersion = new Label("v" + Config.GAME_VERSION, labelStyleB);
		labelVersion.setFontScale(0.8f);
		labelVersion.setX(logo.getRight() - labelVersion.getWidth());
		labelVersion.setY(logo.getY() + (logo.getHeight() * 0.05f));
		addActor(labelVersion);

		Button buttonArcade = new Button(new Image(atlas.getRegion("view-home-icon-arcade")));
		// buttonArcade.setScale(1.1f);
		buttonArcade.setX((getWidth() * 0.35f) - (buttonArcade.getWidth() / 2));
		buttonArcade.setY(getHeight() * 0.39f);
		buttonArcade.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new ArcadePuzzleListView());
			}
		});
		addActor(buttonArcade);

		Button buttonExtreme = new Button(new Image(atlas.getRegion("view-home-icon-extreme")));
		// buttonExtreme.setScale(1.1f);
		buttonExtreme.setX((getWidth() * 0.65f) - (buttonExtreme.getWidth() / 2));
		buttonExtreme.setY(getHeight() * 0.39f);
		buttonExtreme.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new ExtremePuzzleListView());
			}
		});
		addActor(buttonExtreme);

		Player player = Resource.getGameService().getMe();
		if (player.id != null) {
			final Group gAccount = new Group();
			addActor(gAccount);

			imgAccount = new Image(atlas.getRegion("acc-image-dummy"));
			imgAccount.setHeight(60);
			imgAccount.setWidth(60);
			gAccount.addActor(imgAccount);
			Resource.getHttpClient().image(player.picture, new Callback<TextureRegion>() {
				@Override
				public void apply(TextureRegion t) {
					imgAccount.setDrawable(new TextureRegionDrawable(t));
				}
			});
			String name = player.first_name + " " + player.last_name;
			labelAccount = new Label(name, labelStyleA);
			labelAccount.setX(imgAccount.getRight() + 20);
			labelAccount.setY((imgAccount.getHeight() / 2) - (labelAccount.getHeight() / 2));
			if (labelAccount.getTextBounds().x > getWidth() * 0.5f) {
				labelAccount.setText(name.substring(0, Math.min(name.length(), 12)));
			}
			gAccount.addActor(labelAccount);
			gAccount.setHeight(imgAccount.getHeight());
			gAccount.setWidth(imgAccount.getRight() + labelAccount.getTextBounds().x);
			gAccount.setX((getWidth() / 2) - (gAccount.getWidth() / 2));
			gAccount.setY(getHeight() * 0.93f);
			gAccount.getColor().a = 0;
			gAccount.addAction(new SequenceAction(Actions.delay(0.8f), Actions.fadeIn(0.5f), Actions.delay(3), Actions.fadeOut(0.5f), new Action() {
				@Override
				public boolean act(float delta) {
					gAccount.remove();
					return true;
				}
			}));
			// buttonStart.setY(getHeight() * 0.35f);
		}

		Button buttonRate = new Button(new Image(atlas.getRegion("icon-rate")));
		buttonRate.setX(getWidth() * 0.23f);
		buttonRate.setY(getHeight() * 0.2f);
		buttonRate.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (Gdx.app.getType() == ApplicationType.Android) {
					Gdx.net.openURI(Config.URI_ANDROID);
				} else if (Gdx.app.getType() == ApplicationType.iOS) {
					Gdx.net.openURI(Config.URI_IOS);
				} else {
					Gdx.net.openURI(Config.URI_OTHER);
				}
			}
		});
		addActor(buttonRate);

		final Button buttonSound = new Button(	new Image(atlas.getRegion("icon-sound-on")),
												new Image(atlas.getRegion("icon-sound-off")),
												Button.SWITCH);
		// buttonSound.setX(getWidth() * 0.55f);
		buttonSound.setY(getHeight() * 0.2f);
		buttonSound.setSwitchValue(soundManager.isEnabled());
		buttonSound.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				soundManager.toggleEnabled();
				buttonSound.setSwitchValue(soundManager.isEnabled());
			}
		});
		addActor(buttonSound);
		alignChildMiddle(buttonSound);

		Button buttonUpgrade = new Button(new Image(atlas.getRegion("icon-upgrade")));
		buttonUpgrade.setX(getWidth() * 0.63f);
		buttonUpgrade.setY(getHeight() * 0.2f);
		buttonUpgrade.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (pUpgradeOverlay == null) {
					pUpgradeOverlay = new UpgradeOverlay(HomeView.this);
				}
				pUpgradeOverlay.show();
			}
		});
		addActor(buttonUpgrade);

		setupTopBarControl();
	}

	private void setupTopBarControl() {

		tbc = new TopBarControl(this);

		// Back
		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				back();
			}
		});

		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_INFO, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new InfoView());
			}
		});

	}

	@Override
	public void back() {
		super.back();
		Gdx.app.exit();
	}

}
