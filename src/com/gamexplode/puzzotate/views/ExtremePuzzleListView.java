package com.gamexplode.puzzotate.views;

import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.puzzotate.data.PuzzleManager;

public class ExtremePuzzleListView extends PuzzleListView {

	private Settings pSettings;

	public ExtremePuzzleListView() {
		this(0);
	}

	public ExtremePuzzleListView(int currentPuzzleId) {
		super(PuzzleManager.getPuzzles(PuzzleManager.EXTREME), currentPuzzleId);
		pSettings = Resource.getSettings(Config.SETTINGS_EXTREME);
	}

	@Override
	public void create() {
		super.create();
		setListName("Extreme");
	}

	private String formatBest(int best) {
		if (best >= 3600) {
			int h = best / 3600;
			int m = (best % 3600) / 60;
			// Hours
			return String.format("%d.%02d h", h, m);
		} else if (best >= 60) {
			int m = (best % 3600) / 60;
			int s = best % 60;
			// Minutes
			return String.format("%d.%02d m", m, s);
		} else {
			int s = best % 60;
			// Seconds
			return s + " s";
		}
	}

	@Override
	protected boolean getLock(int id) {
		return false;
	}

	@Override
	protected int getStar(int id) {
		return pSettings.getInteger("s" + id);
	}

	@Override
	protected String getScoreLabel(int id) {
		int best = pSettings.getInteger("b" + id);
		return best > 0 ? formatBest(best) : "";
	}

	@Override
	protected void itemClicked(int id) {
		getScene().setView(new ExtremePuzzleView(getPuzzles().getPuzzle(id)));
	}

}
