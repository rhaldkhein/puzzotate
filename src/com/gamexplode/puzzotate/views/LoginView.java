package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.PanelTable;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;

public class LoginView extends View {

	private PuzzleData origPuzzle;
	private TopBarControl tbc;

	public LoginView() {
		this(null);
	}

	public LoginView(PuzzleData data) {
		origPuzzle = data;
	}

	@Override
	public void create() {
		super.create();
		setupTopBarControl();
		drawPanels();
	}

	private void setupTopBarControl() {
		tbc = new TopBarControl(this);
		tbc.setSpace(TopBarControl.POS_LEFT, 7, false);

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				if (origPuzzle != null)
					getScene().setView(new ExtremePuzzleView(origPuzzle));
			}
		});

		tbc.setSpace(TopBarControl.POS_RIGHT, 40);
		tbc.addText(TopBarControl.POS_RIGHT, "Leaderboard");

	}

	private void drawPanels() {

		Table tablePanel = new Table();
		tablePanel.setDebug(getDebug());
		tablePanel.setWidth(Config.WIDTH);
		tablePanel.setHeight(Config.HEIGHT);

		PanelTable panelName = new PanelTable();
		PanelTable panelBody = new PanelTable();
		panelBody.getContainer().align(Align.top).padTop(20);
		// Panel panelSize = new Panel();

		LabelStyle lsWhiteOut = new LabelStyle();
		lsWhiteOut.font = Resource.getFont("white-outlined-normal");

		LabelStyle lsBlueOut = new LabelStyle();
		lsBlueOut.font = Resource.getFont("blue-outlined-normal");

		LabelStyle lsWhiteOutLarge = new LabelStyle();
		lsWhiteOutLarge.font = Resource.getFont("white-outlined-large");

		LabelStyle lsWhiteSmall = new LabelStyle();
		lsWhiteSmall.font = Resource.getFont("white-small");

		// Main table
		tablePanel.add(panelName).padBottom(20);
		tablePanel.row();
		tablePanel.add(panelBody);

		panelName.setSize(640, 80);
		panelBody.setSize(640, 820);
		// panelSize.setSize(640, 120);

		Label labelHead = new Label("Permission Request", lsWhiteOutLarge);
		panelName.setActor(labelHead);

		String txtA = "Puzzotate would like to use your\n	following Facebook information:\n";

		String txtB = "Basic Profile Info\n\nFriends List\n\n";

		String txtC = "Puzzotate will NOT post anything\non your page or share anything\nto your friends.\n\n";

		String txtD = "Please login to Facebook to grant\nus permission\n";

		// String txtHead = "In order to the leaderboard\n"
		// + "and view friends scores,\n"
		// + "We need you to permit us to access\n"
		// + "your Facebook friends,\n\n\n"
		// + "And will NOT post or do anything\non your page.";

		VerticalGroup vgBody = new VerticalGroup();
		panelBody.setActor(vgBody);

		Label labelA = new Label(txtA, lsWhiteOut);
		labelA.setAlignment(Align.center);
		vgBody.addActor(labelA);

		Label labelB = new Label(txtB, lsBlueOut);
		labelB.setAlignment(Align.center);
		labelB.setFontScale(1.1f);
		vgBody.addActor(labelB);

		Label labelC = new Label(txtC, lsWhiteOut);
		labelC.setAlignment(Align.center);
		vgBody.addActor(labelC);

		// Texture tx2 = new Texture("images/fb-login-button.png");
		// tx2.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// Image img2 = new Image(tx2);

		Label labelD = new Label(txtD, lsWhiteSmall);
		labelD.setAlignment(Align.center);
		labelD.setFontScale(0.85f);
		vgBody.addActor(labelD);

		Button fbButton = new Button(Resource.getAtlasManager()
												.getRegionFromFile("images/fb-login-button"));
		vgBody.addActor(fbButton);
		fbButton.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Resource.getSettings().setBoolean("fbpermitted", true);
				getScene().setView(new PuzzleScoreView(origPuzzle));
			}
		});

		addActor(tablePanel);

	}
	
	@Override
	public void back() {
		super.back();
		tbc.triggerButton(TopBarControl.POS_LEFT, 0);
	}
	
}
