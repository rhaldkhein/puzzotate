package com.gamexplode.puzzotate.views;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Config;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.controls.ArcadeMatchOverlay;
import com.gamexplode.puzzotate.controls.PreUpgradeOverlay;
import com.gamexplode.puzzotate.controls.StarGroup;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;

public class ArcadePuzzleView extends PuzzleView {

	private Label pLabelMovesValue;
	private StarGroup pGroupStars;
	private int pCountMoves;
	private int pCountMinMoves;
	private int pCurrentStar;
	private ArcadeMatchOverlay pMatchOverlay;
	private Label pLabelMinMoves;
	private Label labelMovesBest;
	private Panel pnlMoves;
	private Settings settingsArcade;
	private int pPremiumLevel;
	private PreUpgradeOverlay pPreUpgradeOverlay;

	public ArcadePuzzleView(PuzzleData data) {
		super(data);
	}

	@Override
	public void create() {
		settingsArcade = Resource.getSettings(Config.SETTINGS_ARCADE);
		pPremiumLevel = Resource.getSettings().getInteger("up_pr");
		super.create();
		getLabelName().setText("Arcade " + getData().id);
		// If not premium level 1
		if (!(pPremiumLevel >= 1) && !settingsArcade.getBoolean("ar_ul" + getData().id)) {
			back();
		} else {
			reset();
		}
	}

	@Override
	public void reset() {
		super.reset();
		pCountMoves = 0;
		pCountMinMoves = 0;
		pCurrentStar = 3;
		// updateMoves(0);
		drawPuzzle();
		changeStar(pCurrentStar);
		if (pMatchOverlay != null && pMatchOverlay.isVisible()) {
			pMatchOverlay.hide();
		}
		updateBest();
	}

	@Override
	protected boolean move(Direction direction, Callback<Boolean> done) {
		// If not premium level 1
		if (!(pPremiumLevel >= 1) && settingsArcade.contains("ar_s" + getData().id)) {
			// Gdx.app.debug("ArcadePuzzleView", "Restricted, Open upgrade overlay.");
			if (pPreUpgradeOverlay == null) {
				pPreUpgradeOverlay = new PreUpgradeOverlay(this);
			}
			pPreUpgradeOverlay.show();
			pPreUpgradeOverlay.setHeaders("Upgrade Required", "to solve an arcade puzzle again.");
			return false;
		} else {
			return super.move(direction, done);
		}
	}

	private void updateMoves() {
		// pLabelMovesValue.setText(pCountMoves + " / " + (pCountMinMoves <= 0 ? "-" : pCountMinMoves));
		pLabelMovesValue.setText("" + pCountMoves);
		// pLabelMinMoves.setText("" + (pCountMinMoves <= 0 ? "-" : pCountMinMoves));
		if (pCountMinMoves <= 0) {
			// pLabelMinMoves.setText("" + (pCountMinMoves <= 0 ? "-" : pCountMinMoves));
			pLabelMinMoves.setVisible(false);
			pGroupStars.setYPercent(0.2f);
		} else {
			pLabelMinMoves.setVisible(true);
			pLabelMinMoves.setText("" + pCountMinMoves);
			pGroupStars.setYPercent(0.4f);
		}
	}

	private void changeStar(int stars) {
		if (stars == 3) {
			pCountMinMoves = getData().arcade_move_min;
		} else if (stars == 2) {
			pCountMinMoves = getData().arcade_move_min * 2;
		} else {
			pCountMinMoves = 0;
		}
		pGroupStars.setStars(stars);
		updateMoves();
	}

	@Override
	protected void puzzleMove() {
		super.puzzleMove();
		// Gdx.app.debug("ArcadePuzzleView", "Move");
		pCountMoves++;
		updateMoves();
		if (isMatched()) {
			if (pMatchOverlay == null) {
				pMatchOverlay = new ArcadeMatchOverlay(this);
				// addActor(pMatchOverlay);
			}
			// Settings settingsArcade = Resource.getSettings(Config.SETTINGS_ARCADE);
			// String key = "ar_b" + getData().id;
			int best = settingsArcade.getInteger("ar_b" + getData().id);
			// Gdx.app.debug("ArcadePuzzleView", "Moves: " + pCountMoves + " | Best:" + best);
			if (best == 0 || pCountMoves < best) {
				settingsArcade.setInteger("ar_b" + getData().id, pCountMoves);
				settingsArcade.setInteger("ar_s" + getData().id, pGroupStars.getStars());
				pMatchOverlay.show(pGroupStars.getStars(), true);
			} else {
				pMatchOverlay.show(pGroupStars.getStars(), false);
			}
			// Unlock next puzzle
			if (!(pPremiumLevel >= 2))
				settingsArcade.setBoolean("ar_ul" + getPuzzles().getNextOf(getData().id).id, true);
			// Enable next button
			Button btn = getTopBar().getButton(TopBarControl.POS_RIGHT, 0);
			btn.getColor().a = 1f;
			btn.enableListener();
		} else if (pCountMinMoves > 0 && pCountMoves >= pCountMinMoves) {
			changeStar(--pCurrentStar);
		}
	}

	@Override
	protected void changePuzzle(PuzzleData value) {
		super.changePuzzle(value);
		getScene().setView(new ArcadePuzzleView(value));
	}

	@Override
	protected void drawDisplay() {
		super.drawDisplay();

		// Label Styles
		LabelStyle lsWhiteOut = new LabelStyle();
		lsWhiteOut.font = Resource.getFont("white-outlined-normal");
		lsWhiteOut.font.getData().setScale(1);

		LabelStyle lsBlueOut = new LabelStyle();
		lsBlueOut.font = Resource.getFont("blue-outlined-normal");
		lsBlueOut.font.getData().setScale(1);

		LabelStyle lsWhiteOutLarge = new LabelStyle();
		lsWhiteOutLarge.font = Resource.getFont("white-outlined-large");
		lsWhiteOutLarge.font.getData().setScale(1);

		// Time Panel
		pnlMoves = new Panel();
		pnlMoves.setSize(150, 162);
		addActor(pnlMoves);
		pnlMoves.setYPercent(0.656f);
		pnlMoves.setXPercent(0.026f);

		Label labelMoves = new Label("Moves", lsBlueOut);
		labelMoves.setAlignment(Align.center);
		labelMoves.setFontScale(0.8f);
		labelMoves.setX((pnlMoves.getWidth() * 0.5f) - (labelMoves.getWidth() / 2));
		labelMoves.setY(pnlMoves.getHeight() * 0.65f);
		pnlMoves.addActor(labelMoves);

		pLabelMovesValue = new Label("0", lsWhiteOutLarge);
		pLabelMovesValue.setWidth(pnlMoves.getWidth());
		pLabelMovesValue.setAlignment(Align.center);
		// pLabelMovesValue.setX(pnlMoves.getWidth() * 0.51f);
		pLabelMovesValue.setY(pnlMoves.getHeight() * 0.27f);
		// pLabelMovesValue.setY(pnlMoves.getHeight() * 0.06f);
		pnlMoves.addActor(pLabelMovesValue);

		labelMovesBest = new Label("-", Resource.getLabelStyle("white-small"));
		labelMovesBest.setAlignment(Align.center);
		labelMovesBest.setFontScale(0.7f);
		labelMovesBest.setX((pnlMoves.getWidth() * 0.5f) - (labelMovesBest.getWidth() / 2));
		labelMovesBest.setY(pnlMoves.getHeight() * 0.08f);
		pnlMoves.addActor(labelMovesBest);

		Panel pnlStars = new Panel();
		pnlStars.setSize(240, 162);
		addActor(pnlStars);
		pnlStars.setYPercent(0.656f);
		pnlStars.setXPercent(0.25f);

		// Label labelStars = new Label("Stars", lsBlueOut);
		// labelStars.setAlignment(Align.center);
		// labelStars.setFontScale(0.8f);
		// labelStars.setX((pnlStars.getWidth() * 0.5f) - (labelStars.getWidth() / 2));
		// labelStars.setY(pnlStars.getHeight() * 0.65f);
		// pnlStars.addActor(labelStars);

		pLabelMinMoves = new Label("0", lsWhiteOutLarge);
		pLabelMinMoves.setAlignment(Align.center);
		pLabelMinMoves.setWidth(pnlStars.getWidth());
		// labelMinMoves.setFontScale(0.8f);
		// labelMinMoves.setX((pnlStars.getWidth() * 0.5f) - (labelMinMoves.getWidth() / 2));
		pLabelMinMoves.setY(pnlStars.getHeight() * 0.08f);
		pnlStars.addActor(pLabelMinMoves);

		pGroupStars = new StarGroup();
		// pGroupStars.setSpace(pnlStars.getWidth() * 0.45f);
		pGroupStars.setAnim(true);
		pnlStars.addActor(pGroupStars);
		pGroupStars.setOrigin(Align.center);
		pGroupStars.setScale(0.6f);
		pGroupStars.alignMiddle();
		pGroupStars.setYPercent(0.4f);

	}

	@Override
	protected void drawTopBarControl() {
		super.drawTopBarControl();

		getTopBar().addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_RESET, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				reset();
			}
		});

		// Hide previous button if previous puzzle is lock
		// settingsArcade = Resource.getSettings(Config.SETTINGS_ARCADE);
		if (!(pPremiumLevel >= 2) && !settingsArcade.getBoolean("ar_ul" + getPuzzles().getPrevOf(getData().id).id)) {
			Button btn = getTopBar().getButton(TopBarControl.POS_RIGHT, 1);
			btn.getColor().a = 0.5f;
			// btn.clearListeners();
			btn.disableListener();
		}

		// Hide next button if next puzzle is lock
		if (!(pPremiumLevel >= 2) && !settingsArcade.getBoolean("ar_ul" + getPuzzles().getNextOf(getData().id).id)) {
			Button btn = getTopBar().getButton(TopBarControl.POS_RIGHT, 0);
			btn.getColor().a = 0.5f;
			// btn.clearListeners();
			btn.disableListener();
		}
	}

	@Override
	protected void showPuzzleList() {
		super.showPuzzleList();
		getScene().setView(new ArcadePuzzleListView(getData().id));
	}

	private void updateBest() {
		int best = settingsArcade.getInteger("ar_b" + getData().id);
		if (best <= 0) {
			pLabelMovesValue.setY(pnlMoves.getHeight() * 0.2f);
			labelMovesBest.setVisible(false);
		} else {
			pLabelMovesValue.setY(pnlMoves.getHeight() * 0.27f);
			labelMovesBest.setVisible(true);
			labelMovesBest.setText("Best  " + best);
		}
	}

}
