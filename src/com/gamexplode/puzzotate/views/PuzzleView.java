package com.gamexplode.puzzotate.views;

import java.util.Arrays;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.TimeUtils;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.IAdMob;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.controls.Puzzle;
import com.gamexplode.puzzotate.controls.PuzzlePattern;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.data.Puzzles;

public class PuzzleView extends View {

	private static long timestamp;

	private Callback<Boolean> moveCallback = new Callback<Boolean>() {
		@Override
		public void apply(Boolean t) {
			puzzleMove();
			// Gdx.app.debug("PuzzleView", "isUserMoved " + isUserMoved());
			if (!isUserMoved()) {
				userMoved = true;
				start();
			}
		}
	};

	private Puzzles puzzles;
	private Puzzle puzzle;
	private PuzzleData data;
	private TopBarControl tbc;
	private boolean pPanning;
	private boolean matched;
	private boolean userMoved;
	private Label pLabelName;

	private Panel pPanelPuzzle;

	public PuzzleView(PuzzleData data) {
		super();
		this.data = data;
	}

	@Override
	public void create() {
		super.create();

		puzzles = PuzzleManager.getPuzzles(data.type);
		pPanning = false;
		drawDisplay();
		drawTopBarControl();
		tryShowAd();
		// drawPuzzle();
	}

	protected void drawPuzzle() {
		if (puzzle != null) {
			puzzle.remove();
		}
		puzzle = new Puzzle(data);
		pPanelPuzzle.addActor(puzzle);
		puzzle.alignCenter();
		puzzle.setMatchCallback(new Callback<Object>() {
			@Override
			public void apply(Object t) {
				puzzleMatch();
			}
		});
		puzzle.setStatusCallback(new Callback<Integer>() {
			@Override
			public void apply(Integer t) {
				puzzleStatus(t);
			}
		});

		// If match, show match overlay
		if (Arrays.equals(getData().pattern, puzzle.getCurrentStateBlocks())) {
			matched = true;
			puzzleMatch();
		}

	}

	protected void drawDisplay() {

		// Label Styles
		LabelStyle lsWhiteOut = new LabelStyle();
		lsWhiteOut.font = Resource.getFont("white-outlined-normal");
		lsWhiteOut.font.getData().setScale(1);

		LabelStyle lsBlueOut = new LabelStyle();
		lsBlueOut.font = Resource.getFont("blue-outlined-normal");
		lsBlueOut.font.getData().setScale(1);

		LabelStyle lsWhiteOutLarge = new LabelStyle();
		lsWhiteOutLarge.font = Resource.getFont("white-outlined-large");
		lsWhiteOutLarge.font.getData().setScale(1);

		// Puzzle Panel
		pPanelPuzzle = new Panel();
		pPanelPuzzle.setSize(680, 680);
		addActor(pPanelPuzzle);
		pPanelPuzzle.alignMiddle();
		pPanelPuzzle.setYPercent(0.11f);

		// Pattern Panel
		Panel pnlPattern = new Panel();
		pnlPattern.setSize(260, 300);
		addActor(pnlPattern);
		pnlPattern.alignMiddle(0.29f);
		pnlPattern.setYPercent(0.656f);

		PuzzlePattern puzPattern = new PuzzlePattern(data);
		pnlPattern.addActor(puzPattern);
		puzPattern.alignMiddle();
		puzPattern.setYPercent(0.07f);

		Label lblPattern = new Label("Pattern", lsBlueOut);
		lblPattern.setAlignment(Align.center);
		lblPattern.setFontScale(0.9f);
		pnlPattern.addActor(lblPattern);
		lblPattern.setX(pnlPattern.getHalfWidth() - (lblPattern.getWidth() / 2));
		lblPattern.setY(pnlPattern.getHeight() * 0.82f);

		// Name Panel
		Panel pnlName = new Panel();
		pnlName.setSize(290, 120);
		addActor(pnlName);
		pnlName.alignMiddle(-0.272f);
		pnlName.setYPercent(0.797f);

		pLabelName = new Label("Puzzle", lsWhiteOutLarge);
		pnlName.addActor(pLabelName);
		pLabelName.setFontScale(0.85f);
		pLabelName.setY((pnlName.getHeight() * 0.54f) - (pLabelName.getHeight() / 2));
		// pLabelName.setX(pnlName.getWidth() * 0.08f);
		pLabelName.setWidth(pnlName.getWidth());
		// pLabelName.setHeight(pnlName.getHeight());
		pLabelName.setAlignment(Align.center);

		// Size Panel
		Panel pnlSize = new Panel();
		pnlSize.setSize(100, 120);
		addActor(pnlSize);
		pnlSize.alignMiddle(0.012f);
		pnlSize.setYPercent(0.797f);

		Label lblSize = new Label("Size", lsBlueOut);
		pnlSize.addActor(lblSize);
		lblSize.setAlignment(Align.center);
		lblSize.setFontScale(0.8f);
		lblSize.setX(pnlSize.getHalfWidth() - (lblSize.getWidth() / 2));
		lblSize.setY(pnlSize.getHeight() * 0.58f);

		Label plabelSizeValue = new Label(String.valueOf(data.rotate_size), lsWhiteOutLarge);
		plabelSizeValue.setFontScale(0.85f);
		pnlSize.addActor(plabelSizeValue);
		plabelSizeValue.setX(pnlSize.getHalfWidth() - (plabelSizeValue.getWidth() / 2));
		plabelSizeValue.setY(pnlSize.getHeight() * 0.1f);

	}

	protected void tryShowAd() {
		if (Resource.getSettings().getInteger("up_pr") <= 1) {
			if (timestamp > 0 && TimeUtils.timeSinceMillis(timestamp) > 60000 * 1.5) {
				IAdMob admob = (IAdMob) Resource.getInterface("admob");
				if (admob != null)
					admob.showInterstitial();
				timestamp = TimeUtils.millis();
			} else if (timestamp == 0) {
				timestamp = TimeUtils.millis();
			}
		}
	}

	protected Puzzles getPuzzles() {
		return puzzles;
	}

	protected Puzzle getPuzzle() {
		return puzzle;
	}

	public PuzzleData getData() {
		return data;
	}

	protected TopBarControl getTopBar() {
		return tbc;
	}

	protected Label getLabelName() {
		return pLabelName;
	}

	protected void start() {
		// Override this
	}

	public void reset() {
		userMoved = false;
		matched = false;
		tryShowAd();
	}

	protected boolean isMatched() {
		return matched;
	}

	protected boolean isUserMoved() {
		return userMoved;
	}

	protected void puzzleMatch() {
		// Override this
		matched = true;
	}

	protected void puzzleStatus(int percent) {
		// Override this
	}

	protected void changePuzzle(PuzzleData value) {
		// Override this
	}

	protected void puzzleMove() {
		// Override this
	}

	protected void showPuzzleList() {
		// Override this
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		pPanning = false;
		return super.panStop(x, y, pointer, button);
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (!matched) {
			if (!pPanning) {
				if (puzzle != null) {
					// boolean move = false;
					if (deltaX > deltaY) {
						if (deltaX > Math.abs(deltaY)) {
							// Gdx.app.debug("tag", "Right");
							// move = puzzle.move(Direction.RIGHT, moveCallback);
							move(Direction.RIGHT, moveCallback);
						} else {
							// Gdx.app.debug("tag", "Up");
							// move = puzzle.move(Direction.UP, moveCallback);
							move(Direction.UP, moveCallback);
						}
					} else {
						if (deltaY > Math.abs(deltaX)) {
							// Gdx.app.debug("tag", "Down");
							// move = puzzle.move(Direction.DOWN, moveCallback);
							move(Direction.DOWN, moveCallback);
						} else {
							// Gdx.app.debug("tag", "Left");
							// move = puzzle.move(Direction.LEFT, moveCallback);
							move(Direction.LEFT, moveCallback);
						}
					}
					// Gdx.app.debug("PuzzleView", "" + move);
					// if (move) {
					// start();
					// }
				}
			}
			pPanning = true;
		}
		return super.pan(x, y, deltaX, deltaY);
	}

	protected boolean move(Direction direction, final Callback<Boolean> done) {
		return puzzle.move(direction, done);
	}

	protected void drawTopBarControl() {
		tbc = new TopBarControl(this);
		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				showPuzzleList();
			}
		});
		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_NEXT, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				boolean proceed = false;
				for (Entry<PuzzleData> entry : puzzles.getPuzzles()) {
					if (proceed) {
						// getScene().setView(new PuzzleView(entry.value));
						changePuzzle(entry.value);
						return;
					}
					if (data.id == entry.value.id) {
						proceed = true;
					}
				}
				// getScene().setView(new PuzzleView(puzzles.getFirst()));
				changePuzzle(puzzles.getFirst());
			}
		});
		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_PREV, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				PuzzleData previous = null;
				for (Entry<PuzzleData> entry : puzzles.getPuzzles()) {
					if (data.id == entry.value.id) {
						break;
					}
					previous = entry.value;
				}
				if (previous == null) {
					// Get last puzzle
					// getScene().setView(new PuzzleView(puzzles.getLast()));
					changePuzzle(puzzles.getLast());
				} else {
					// getScene().setView(new PuzzleView(previous));
					changePuzzle(previous);
				}
			}
		});
	}

	@Override
	public void back() {
		super.back();
		tbc.triggerButton(TopBarControl.POS_LEFT, 0);
	}

}
