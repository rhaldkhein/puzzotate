package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.puzzotate.controls.Overlay;

public class InputMessageBox extends Overlay {

	private Callback<String> callback;
	private String msg;

	public InputMessageBox(View view, String msg, Callback<String> callback) {
		super();
		view.addActor(this);
		this.callback = callback;
		this.msg = msg;
		create();
	}

	private void create() {
		setSize(600, 400);
		alignCenter();
		drawDisplay();
		show(false);
	}

	private void drawDisplay() {

		Label lblMessage = new Label(msg, Resource.getLabelStyle("white-outlined-large"));
		addActor(lblMessage);
		alignChildMiddle(lblMessage);
		lblMessage.setY(getHeight() * 0.7f);
		
		Button btnClose = new Button(Resource.getAtlasManager().getRegion("icon-cancel"));
		addActor(btnClose);
		btnClose.setX(getWidth() * 0.15f);
		btnClose.setY(getHeight() * 0.2f);
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide(false);
				callback.apply("no");
			}
		});
		
		Button btnNext = new Button(Resource.getAtlasManager().getRegion("icon-next-word"));
		addActor(btnNext);
		btnNext.setX(getWidth() * 0.6f);
		btnNext.setY(getHeight() * 0.2f);
		btnNext.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide(false);
				callback.apply("yes");
			}
		});
		
	}

}
