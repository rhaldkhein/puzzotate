package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.controls.Block;
import com.gamexplode.puzzotate.controls.Puzzle;
import com.gamexplode.puzzotate.controls.PuzzlePattern;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.controls.Turnstile;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.data.Puzzles;

public class DemoPuzzleView extends View {

	private Callback<Boolean> moveCallback = new Callback<Boolean>() {
		@Override
		public void apply(Boolean t) {
			puzzleMoved();
		}
	};

	private Panel pPanelPuzzle;
	private PuzzleData data;
	private Label pLblSizeValue;
	private Panel pPnlPattern;
	private Puzzle puzzle;
	private boolean pPanning;
	private boolean pIsMatch;
	private Label pLblMatchValue;
	private TopBarControl tbc;
	private Label pLblMovesValue;
	private int pCountMoves;
	private Label pLblWidthValue;
	private Label pLblHeightValue;
	private PuzzlePattern pattern;
	private boolean pEditPuzzle;
	private Label pLblEditModValue;
	private boolean pDragging;
	private int pVisibleValue;
	private Label pLblVisibleValue;
	private Label pLblMinMoves;

	private int[] savedStateBlocks;
	private int[] savedStateGrid;

	private Puzzles puzzles;

	// private Label pLblVisibleValue;
	// private int pClipboard;

	public DemoPuzzleView(PuzzleData data) {
		this.data = data;
	}

	@Override
	public void create() {
		super.create();

		puzzles = PuzzleManager.getPuzzles(data.type);

		drawTopBarControl();
		drawDisplay();
		update();

		pEditPuzzle = false;
		pDragging = false;
		// pClipboard = 0;
		pVisibleValue = 1;

	}

	private void drawDisplay() {

		// Label Styles
		LabelStyle lsBlueOut = Resource.getLabelStyle("blue-outlined-normal");
		LabelStyle lsWhiteOutLarge = Resource.getLabelStyle("white-outlined-large");

		// Puzzle Panel
		pPanelPuzzle = new Panel();
		pPanelPuzzle.setSize(680, 680);
		addActor(pPanelPuzzle);
		pPanelPuzzle.alignMiddle();
		pPanelPuzzle.setYPercent(0.11f);

		// Pattern Panel
		pPnlPattern = new Panel();
		pPnlPattern.setSize(260, 300);
		addActor(pPnlPattern);
		pPnlPattern.alignMiddle(0.29f);
		pPnlPattern.setYPercent(0.656f);

		Label lblPattern = new Label("Pattern", lsBlueOut);
		lblPattern.setAlignment(Align.center);
		lblPattern.setFontScale(0.9f);
		pPnlPattern.addActor(lblPattern);
		lblPattern.setX(pPnlPattern.getHalfWidth() - (lblPattern.getWidth() / 2));
		lblPattern.setY(pPnlPattern.getHeight() * 0.82f);

		// Name Panel
		Panel pnlName = new Panel();
		pnlName.setSize(80, 100);
		addActor(pnlName);
		pnlName.setXPercent(0.025f);
		pnlName.setYPercent(0.81f);

		Label pLabelName = new Label("" + data.id, lsWhiteOutLarge);
		pnlName.addActor(pLabelName);
		pLabelName.setFontScale(0.85f);
		pLabelName.setY((pnlName.getHeight() * 0.54f) - (pLabelName.getHeight() / 2));
		// pLabelName.setX(pnlName.getWidth() * 0.08f);
		pLabelName.setWidth(pnlName.getWidth());
		// pLabelName.setHeight(pnlName.getHeight());
		pLabelName.setAlignment(Align.center);

		// Size Panel
		Panel pnlSize = new Panel();
		pnlSize.setSize(80, 100);
		addActor(pnlSize);
		pnlSize.setXPercent(0.14f);
		pnlSize.setYPercent(0.81f);
		pnlSize.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputNumber(DemoPuzzleView.this, new Callback<String>() {
					@Override
					public void apply(String t) {
						t = t.trim();
						if (!t.isEmpty()) {
							data.rotate_size = Integer.parseInt(t);
							update();
						}
					}
				});
			}
		});

		Label lblSize = new Label("Si", lsBlueOut);
		pnlSize.addActor(lblSize);
		lblSize.setAlignment(Align.center);
		lblSize.setFontScale(0.8f);
		lblSize.setX(pnlSize.getHalfWidth() - (lblSize.getWidth() / 2));
		lblSize.setY(pnlSize.getHeight() * 0.5f);

		pLblSizeValue = new Label(" ", lsWhiteOutLarge);
		pLblSizeValue.setFontScale(0.85f);
		pnlSize.addActor(pLblSizeValue);
		pLblSizeValue.setX(pnlSize.getHalfWidth() - (pLblSizeValue.getWidth() / 2));
		pLblSizeValue.setY(pnlSize.getHeight() * 0f);
		pLblSizeValue.setAlignment(Align.center);

		Panel pPnlMatch = new Panel();
		pPnlMatch.setSize(80, 100);
		addActor(pPnlMatch);
		pPnlMatch.setXPercent(0.26f);
		pPnlMatch.setYPercent(0.81f);

		Label lblMatch = new Label("Ma", lsBlueOut);
		pPnlMatch.addActor(lblMatch);
		lblMatch.setAlignment(Align.center);
		lblMatch.setFontScale(0.8f);
		lblMatch.setX(pPnlMatch.getHalfWidth() - (lblMatch.getWidth() / 2));
		lblMatch.setY(pPnlMatch.getHeight() * 0.5f);

		pLblMatchValue = new Label("N", lsWhiteOutLarge);
		pLblMatchValue.setFontScale(0.85f);
		pPnlMatch.addActor(pLblMatchValue);
		pLblMatchValue.setWidth(pPnlMatch.getWidth());
		pLblMatchValue.setX(0);
		pLblMatchValue.setY(pPnlMatch.getHeight() * 0f);
		pLblMatchValue.setAlignment(Align.center);

		Panel pPnlEditMode = new Panel();
		pPnlEditMode.setSize(80, 100);
		addActor(pPnlEditMode);
		pPnlEditMode.setXPercent(0.38f);
		pPnlEditMode.setYPercent(0.81f);
		pPnlEditMode.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				pEditPuzzle = !pEditPuzzle;
				pLblEditModValue.setText(pEditPuzzle ? "Y" : "N");
			}
		});

		Label lblEditMode = new Label("Ed", lsBlueOut);
		pPnlEditMode.addActor(lblEditMode);
		lblEditMode.setAlignment(Align.center);
		lblEditMode.setFontScale(0.8f);
		lblEditMode.setX(pPnlEditMode.getHalfWidth() - (lblEditMode.getWidth() / 2));
		lblEditMode.setY(pPnlEditMode.getHeight() * 0.5f);

		pLblEditModValue = new Label("N", lsWhiteOutLarge);
		pLblEditModValue.setFontScale(0.85f);
		pPnlEditMode.addActor(pLblEditModValue);
		pLblEditModValue.setWidth(pPnlEditMode.getWidth());
		pLblEditModValue.setX(0);
		pLblEditModValue.setY(pPnlEditMode.getHeight() * 0f);
		pLblEditModValue.setAlignment(Align.center);

		Panel pnlMoves = new Panel();
		pnlMoves.setSize(80, 100);
		addActor(pnlMoves);
		pnlMoves.setYPercent(0.728f);
		pnlMoves.setXPercent(0.026f);
		pnlMoves.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				pCountMoves = 0;
				pLblMovesValue.setText("" + pCountMoves);
			}
		});

		Label labelMoves = new Label("Mo", lsBlueOut);
		labelMoves.setAlignment(Align.center);
		labelMoves.setFontScale(0.8f);
		labelMoves.setX((pnlMoves.getWidth() * 0.5f) - (labelMoves.getWidth() / 2));
		labelMoves.setY(pnlMoves.getHeight() * 0.5f);
		pnlMoves.addActor(labelMoves);

		pLblMovesValue = new Label("0", lsWhiteOutLarge);
		pLblMovesValue.setWidth(pnlMoves.getWidth());
		pLblMovesValue.setAlignment(Align.center);
		pLblMovesValue.setY(pnlMoves.getHeight() * 0f);
		pnlMoves.addActor(pLblMovesValue);

		Panel pnlWidth = new Panel();
		pnlWidth.setSize(80, 100);
		addActor(pnlWidth);
		pnlWidth.setYPercent(0.728f);
		pnlWidth.setXPercent(0.14f);
		pnlWidth.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputNumber(DemoPuzzleView.this, new Callback<String>() {
					@Override
					public void apply(String t) {
						t = t.trim();
						if (!t.isEmpty()) {
							data.width = Integer.parseInt(t);
							data.pattern = generatePattern(data.width * data.height);
							data.grid_objects = null;
							data.shuffle_preset = null;
							update();
						}
					}
				});
			}
		});

		Label labelWidth = new Label("Wi", lsBlueOut);
		labelWidth.setAlignment(Align.center);
		labelWidth.setFontScale(0.8f);
		labelWidth.setX((pnlWidth.getWidth() * 0.5f) - (labelWidth.getWidth() / 2));
		labelWidth.setY(pnlWidth.getHeight() * 0.5f);
		pnlWidth.addActor(labelWidth);

		pLblWidthValue = new Label("0", lsWhiteOutLarge);
		pLblWidthValue.setWidth(pnlWidth.getWidth());
		pLblWidthValue.setAlignment(Align.center);
		pLblWidthValue.setY(pnlWidth.getHeight() * 0f);
		pnlWidth.addActor(pLblWidthValue);

		Panel pnlHeight = new Panel();
		pnlHeight.setSize(80, 100);
		addActor(pnlHeight);
		pnlHeight.setYPercent(0.728f);
		pnlHeight.setXPercent(0.26f);
		pnlHeight.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputNumber(DemoPuzzleView.this, new Callback<String>() {
					@Override
					public void apply(String t) {
						t = t.trim();
						if (!t.isEmpty()) {
							data.height = Integer.parseInt(t);
							data.pattern = generatePattern(data.width * data.height);
							data.grid_objects = null;
							data.shuffle_preset = null;
							update();
						}
					}
				});
			}
		});

		Label labelHeight = new Label("He", lsBlueOut);
		labelHeight.setAlignment(Align.center);
		labelHeight.setFontScale(0.8f);
		labelHeight.setX((pnlHeight.getWidth() * 0.5f) - (labelHeight.getWidth() / 2));
		labelHeight.setY(pnlHeight.getHeight() * 0.5f);
		pnlHeight.addActor(labelHeight);

		pLblHeightValue = new Label("0", lsWhiteOutLarge);
		pLblHeightValue.setWidth(pnlHeight.getWidth());
		pLblHeightValue.setAlignment(Align.center);
		pLblHeightValue.setY(pnlHeight.getHeight() * 0f);
		pnlHeight.addActor(pLblHeightValue);

		Panel pnlVisible = new Panel();
		pnlVisible.setSize(80, 100);
		addActor(pnlVisible);
		pnlVisible.setYPercent(0.728f);
		pnlVisible.setXPercent(0.38f);
		pnlVisible.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (pVisibleValue == 2) {
					pVisibleValue = 0;
				}
				pVisibleValue++;
				pLblVisibleValue.setText(pVisibleValue + "");
				if (pVisibleValue == 2) {
					puzzle.setBlocksVisible(false);
				} else {
					puzzle.setBlocksVisible(true);
				}
			}
		});

		Label labelVisible = new Label("Vi", lsBlueOut);
		labelVisible.setAlignment(Align.center);
		labelVisible.setFontScale(0.8f);
		labelVisible.setX((pnlVisible.getWidth() * 0.5f) - (labelVisible.getWidth() / 2));
		labelVisible.setY(pnlVisible.getHeight() * 0.5f);
		pnlVisible.addActor(labelVisible);

		pLblVisibleValue = new Label("1", lsWhiteOutLarge);
		pLblVisibleValue.setWidth(pnlVisible.getWidth());
		pLblVisibleValue.setAlignment(Align.center);
		pLblVisibleValue.setY(pnlVisible.getHeight() * 0f);
		pnlVisible.addActor(pLblVisibleValue);

		Panel pnlSave = new Panel();
		pnlSave.setSize(80, 100);
		addActor(pnlSave);
		pnlSave.setYPercent(0.645f);
		pnlSave.setXPercent(0.026f);
		pnlSave.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputMessageBox(DemoPuzzleView.this, "Save ?", new Callback<String>() {
					@Override
					public void apply(String t) {
						if (t.equals("yes")) {
							DemoPuzzles puzzles = ((DemoPuzzles) PuzzleManager.getPuzzles(PuzzleManager.DEMO));
							data.shuffle_preset = puzzle.getCurrentStateBlocks();
							data.grid_objects = puzzle.getCurrentStateTurnstiles();
							puzzles.addPuzzle(data);
							puzzles.save();
						}
					}
				});
			}
		});

		Panel pnlMinMoves = new Panel();
		pnlMinMoves.setSize(80, 100);
		addActor(pnlMinMoves);
		pnlMinMoves.setYPercent(0.645f);
		pnlMinMoves.setXPercent(0.14f);
		pnlMinMoves.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputNumber(DemoPuzzleView.this, new Callback<String>() {
					@Override
					public void apply(String t) {
						t = t.trim();
						if (!t.isEmpty()) {
							data.arcade_move_min = Integer.parseInt(t);
							update();
						}
					}
				});
			}
		});

		Label labelMinMoves = new Label("Mi", lsBlueOut);
		labelMinMoves.setAlignment(Align.center);
		labelMinMoves.setFontScale(0.8f);
		labelMinMoves.setX((pnlMinMoves.getWidth() * 0.5f) - (labelMinMoves.getWidth() / 2));
		labelMinMoves.setY(pnlMinMoves.getHeight() * 0.5f);
		pnlMinMoves.addActor(labelMinMoves);

		pLblMinMoves = new Label("0", lsWhiteOutLarge);
		pLblMinMoves.setWidth(pnlMinMoves.getWidth());
		pLblMinMoves.setAlignment(Align.center);
		pLblMinMoves.setY(pnlMinMoves.getHeight() * 0f);
		pnlMinMoves.addActor(pLblMinMoves);

		Panel pnlSaveState = new Panel();
		pnlSaveState.setSize(80, 100);
		addActor(pnlSaveState);
		pnlSaveState.setYPercent(0.645f);
		pnlSaveState.setXPercent(0.26f);
		pnlSaveState.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputMessageBox(DemoPuzzleView.this, "Mark State ?", new Callback<String>() {
					@Override
					public void apply(String t) {
						if (t.equals("yes")) {
							savedStateBlocks = puzzle.getCurrentStateBlocks();
							savedStateGrid = puzzle.getCurrentStateTurnstiles();
							pCountMoves = 0;
							pLblMovesValue.setText("" + pCountMoves);
						}
					}
				});
			}
		});

		Panel pnlLoadState = new Panel();
		pnlLoadState.setSize(80, 100);
		addActor(pnlLoadState);
		pnlLoadState.setYPercent(0.645f);
		pnlLoadState.setXPercent(0.38f);
		pnlLoadState.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (savedStateBlocks != null && savedStateGrid != null) {
					data.shuffle_preset = savedStateBlocks;
					data.grid_objects = savedStateGrid;
					pCountMoves = 0;
					pLblMovesValue.setText("" + pCountMoves);
					update();
				}
			}
		});

		Panel pnlSwap = new Panel();
		pnlSwap.setSize(80, 100);
		addActor(pnlSwap);
		pnlSwap.setYPercent(0.645f);
		pnlSwap.setXPercent(0.495f);
		pnlSwap.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				int[] tmp = data.pattern;
				data.pattern = puzzle.getCurrentStateBlocks();
				data.shuffle_preset = tmp;
				update();
			}
		});

	}

	private void update() {
		// Update size label
		pLblSizeValue.setText("" + data.rotate_size);
		pLblWidthValue.setText("" + data.width);
		pLblHeightValue.setText("" + data.height);
		pLblMinMoves.setText("" + data.arcade_move_min);

		// Update pattern
		if (pattern != null)
			pattern.remove();
		pattern = new PuzzlePattern(data);
		// pPnlPattern.clear();
		pPnlPattern.addActor(pattern);
		pattern.alignMiddle();
		pattern.setYPercent(0.07f);

		// Update puzzle
		if (puzzle != null)
			puzzle.remove();
		puzzle = new Puzzle(data);
		// pPanelPuzzle.clear();
		pPanelPuzzle.addActor(puzzle);
		puzzle.alignCenter();
		puzzle.setMatchCallback(new Callback<Object>() {
			@Override
			public void apply(Object t) {
				puzzleMatch();
			}
		});
		if (pVisibleValue == 2) {
			puzzle.setBlocksVisible(false);
		}

		pPnlPattern.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.debug("DemoPuzzleView", "Test");
				data.shuffle_preset = data.pattern;
				pCountMoves = 0;
				pLblMovesValue.setText("" + pCountMoves);
				update();
			}
		});

		puzzle.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				if (event.getTarget().getParent() instanceof Block && pEditPuzzle && !pDragging) {
					final Block block = (Block) event.getTarget().getParent();
					new InputBlock(DemoPuzzleView.this, new Callback<Integer>() {
						public void apply(Integer t) {
							int[] pattern = puzzle.getCurrentStateBlocks();
							pattern[puzzle.getBlockIndex(block)] = t;
							data.pattern = pattern;
							data.shuffle_preset = null;
							update();
						}
					});
				}

				if (event.getTarget().getParent() instanceof Turnstile && !pDragging) {
					if (pEditPuzzle) {
						final Turnstile ts = (Turnstile) event.getTarget().getParent();
						new InputGrid(DemoPuzzleView.this, new Callback<Integer>() {
							public void apply(Integer t) {
								// Gdx.app.debug("DemoPuzzleView", "" + t);
								int[] grid = puzzle.getCurrentStateTurnstiles();
								grid[puzzle.getTurnstileIndex(ts)] = t;
								data.grid_objects = grid;
								update();
							}
						});
					} else if (pVisibleValue == 2) {
						// Blocks hidden, change turnstile rotation
						final Turnstile ts = (Turnstile) event.getTarget().getParent();
						new InputGrid(DemoPuzzleView.this, ts.getTransformedId(), new Callback<Integer>() {
							public void apply(Integer t) {
								ts.setAngleById(t);
							}
						});
					}
				}
			}
		});

		puzzle.addListener(new DragListener() {
			private Block targetBlock;
			private int idBlock;

			@Override
			public void dragStart(InputEvent event, float x, float y, int pointer) {
				super.dragStart(event, x, y, pointer);
				pDragging = true;
				if (event.getTarget().getParent() instanceof Block && pEditPuzzle) {
					idBlock = ((Block) event.getTarget().getParent()).getTransformedId();
				}
			}

			@Override
			public void dragStop(InputEvent event, float x, float y, int pointer) {
				super.dragStop(event, x, y, pointer);
				if (targetBlock != null && pEditPuzzle) {
					int[] pattern = puzzle.getCurrentStateBlocks();
					pattern[puzzle.getBlockIndex(targetBlock)] = idBlock;
					data.pattern = pattern;
					data.shuffle_preset = null;
					update();
				}
				pDragging = false;
			}

			@Override
			public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
				super.enter(event, x, y, pointer, fromActor);
				if (pDragging) {
					if (event.getTarget().getParent() instanceof Block && pEditPuzzle) {
						targetBlock = (Block) event.getTarget().getParent();
					}
				}
			}

		});

	}

	private void puzzleMoved() {
		if (pIsMatch) {
			pLblMatchValue.setText("Y");
		} else {
			pLblMatchValue.setText("N");
		}
		pLblMovesValue.setText("" + ++pCountMoves);
	}

	private void puzzleMatch() {
		pIsMatch = true;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		pPanning = false;
		// Gdx.app.debug("DemoPuzzleView", "Pan Stop");
		return super.panStop(x, y, pointer, button);
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (!pPanning && !pEditPuzzle) {
			if (puzzle != null) {
				pIsMatch = false;
				if (deltaX > deltaY) {
					if (deltaX > Math.abs(deltaY)) {
						puzzle.move(Direction.RIGHT, moveCallback);
					} else {
						puzzle.move(Direction.UP, moveCallback);
					}
				} else {
					if (deltaY > Math.abs(deltaX)) {
						puzzle.move(Direction.DOWN, moveCallback);
					} else {
						puzzle.move(Direction.LEFT, moveCallback);
					}
				}
			}
		}
		pPanning = true;
		return super.pan(x, y, deltaX, deltaY);
	}

	protected void drawTopBarControl() {
		tbc = new TopBarControl(this);
		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_BACK, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				getScene().setView(new DemoPuzzleListView(data.id));
			}
		});

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_RESET, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				pCountMoves = 0;
				pLblMovesValue.setText("" + pCountMoves);
				update();
			}
		});

		tbc.addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_ERASE, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				new InputMessageBox(DemoPuzzleView.this, "Delete ?", new Callback<String>() {
					@Override
					public void apply(String t) {
						if (t.equals("yes")) {
							DemoPuzzles puzzles = ((DemoPuzzles) PuzzleManager.getPuzzles(PuzzleManager.DEMO));
							puzzles.delete(data.id);
							getScene().setView(new DemoPuzzleListView(data.id));
						}
					}
				});
			}
		});

		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_NEXT, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				boolean proceed = false;
				for (Entry<PuzzleData> entry : puzzles.getPuzzles()) {
					if (proceed) {
						// getScene().setView(new PuzzleView(entry.value));
						changePuzzle(entry.value);
						return;
					}
					if (data.id == entry.value.id) {
						proceed = true;
					}
				}
				// getScene().setView(new PuzzleView(puzzles.getFirst()));
				changePuzzle(puzzles.getFirst());
			}
		});
		tbc.addButton(TopBarControl.POS_RIGHT, TopBarControl.BUTTON_PREV, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				PuzzleData previous = null;
				for (Entry<PuzzleData> entry : puzzles.getPuzzles()) {
					if (data.id == entry.value.id) {
						break;
					}
					previous = entry.value;
				}
				if (previous == null) {
					// Get last puzzle
					// getScene().setView(new PuzzleView(puzzles.getLast()));
					changePuzzle(puzzles.getLast());
				} else {
					// getScene().setView(new PuzzleView(previous));
					changePuzzle(previous);
				}
			}
		});

	}

	private void changePuzzle(PuzzleData value) {
		getScene().setView(new DemoPuzzleView(value));
	}

	private int[] generatePattern(int size) {
		int[] patt = new int[size];
		size--;
		for (; size >= 0; size--) {
			patt[size] = 1111;
		}
		return patt;
	}

}
