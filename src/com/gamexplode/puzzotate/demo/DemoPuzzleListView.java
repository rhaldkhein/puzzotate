package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gamexplode.puzzotate.controls.TopBarControl;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.views.PuzzleListView;

public class DemoPuzzleListView extends PuzzleListView {

	public DemoPuzzleListView() {
		this(0);
	}

	public DemoPuzzleListView(int currentPuzzleId) {
		super(PuzzleManager.getPuzzles(PuzzleManager.DEMO), currentPuzzleId);
		// pSettings = Resource.getSettings(Config.SETTINGS_ARCADE);
	}

	@Override
	protected boolean getLock(int id) {
		return false;
	}

	@Override
	protected void itemClicked(int id) {
		super.itemClicked(id);
		getScene().setView(new DemoPuzzleView(getPuzzles().getPuzzle(id)));
	}

	@Override
	public void create() {
		super.create();
		getTopBar().addButton(TopBarControl.POS_LEFT, TopBarControl.BUTTON_INFO, new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				PuzzleData data = new PuzzleData();
				data.id = getPuzzles().getPuzzles().size + 1;
				data.type = PuzzleManager.DEMO;
				data.width = 3;
				data.height = 3;
				data.rotate_size = 2;
				data.pattern = new int[] { 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111 };
				getScene().setView(new DemoPuzzleView(data));
			}
		});
		setListName("Demo");

		// DemoPuzzles dp = (DemoPuzzles) getPuzzles();
		// dp.export();

	}

}
