package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.IntArray;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Utils;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.puzzotate.controls.Block;
import com.gamexplode.puzzotate.controls.Overlay;

public class InputBlock extends Overlay {

	private Callback<Integer> callback;
	private ScrollPane scrollPane;
	private VerticalGroup container;
	// private Group container;
	private int[] selected;
	private int size;
	private int state;

	public InputBlock(View view, Callback<Integer> callback) {
		super();
		view.addActor(this);
		this.callback = callback;
		create();
	}

	private void create() {
		setSize(600, 800);
		state = 0;
		size = 120;
		selected = new int[4];
		container = new VerticalGroup();
		scrollPane = new ScrollPane(container);
		scrollPane.setSize(getWidth() * 0.9f, getHeight() * 0.9f);
		addActor(scrollPane);
		alignChildCenter(scrollPane);
		alignCenter();
		show(false);
		selectColor();
		Button btnClose = new Button(Resource.getAtlasManager().getRegion("icon-cancel"));
		addActor(btnClose);
		btnClose.setX(getWidth() * 0.8f);
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide(false);
			}
		});

		Button btnPrev = new Button(Resource.getAtlasManager().getRegion("icon-return"));
		addActor(btnPrev);
		btnPrev.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				switch (state) {
					case 1:
						// selectBase();
						break;
					case 2:
						selectColor();
						break;
					case 3:
						// selected[0] = block.getBlockBase();
						selectBase();
						break;
					case 4:
						// selected[0] = block.getBlockBase();
						selectAngle();
						break;
					default:
						break;
				}
			}
		});

		container.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				// Gdx.app.debug("InputBlock", "" + event.getTarget().getParent());
				if (event.getTarget().getParent() instanceof Block) {
					Block block = (Block) event.getTarget().getParent();
					switch (state) {
						case 1:
							selected[1] = block.getBlockColor();
							selectBase();
							break;
						case 2:
							selected[0] = block.getBlockBase();
							selectAngle();
							break;
						case 3:
							selected[3] = block.getAngle();
							selectBolted();
							break;
						case 4:
							selected[2] = block.isBolted() ? 2 : 1;
							callback.apply(Utils.joinIntArray(new IntArray(selected), ""));
							hide(false);
							break;
						default:
							break;
					}
				}
			}
		});
	}

	private void selectColor() {
		container.clearChildren();
		state = 1;
		Block block1 = new Block(6111);
		Block block2 = new Block(6211);
		Block block3 = new Block(6311);
		Block block4 = new Block(6411);
		block1.setSize(size, size);
		block2.setSize(size, size);
		block3.setSize(size, size);
		block4.setSize(size, size);
		container.addActor(block1);
		container.addActor(block2);
		container.addActor(block3);
		container.addActor(block4);
	}

	private void selectBase() {
		container.clearChildren();
		state = 2;
		// 1000 + (100 * 1) + 11
		Block block1 = new Block(1111);
		Block block2 = new Block(2000 + (100 * selected[1]) + 11);
		Block block3 = new Block(3000 + (100 * selected[1]) + 11);
		Block block4 = new Block(4000 + (100 * selected[1]) + 11);
		Block block5 = new Block(5000 + (100 * selected[1]) + 11);
		Block block6 = new Block(6000 + (100 * selected[1]) + 11);
		Block block7 = new Block(7000 + (100 * selected[1]) + 11);
		Block block8 = new Block(8000 + (100 * selected[1]) + 11);
		block1.setSize(size, size);
		block2.setSize(size, size);
		block3.setSize(size, size);
		block4.setSize(size, size);
		block5.setSize(size, size);
		block6.setSize(size, size);
		block7.setSize(size, size);
		block8.setSize(size, size);
		container.addActor(block6);
		container.addActor(block5);
		container.addActor(block4);
		container.addActor(block3);
		container.addActor(block2);
		container.addActor(block7);
		container.addActor(block8);
		container.addActor(block1);
	}

	private void selectAngle() {
		container.clearChildren();
		state = 3;
		Block block1;
		Block block2;
		Block block3;
		Block block4;
		switch (selected[0]) {
			case 4:
			case 5:
			case 7:
				// 4 angles
				block1 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block2 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block3 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block4 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block1.setSize(size, size);
				block2.setSize(size, size);
				block3.setSize(size, size);
				block4.setSize(size, size);
				block1.setAngle(1);
				block2.setAngle(2);
				block3.setAngle(3);
				block4.setAngle(4);
				container.addActor(block1);
				container.addActor(block2);
				container.addActor(block3);
				container.addActor(block4);
				break;
			case 2:
			case 3:
				// 2 angles
				block1 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block2 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block1.setSize(size, size);
				block2.setSize(size, size);
				block1.setAngle(1);
				block2.setAngle(2);
				container.addActor(block1);
				container.addActor(block2);
				break;
			case 1:
			case 6:
			case 8:
				// 1 angles
				block1 = new Block((1000 * selected[0]) + (100 * selected[1]) + 11);
				block1.setSize(size, size);
				block1.setAngle(1);
				container.addActor(block1);
				break;
			default:
				break;
		}
	}

	private void selectBolted() {
		container.clearChildren();
		state = 4;
		// Gdx.app.debug("InputBlock", "" + selected[3]);
		Block block1 = new Block((1000 * selected[0]) + (100 * selected[1]) + (10 * 1) + 1);
		Block block2 = new Block((1000 * selected[0]) + (100 * selected[1]) + (10 * 2) + 1);
		block1.setSize(size, size);
		block2.setSize(size, size);
		block1.setAngle(selected[3]);
		block2.setAngle(selected[3]);
		container.addActor(block1);
		container.addActor(block2);
	}

}
