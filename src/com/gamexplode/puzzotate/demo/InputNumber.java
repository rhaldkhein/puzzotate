package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.puzzotate.controls.Overlay;

public class InputNumber extends Overlay {

	private Label pLabelErase;
	private Label pLabelInput;
	private Label pLabelOK;
	private Callback<String> callback;

	public InputNumber(View view, Callback<String> callback) {
		super();
		view.addActor(this);
		this.callback = callback;
		create();
	}

	private void create() {
		setSize(600, 800);
		alignCenter();
		drawDisplay();
		show(false);
	}

	private void drawDisplay() {
		LabelStyle lsWhiteOutLarge = Resource.getLabelStyle("white-outlined-large");
		// lsWhiteOutLarge.font.getData().setScale(1.5f);

		pLabelInput = new Label(" ", lsWhiteOutLarge);
		addActor(pLabelInput);
		pLabelInput.setY(getHeight() * 0.8f);
		pLabelInput.setWidth(getWidth());
		pLabelInput.setX(0);
		pLabelInput.setAlignment(Align.center);

		pLabelOK = new Label("Ok", lsWhiteOutLarge);
		addActor(pLabelOK);
		pLabelOK.setY(getHeight() * 0.1f);
		pLabelOK.setWidth(getWidth());
		pLabelOK.setX(0);
		pLabelOK.setAlignment(Align.center);
		pLabelOK.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				callback.apply(pLabelInput.getText().toString());
				InputNumber.this.remove();
			}
		});

		float w = getWidth() * 0.25f, h = w * 0.7f;
		float sX = 80, vX = sX, vY = 500;
		for (int i = 1; i < 13; i++) {
			Label lbl;
			if (i == 10) {
				lbl = new Label("0", lsWhiteOutLarge);
			} else if (i == 11) {
				lbl = new Label(".", lsWhiteOutLarge);
			} else if (i == 12) {
				lbl = new Label("<", lsWhiteOutLarge);
				pLabelErase = lbl;
			} else {
				lbl = new Label(String.valueOf(i), lsWhiteOutLarge);
			}
			lbl.setSize(w, h);
			lbl.setPosition(vX, vY);
			lbl.setAlignment(Align.center);
			vX += w;
			if (i % 3 == 0) {
				vX = sX;
				vY -= h;
			}
			addActor(lbl);
			lbl.addListener(new ClickListener() {
				@Override
				public void clicked(InputEvent event, float x, float y) {
					super.clicked(event, x, y);
					if (event.getListenerActor() == pLabelErase) {
						String str = pLabelInput.getText().toString();
						if (!str.isEmpty()) {
							pLabelInput.setText(str.substring(0, str.length() - 1));
						}
					} else {
						pLabelInput.setText("" + pLabelInput.getText() + ((Label) event.getListenerActor()).getText());
					}
				}
			});
		}
	}

}
