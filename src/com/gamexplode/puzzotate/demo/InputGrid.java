package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.IntArray;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Utils;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.puzzotate.controls.Overlay;
import com.gamexplode.puzzotate.controls.Turnstile;

public class InputGrid extends Overlay {

	private Callback<Integer> callback;
	private ScrollPane scrollPane;
	private VerticalGroup container;
	private int size;
	private int state;

	// private int angle;
	private int[] selected;

	public InputGrid(View view, Callback<Integer> callback) {
		this(view, 0, callback);
	}

	public InputGrid(View view, int type, Callback<Integer> callback) {
		super();
		view.addActor(this);
		this.callback = callback;
		selected = new int[3];
		if (type != 0) {
			IntArray arr = Utils.splitByDigit(type);
			selected[0] = arr.get(0);
			selected[1] = arr.get(1);
		}
		create();
	}

	private void create() {
		setSize(600, 800);
		alignCenter();
		size = 120;
		container = new VerticalGroup();
		scrollPane = new ScrollPane(container);
		scrollPane.setSize(getWidth() * 0.9f, getHeight() * 0.9f);
		addActor(scrollPane);
		alignChildCenter(scrollPane);
		show(false);

		if (selected[1] != 0) {
			selectAngle();
		} else {
			selectType();
		}

		Button btnClose = new Button(Resource.getAtlasManager().getRegion("icon-cancel"));
		addActor(btnClose);
		btnClose.setX(getWidth() * 0.8f);
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide(false);
			}
		});

		Button btnPrev = new Button(Resource.getAtlasManager().getRegion("icon-return"));
		addActor(btnPrev);
		btnPrev.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				switch (state) {
					case 1:
						// selectBase();
						break;
					case 2:
						selectType();
						break;
					default:
						break;
				}
			}
		});

		container.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Turnstile ts = (Turnstile) event.getTarget().getParent();
				switch (state) {
					case 1:
						// selected[1] = block.getBlockColor();
						IntArray arr = Utils.splitByDigit(ts.getTransformedId());
						selected[0] = arr.get(0);
						selected[1] = arr.get(1);
						selectAngle();
						break;
					case 2:
						selected[2] = ts.getAngle();
						callback.apply(Utils.joinIntArray(new IntArray(selected), ""));
						hide(false);
						break;
					default:
						break;
				}
			}
		});

	}

	private void selectType() {
		container.clearChildren();
		state = 1;
		// Non bolted
		Turnstile turnstile0 = new Turnstile(111);
		Turnstile turnstile1 = new Turnstile(221);
		Turnstile turnstile2 = new Turnstile(231);
		Turnstile turnstile3 = new Turnstile(241);
		Turnstile turnstile4 = new Turnstile(251);

		turnstile0.setSize(size, size);
		turnstile1.setSize(size, size);
		turnstile2.setSize(size, size);
		turnstile3.setSize(size, size);
		turnstile4.setSize(size, size);

		container.addActor(turnstile0);
		container.addActor(turnstile1);
		container.addActor(turnstile2);
		container.addActor(turnstile3);
		container.addActor(turnstile4);

		Turnstile turnstileB0 = new Turnstile(311);
		Turnstile turnstileB1 = new Turnstile(321);
		Turnstile turnstileB2 = new Turnstile(331);
		Turnstile turnstileB3 = new Turnstile(341);
		Turnstile turnstileB4 = new Turnstile(351);

		turnstileB0.setSize(size, size);
		turnstileB1.setSize(size, size);
		turnstileB2.setSize(size, size);
		turnstileB3.setSize(size, size);
		turnstileB4.setSize(size, size);

		container.addActor(turnstileB0);
		container.addActor(turnstileB1);
		container.addActor(turnstileB2);
		container.addActor(turnstileB3);
		container.addActor(turnstileB4);

	}

	private void selectAngle() {
		container.clearChildren();
		state = 2;
		Turnstile ts1;
		Turnstile ts2;
		Turnstile ts3;
		Turnstile ts4;
		switch (selected[1]) {
			case 1:
			case 2:
			case 4:
				// 4 angles
				ts1 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts2 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts3 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts4 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts1.setSize(size, size);
				ts2.setSize(size, size);
				ts3.setSize(size, size);
				ts4.setSize(size, size);
				ts1.setAngle(1);
				ts2.setAngle(2);
				ts3.setAngle(3);
				ts4.setAngle(4);
				container.addActor(ts1);
				container.addActor(ts2);
				container.addActor(ts3);
				container.addActor(ts4);
				break;
			case 3:
				// 2 angles
				ts1 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts2 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts1.setSize(size, size);
				ts2.setSize(size, size);
				ts1.setAngle(1);
				ts2.setAngle(2);
				container.addActor(ts1);
				container.addActor(ts2);
				break;
			case 5:
				// 1 angles
				ts1 = new Turnstile((100 * selected[0]) + (10 * selected[1]) + 1);
				ts1.setSize(size, size);
				ts1.setAngle(1);
				container.addActor(ts1);
				break;
			default:
				break;
		}
	}

}
