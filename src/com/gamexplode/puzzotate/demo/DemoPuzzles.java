package com.gamexplode.puzzotate.demo;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.StringBuilder;
import com.gamexplode.core.Config;
import com.gamexplode.puzzotate.data.PuzzleData;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.data.Puzzles;

public class DemoPuzzles extends Puzzles {

	private FileHandle fileDemo;
	private FileHandle fileExport;

	// private FileHandle fileDemo;

	public DemoPuzzles() {
		super(PuzzleManager.DEMO);
	}

	@Override
	protected void create() {
		super.create();

		if (Gdx.app.getType() == ApplicationType.Desktop) {
			fileDemo = Gdx.files.external("./bin/" + Config.FILE_DEMO_PUZZLES);
		} else {
			fileDemo = Gdx.files.external(Config.FILE_DEMO_PUZZLES);
		}

		if (!fileDemo.exists()) {
			fileDemo.writeString("[]", false);
		}

		JsonValue root = new JsonReader().parse(fileDemo.readString());
		JsonValue puzz;

		for (int i = root.size - 1; i >= 0; i--) {
			puzz = root.get(i);
			if (puzz.has("id")) {
				PuzzleData data = new PuzzleData();
				data.id = puzz.getInt("id");
				data.width = puzz.getInt("width");
				data.height = puzz.getInt("height");
				data.rotate_size = puzz.getInt("rsize");
				data.arcade_move_min = puzz.getInt("arc_min_move");
				data.pattern = puzz.has("pattern") && puzz.get("pattern").isArray() ? puzz.get("pattern").asIntArray() : new int[] {};
				data.shuffle_preset = puzz.has("preset") && puzz.get("preset").isArray() ? puzz.get("preset").asIntArray() : null;
				data.grid_objects = puzz.has("grid") && puzz.get("grid").isArray() ? puzz.get("grid").asIntArray() : null;
				addPuzzle(data);
			}
		}
	}

	public void save() {
		IntMap<PuzzleData> allPuzzles = getPuzzles();
		IntArray keys = allPuzzles.keys().toArray();
		keys.sort();
		Json json = new Json();
		StringBuilder sb = new StringBuilder();
		sb.append("[{");
		int l = keys.items.length - 1;
		for (int key : keys.items) {
			PuzzleData data = allPuzzles.get(key);
			sb.append("\"id\":" + data.id + ",");
			sb.append("\"width\":" + data.width + ",");
			sb.append("\"height\":" + data.height + ",");
			sb.append("\"rsize\":" + data.rotate_size + ",");
			sb.append("\"arc_min_move\":" + data.arcade_move_min + ",");
			sb.append("\"pattern\":" + json.toJson(data.pattern) + ",");
			sb.append("\"preset\":" + json.toJson(data.shuffle_preset) + ",");
			sb.append("\"grid\":" + json.toJson(data.grid_objects));
			if (l > 0) {
				sb.append("},{");
				l--;
			}
		}
		sb.append("}]");
		fileDemo.writeString(sb.toString(), false);
	}

	public void delete(int id) {
		IntMap<PuzzleData> allPuzzles = getPuzzles();
		IntArray keys = allPuzzles.keys().toArray();
		for (int key : keys.items) {
			if (allPuzzles.get(key).id == id) {
				allPuzzles.remove(key);
			}
		}
		save();
	}

	public void export() {

		String fileExportName = "export.txt";

		if (Gdx.app.getType() == ApplicationType.Desktop) {
			fileExport = Gdx.files.external("./bin/" + fileExportName);
		} else {
			fileExport = Gdx.files.external(fileExportName);
		}

		fileExport.writeString("package com.gamexplode.puzzotate.data;\n\n", false);
		fileExport.writeString("public class ArcadePuzzles extends Puzzles {\n\n", true);
		fileExport.writeString("public ArcadePuzzles() { super(PuzzleManager.ARCADE); }\n\n", true);
		fileExport.writeString("@Override\nprotected void create() {\nsuper.create();\n", true);

		IntMap<PuzzleData> allPuzzles = getPuzzles();
		IntArray keys = allPuzzles.keys().toArray();
		keys.sort();

		for (int key : keys.items) {
			PuzzleData data = allPuzzles.get(key);

			fileExport.writeString("\nPuzzleData d" + data.id + " = new PuzzleData();", true);
			fileExport.writeString("\nd" + data.id + ".id = " + data.id + ";", true);
			fileExport.writeString("\nd" + data.id + ".width = " + data.width + ";", true);
			fileExport.writeString("\nd" + data.id + ".height = " + data.height + ";", true);
			fileExport.writeString("\nd" + data.id + ".rotate_size = " + data.rotate_size + ";", true);
			fileExport.writeString("\nd" + data.id + ".arcade_move_min = " + data.arcade_move_min + ";", true);
			fileExport.writeString("\nd" + data.id + " .pattern = new int[] { " + arrToString(data.pattern) + " };", true);
			fileExport.writeString("\nd" + data.id + " .shuffle_preset = new int[] { " + arrToString(data.shuffle_preset) + " };", true);
			fileExport.writeString("\nd" + data.id + " .grid_objects = new int[] { " + arrToString(data.grid_objects) + " };", true);
			fileExport.writeString("\naddPuzzle(d" + data.id + ");\n", true);

		}

		fileExport.writeString("\n\n}\n\n}", true);

	}

	private String arrToString(int[] arr) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < arr.length; i++) {
			if (i == arr.length - 1) {
				sb.append(arr[i]);
			} else {
				sb.append(arr[i] + ", ");
			}
		}
		return sb.toString();
	}

}
