package com.gamexplode.puzzotate.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.Scene;
import com.gamexplode.core.scene.controls.Panel;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.views.HomeView;

public class MainScene extends Scene {

	public MainScene() {
		super();
		// Create universal sprite batch for the whole game
		SpriteBatch batch = new SpriteBatch();
		// Setup camera for the game
		OrthographicCamera camera = new OrthographicCamera();
		// camera.zoom = -5;
		// Create a viewport for the game
		// FitViewport viewport = new FitViewport(Config.WIDTH, Config.HEIGHT, camera);
		ExtendViewport viewport = new ExtendViewport(Config.WIDTH, Config.HEIGHT, camera);
		// Plug scene stage to scene
		setStage(new SceneStage(viewport, batch));
		// Read and load all puzzles
		// Puzzles.open();
		PuzzleManager.init();
		// Initialize game service
		Resource.getGameService().initialize("puzzotate.gamexplode.com", true);
		// Set view
		initView();
		// Capture back button
		Gdx.input.setCatchBackKey(true);

	}

	private void initView() {

		initDefaults();

		// Create background
		Image bg = new Image(Resource.getAtlasManager().getRegionFromFile("images/bg-home"));
		bg.setWidth(Config.WIDTH);
		bg.setHeight(Config.HEIGHT);
		bg.setX((getStage().getWidth() / 2) - (bg.getWidth() / 2));
		setBackgroundImage(bg);

		if (!Resource.getSettings().contains("up_pr")) {
			Resource.getSettings().setInteger("up_pr", 1);
		}

		// Show initial view
		setView(new HomeView());

	}

	private void initDefaults() {
		AtlasManager atlas = Resource.getAtlasManager();
		TextureRegion[] arrTexturesDefault = new TextureRegion[9];
		arrTexturesDefault[0] = atlas.getRegion("p-default-c1");
		arrTexturesDefault[1] = atlas.getRegion("p-default-c2");
		arrTexturesDefault[2] = atlas.getRegion("p-default-c3");
		arrTexturesDefault[3] = atlas.getRegion("p-default-c4");
		arrTexturesDefault[4] = atlas.getRegion("p-default-e1");
		arrTexturesDefault[5] = atlas.getRegion("p-default-e1");
		arrTexturesDefault[6] = atlas.getRegion("p-default-e1");
		arrTexturesDefault[7] = atlas.getRegion("p-default-e1");
		arrTexturesDefault[8] = atlas.getRegion("p-default-e1");
		Panel.addSkin(Panel.SKIN_DEFAULT, arrTexturesDefault);

		TextureRegion[] arrTexturesOverlay = new TextureRegion[9];
		arrTexturesOverlay[0] = atlas.getRegion("p-overlay-c1");
		arrTexturesOverlay[1] = atlas.getRegion("p-overlay-c2");
		arrTexturesOverlay[2] = atlas.getRegion("p-overlay-c3");
		arrTexturesOverlay[3] = atlas.getRegion("p-overlay-c4");
		arrTexturesOverlay[4] = atlas.getRegion("p-overlay-e1");
		arrTexturesOverlay[5] = atlas.getRegion("p-overlay-e2");
		arrTexturesOverlay[6] = atlas.getRegion("p-overlay-e3");
		arrTexturesOverlay[7] = atlas.getRegion("p-overlay-e4");
		arrTexturesOverlay[8] = atlas.getRegion("p-overlay-f1");
		Panel.addSkin("overlay", arrTexturesOverlay);

	}

	@Override
	public void update() {
		super.update();
	}

	@Override
	public void draw() {
		super.draw();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
