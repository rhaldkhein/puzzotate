package com.gamexplode.puzzotate.scenes;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.Scene;
import com.gamexplode.puzzotate.views.TestView;

public class TestScene extends Scene {
	public TestScene() {
		super();
		// Create universal sprite batch for the whole game
		SpriteBatch batch = new SpriteBatch();
		// Setup camera for the game
		OrthographicCamera camera = new OrthographicCamera();
		// camera.zoom = -5;
		// Create a viewport for the game
		FitViewport viewport = new FitViewport(Config.WIDTH, Config.HEIGHT, camera);
		//ExtendViewport viewport = new ExtendViewport(Config.WIDTH, Config.HEIGHT, camera);
		// Plug scene stage to scene
		setStage(new SceneStage(viewport, batch));
		// Read and load all puzzles
		// Puzzles.open();
		// Initialize game service
		Resource.getGameService().initialize("puzzotate.gamexplode.com", true);
		// Set home view
		initView();
	}
	
	private void initView() {
		setView(new TestView());
		// setView(new LoginView());
	}
}
