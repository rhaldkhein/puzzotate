package com.gamexplode.puzzotate.test;

import com.badlogic.gdx.Gdx;

public class PrintDemo {
	public void printCount() {
		try {
			for (int i = 5; i > 0; i--) {
				// Gdx.app.debug("PrintDemo", "Counter   ---   " + i);
				// System.out.println("Counter   ---   " + i);
			}
		} catch (Exception e) {
			Gdx.app.debug("PrintDemo", "Thread  interrupted.");
			// System.out.println("Thread  interrupted.");
		}
	}
}
