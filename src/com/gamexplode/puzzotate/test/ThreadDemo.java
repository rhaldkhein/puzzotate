package com.gamexplode.puzzotate.test;

import com.badlogic.gdx.Gdx;

public class ThreadDemo extends Thread {
	private Thread t;
	private String threadName;
	PrintDemo PD;

	public ThreadDemo(String name, PrintDemo pd) {
		threadName = name;
		PD = pd;
	}

	public void run() {
		synchronized (PD) {
			PD.printCount();
		}
		Gdx.app.debug("ThreadDemo", "Thread " + threadName + " exiting.");
		// System.out.println("Thread " + threadName + " exiting.");
	}

	public void start() {
		Gdx.app.debug("ThreadDemo", "Starting " + threadName);
		// System.out.println("Starting " + threadName);
		if (t == null) {
			t = new Thread(this, threadName);
			t.start();
		}
	}

}
