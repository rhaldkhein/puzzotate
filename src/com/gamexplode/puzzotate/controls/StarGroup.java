package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.controls.Group;

public class StarGroup extends Group {

	Image[] bgStars = new Image[3];
	Image[] fgStars = new Image[3];
	private int pCurrStars;
	private int pLastStars;
	private float pSpace;
	private boolean pAnim;

	public StarGroup() {
		this(0);
	}

	public StarGroup(int stars) {
		this(stars, false);
	}

	public StarGroup(int stars, boolean anim) {
		// setDebug(true);
		// pCurrStars = stars;
		pLastStars = 0;
		pAnim = anim;
		create();
		setStars(stars);
	}

	private void create() {
		AtlasManager atlas = Resource.getAtlasManager();
		bgStars[0] = new Image(atlas.getRegion("icon-star-off"));
		bgStars[1] = new Image(atlas.getRegion("icon-star-off"));
		bgStars[2] = new Image(atlas.getRegion("icon-star-off"));
		fgStars[0] = new Image(atlas.getRegion("icon-star-on"));
		fgStars[1] = new Image(atlas.getRegion("icon-star-on"));
		fgStars[2] = new Image(atlas.getRegion("icon-star-on"));
		for (int i = bgStars.length - 1; i >= 0; i--) {
			addActor(bgStars[i]);
			addActor(fgStars[i]);
			bgStars[i].setVisible(false);
			fgStars[i].setVisible(false);
			bgStars[i].setOrigin(Align.center);
			fgStars[i].setOrigin(Align.center);
			// if (i == 1) {
			// bgStars[i].setScale(1.2f);
			// fgStars[i].setScale(1.2f);
			// }
		}
		setSpace(bgStars[0].getWidth());
	}

	public int getStars() {
		return pCurrStars;
	}

	public float getStarWidth() {
		return bgStars[0].getWidth();
	}

	public float getStarHeight() {
		return bgStars[0].getHeight();
	}

	public void setSpace(float space) {
		pSpace = space;
		resize();
	}
	
	public void setAnim(boolean anim) {
		pAnim = anim;
	}

	public void setStars(int stars) {
		stars = stars > 3 ? 3 : stars;
		stars = stars < 0 ? 0 : stars;
		pLastStars = pCurrStars;
		pCurrStars = stars;
		update();
	}

	private void resize() {
		int x = 0;
		for (int i = bgStars.length - 1; i >= 0; i--) {
			bgStars[i].setX(x);
			fgStars[i].setX(x);
			x += pSpace;
		}
		// bgStars[1].setX((getWidth() / 2) - (bgStars[1].getWidth() / 2));
		// fgStars[1].setX((getWidth() / 2) - (fgStars[1].getWidth() / 2));
		setWidth(bgStars[0].getRight());
		setHeight(bgStars[0].getTop());
	}

	private void update() {
		clear();
		if (pLastStars < pCurrStars) {
			// Add stars
			int idx = 2;
			for (int i = 0; i < 3; i++) {
				if (pLastStars <= i && i < pCurrStars) {
					// Animate
					fgStars[idx].setVisible(true);
					if (pAnim) {
						fgStars[idx].setScale(0);
						fgStars[idx].addAction(Actions.scaleTo(1, 1, 0.5f, Interpolation.bounceOut));
						bgStars[idx].addAction(Actions.scaleTo(0, 0, 0.5f, Interpolation.bounceIn));
					} else {
						bgStars[idx].setVisible(false);
					}
				} else if (i < pLastStars) {
					bgStars[idx].setVisible(false);
					fgStars[idx].setVisible(true);
				}
				idx--;
			}
		} else {
			// Remove stars
			int idx = 2;
			for (int i = 0; i < 3; i++) {
				if (pCurrStars <= i && i < pLastStars) {
					// Animate
					if (pAnim) {
						fgStars[idx].setVisible(true);
						fgStars[idx].setScale(1);
						fgStars[idx].addAction(Actions.scaleTo(0, 0, 0.5f, Interpolation.bounceIn));
						
						bgStars[idx].setVisible(true);
						bgStars[idx].setScale(0);
						bgStars[idx].addAction(Actions.scaleTo(1, 1, 0.5f, Interpolation.bounceOut));
					} else {
						bgStars[idx].setScale(1);
						bgStars[idx].setVisible(true);
					}
				} else if (i < pCurrStars) {
					bgStars[idx].setVisible(false);
					fgStars[idx].setVisible(true);
				}
				idx--;
			}
		}
	}

	// private void update() {
	//
	// // Remove all foreground stars
	// clear();
	//
	// int diff = Math.abs(pLastStars - pCurrStars);
	// Gdx.app.debug("StarGroup", "" + pLastStars + " " + pCurrStars + " " + diff);
	// int idx = 2;
	// for (int i = pCurrStars - 1; i >= 0; i--) {
	// bgStars[idx].setVisible(false);
	// fgStars[idx].setVisible(true);
	// fgStars[idx].setZIndex(100);
	// if (idx <= diff) {
	// fgStars[idx].setScale(0);
	// fgStars[idx].addAction(Actions.scaleTo(1, 1, 0.5f, Interpolation.bounceOut));
	// }
	// idx--;
	// }
	//
	// }
	//
	@Override
	public void clear() {
		for (int i = bgStars.length - 1; i >= 0; i--) {
			bgStars[i].setVisible(true);
			fgStars[i].setVisible(false);
			bgStars[i].setZIndex(0);
		}
	}

}
