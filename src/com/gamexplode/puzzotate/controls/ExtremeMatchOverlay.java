package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.SceneManager;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.views.ExtremePuzzleView;

public class ExtremeMatchOverlay extends Overlay {

	// private Image fill;
	private Label labelHead;
	private Label labelMessage;
	// private Label labelBest;
	private Button btnClose;
	private Button btnNext;
	private Button btnReset;
	private int currPuzzleId;

	private static final String[] textHeads = { "Match", "Bravo", "Nice", "Good Job", "Brilliant", "Perfect" };

	// private static final RepeatAction actBlink = Actions.forever(new SequenceAction(Actions.alpha(1, 0.2f), Actions.delay(0.2f), Actions.alpha(
	// 0.3f,
	// 0.2f)));

	public ExtremeMatchOverlay() {
		// fill = new Image(Resource.getAtlasManager().getRegion("panel-2"));
		// fill.setColor(Color.GRAY);
		// fill.getColor().a = 0.75f;
		setVisible(false);
		// addActor(fill);
		// parent.addActor(this);
		create();
	}

	private void create() {

		LabelStyle lsWhiteOutlineLarge = new LabelStyle();
		lsWhiteOutlineLarge.font = Resource.getFont("white-outlined-large");

		LabelStyle lsWhiteOutline = new LabelStyle();
		lsWhiteOutline.font = Resource.getFont("white-outlined-normal");

		LabelStyle lsRedOutline = new LabelStyle();
		lsRedOutline.font = Resource.getFont("red-outlined-normal");

		labelHead = new Label("", lsWhiteOutlineLarge);
		// labelHead.setDebug(true);
		labelHead.setAlignment(Align.center);
		labelHead.setFontScale(1.1f);

		labelMessage = new Label("You completed the puzzle!", lsWhiteOutline);
		// labelMessage.setDebug(true);
		labelMessage.setAlignment(Align.center);
		labelMessage.setFontScale(1.1f);

		// labelBest = new Label("New Best Time!", lsRedOutline);
		// labelBest.setDebug(true);
		// labelBest.setAlignment(Align.center);
		// labelBest.setFontScale(1.2f);
		// labelBest.addAction(actBlink);

		AtlasManager atlas = Resource.getAtlasManager();
		btnClose = new Button(atlas.getRegion("icon-cancel"));
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide();
			}
		});
		addActor(btnClose);

		btnReset = new Button(atlas.getRegion("icon-reset"));
		btnReset.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				SceneManager manager = (SceneManager) getStage().getRoot().getUserObject();
				ExtremePuzzleView pv = (ExtremePuzzleView) manager.getCurrentScene().getView();
				pv.reset();
			}
		});
		addActor(btnReset);

		btnNext = new Button(atlas.getRegion("icon-next-word"));
		btnNext.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				SceneManager manager = (SceneManager) getStage().getRoot().getUserObject();
				manager.getCurrentScene().setView(new ExtremePuzzleView(PuzzleManager.getPuzzles(PuzzleManager.EXTREME).getNextOf(currPuzzleId)));
			}
		});
		addActor(btnNext);

		addActor(labelHead);
		addActor(labelMessage);

	}

	public void show(int time, boolean best, int puzzleId) {
		super.show();
		currPuzzleId = puzzleId;
		setSize(600, best ? 520 : 450);
		alignCenter();
		
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				Resource.getSoundManager().playSound("success");
			}
		}, 0.3f);

		labelHead.setText(getTextHead() + "!");
		labelHead.setX(getWidth() / 2);
		labelHead.setY(getHeight() * 0.8f);

		labelMessage.setX((getWidth() / 2) - (labelMessage.getWidth() / 2));
		labelMessage.setY(getHeight() * 0.5f);

		btnClose.setX((getWidth() * 0.5f) - (btnClose.getWidth()) - (getWidth() * 0.19f));
		btnClose.setY((getHeight() * 0.12f));

		btnReset.setX((getWidth() * 0.5f) - (btnReset.getWidth() * 0.7f));
		btnReset.setY((getHeight() * 0.135f));

		btnNext.setX((getWidth() * 0.5f) + (getWidth() * 0.12f));
		btnNext.setY((getHeight() * 0.12f));
		//
		// if (best) {
		// labelBest.setVisible(true);
		// labelBest.setX((getWidth() / 2) - (labelBest.getWidth() / 2));
		// labelBest.setY(getHeight() - (labelBest.getHeight() * 4.5f));
		// } else {
		// labelBest.setVisible(false);
		// }

		// setVisible(true);
		// getColor().a = 0;
		// addAction(new SequenceAction(Actions.delay(0.5f), Actions.fadeIn(0.5f)));
	}

	// @Override
	// public void setSize(float width, float height) {
	// super.setSize(width, height);
	// fill.setSize(width, height);
	// }

	private String getTextHead() {
		return textHeads[MathUtils.random(textHeads.length - 1)];
	}
}
