package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;

public class PreUpgradeOverlay extends Overlay {

	private View parentView;
	private boolean created;
	private Label labelHead;
	private Label labelMsgA;
	private UpgradeOverlay pUpgradeOverlay;

	public PreUpgradeOverlay(View view) {
		super();
		parentView = view;
		parentView.addActor(this);
		created = false;
	}

	public void setHeaders(String head, String sub) {
		if (labelHead != null) {
			labelHead.setText(head);
		}

		if (labelHead != null) {
			labelMsgA.setText(sub);
		}
	}

	private void create() {
		setSize(600, 400);
		alignCenter();

		labelHead = new Label("Upgrade Required", Resource.getLabelStyle("white-outlined-large"));
		labelHead.setAlignment(Align.center);
		labelHead.setFontScale(0.9f);
		addActor(labelHead);
		alignChildMiddle(labelHead);
		labelHead.setY(getHeight() * 0.68f);

		labelMsgA = new Label("to get more features.", Resource.getLabelStyle("white-small"));
		labelMsgA.setAlignment(Align.center);
		labelMsgA.setFontScale(0.9f);
		addActor(labelMsgA);
		alignChildMiddle(labelMsgA);
		labelMsgA.setY(getHeight() * 0.56f);

		AtlasManager atlas = Resource.getAtlasManager();
		Button btnClose = new Button(atlas.getRegion("icon-cancel"));
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide();
			}
		});

		Button btnNext = new Button(atlas.getRegion("icon-check"));
		btnNext.addListener(new ClickListener() {

			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				PreUpgradeOverlay.this.hide();
				if (pUpgradeOverlay == null) {
					pUpgradeOverlay = new UpgradeOverlay(parentView);
				}
				pUpgradeOverlay.show();
			}
		});

		btnNext.setX(getWidth() * 0.64f);
		btnNext.setY((getHeight() * 0.18f));
		addActor(btnNext);

		btnClose.setX(getWidth() * 0.18f);
		btnClose.setY((getHeight() * 0.18f));
		addActor(btnClose);

		created = true;

	}

	@Override
	public void show() {
		if (!created) {
			create();
		}
		super.show();
	}

}
