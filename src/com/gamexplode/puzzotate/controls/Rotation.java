package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.gamexplode.core.Config;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.puzzotate.data.PuzzleData;

public class Rotation extends Group {

	private Array<Integer> indexesFrom;
	private Array<Integer> indexesTo;
	private Array<Block> blocks;
	private Direction direction;
	private Puzzle puzzle;
	private PuzzleData data;

	private int errCode;
	private Array<Turnstile> errTurnstiles;
	private Array<Block> errBlocks;

	private Vector2 target;
	private boolean done;

	private float animDuration;

	/**
	 * Rotation formula
	 */
	private final static int[] rotateOperation2x2 = new int[] { 2, -1, 1, -2 };
	private final static int[] rotateOperation3x3 = new int[] { 6, 2, -2, 4, 0, -4, 2, -2, -6 };
	private final static int[] rotateOperation4x4 = new int[] { 12, 7, 2, -3, 9, 4, -1, -6, 6, 1, -4, -9, 3, -2, -7, -12 };

	public Rotation(Puzzle puzzle, PuzzleData data) {
		super();
		// setDebug(true);
		errCode = 100;
		errTurnstiles = new Array<Turnstile>();
		errBlocks = new Array<Block>();
		blocks = new Array<Block>();
		indexesFrom = new Array<Integer>();
		indexesTo = new Array<Integer>();
		done = false;
		// animDuration = 0.05f; // Default
		animDuration = Config.ROTATION_DURATION;
		// animDuration = 3f;
		this.puzzle = puzzle;
		this.data = data;
		create();
	}

	private void create() {
		setWidth((puzzle.getBlockLength() * data.rotate_size) + (puzzle.getBlockSpace() * (data.rotate_size - 1)));
		setHeight(getWidth());
		setOrigin(Align.center);
	}

	public void ready() {
		int x = 0, y = 0, i = 0;
		Block block;
		for (; i < blocks.size; i++) {
			block = blocks.get(i);
			puzzle.removeActor(block);
			addActor(block);
			block.setX(x);
			block.setY(y);
			x += (puzzle.getBlockLength() + puzzle.getBlockSpace());
			if (i != 0 && (i + 1) % data.rotate_size == 0) {
				y += (puzzle.getBlockLength() + puzzle.getBlockSpace());
				x = 0;
			}
		}
	}

	@Override
	public void clear() {
		for (Block block : blocks) {
			block.setInRotation(false);
		}
		blocks.clear();
		errTurnstiles.clear();
		super.clear();
	}

	public void setTarget(Vector2 target) {
		this.target = target;
	}

	public Vector2 getTarget() {
		return target;
	}

	public void setError(int code) {
		setError(code, false);
	}

	public void setError(int code, boolean important) {
		if (errCode == 100 || important)
			errCode = code;
	}

	// public void setError(int code, Array<Actor> actors) {
	// errCode = code;
	// errActors = actors;
	// }

	public void addErrorTurnstile(Turnstile actor) {
		errTurnstiles.add(actor);
	}

	public void addErrorBlock(Block actor) {
		errBlocks.add(actor);
	}

	public boolean error() {
		return errCode != 100;
	}

	public int getError() {
		return errCode;
	}

	public Array<Turnstile> getErrorTurnstiles() {
		return errTurnstiles;
	}

	public Array<Block> getErrorBlocks() {
		return errBlocks;
	}

	public void addBlock(Block block, int index) {
		if (blocks.size == 0) {
			setX(block.getX());
			setY(block.getY());
		}
		indexesFrom.add(index);
		blocks.add(block);
	}

	public Array<Block> getBlocks() {
		return blocks;
	}

	public void setDirection(Direction direction) {
		if (direction == Direction.UP) {
			direction = Direction.RIGHT;
		} else if (direction == Direction.DOWN) {
			direction = Direction.LEFT;
		}
		this.direction = direction;
	}

	public Direction getRotateDirection() {
		return direction;
	}

	public Array<Integer> getIndexesFrom() {
		return indexesFrom;
	}

	public Array<Integer> getIndexesTo() {
		return indexesTo;
	}

	public void rotate(boolean animate, final Action action) {
		Action rotateAction = new Action() {
			@Override
			public boolean act(float delta) {
				done = true;
				Block block;
				int[] operation;

				if (data.rotate_size == 2) {
					operation = rotateOperation2x2;
				} else if (data.rotate_size == 3) {
					operation = rotateOperation3x3;
				} else {
					operation = rotateOperation4x4;
				}

				for (int i = 0, l = blocks.size; i < l; i++) {
					block = blocks.get(i);
					indexesTo.add(indexesFrom.get(i + operation[i]));
					block.setAngle(direction == Direction.RIGHT ? block.getAngle() - 1 : block.getAngle() + 1);
					Rotation.this.getParent().addActor(block);
				}

				// For counter clockwise
				if (direction == Direction.RIGHT) {
					indexesTo.reverse();
				}
				action.act(delta);
				clear();
				remove();
				return true;
			}
		};
		if (animate) {
			addAction(Actions.sequence(	Actions.parallel(	Actions.rotateBy(direction == Direction.LEFT ? -45 : 45, animDuration),
															Actions.scaleTo(0.7f, 0.7f, animDuration)),
										Actions.parallel(	Actions.rotateBy(direction == Direction.LEFT ? -45 : 45, animDuration),
															Actions.scaleTo(1f, 1f, animDuration)),
										rotateAction));
		} else {
			addAction(Actions.sequence(Actions.rotateBy(direction == Direction.LEFT ? -90 : 90, 0), rotateAction));
		}

	}

	public boolean getDone() {
		return done;
	}

}
