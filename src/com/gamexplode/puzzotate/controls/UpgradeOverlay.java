package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.pay.Offer;
import com.badlogic.gdx.pay.OfferType;
import com.badlogic.gdx.pay.PurchaseManagerConfig;
import com.badlogic.gdx.pay.PurchaseObserver;
import com.badlogic.gdx.pay.PurchaseSystem;
import com.badlogic.gdx.pay.Transaction;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Config;
import com.gamexplode.core.IAdMob;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.Panel;

public class UpgradeOverlay extends Overlay {

	private View parentView;
	private boolean created;
	private Button btnUpgradeA;
	private Button btnUpgradeB;
	private Label lblPriceA;
	private Label lblPriceB;
	private String strAndroidPriceA;
	private String strAndroidPriceB;
	private String strIosPriceA;
	private String strIosPriceB;
	private Button btnRestore;
	private Label lblRestore;

	// private MsgBox msgBox;

	public UpgradeOverlay(View view) {
		super();
		parentView = view;
		parentView.addActor(this);
		created = false;
		strAndroidPriceA = "$ 0.99 USD";
		strAndroidPriceB = "$ 2.99 USD";
		strIosPriceA = "$ 0.99 USD";
		strIosPriceB = "$ 2.99 USD";
	}

	private void create() {
		setSize(600, 800);
		alignCenter();

		Label labelHead = new Label("Upgrade", Resource.getLabelStyle("white-outlined-large"));
		labelHead.setAlignment(Align.center);
		labelHead.setFontScale(0.9f);
		addActor(labelHead);
		alignChildMiddle(labelHead);
		labelHead.setY(getHeight() * 0.84f);

		Label labelMsgA = new Label("and unlock more features.", Resource.getLabelStyle("white-small"));
		labelMsgA.setAlignment(Align.center);
		labelMsgA.setFontScale(0.9f);
		addActor(labelMsgA);
		alignChildMiddle(labelMsgA);
		labelMsgA.setY(getHeight() * 0.79f);

		Panel pnlA = new Panel();
		pnlA.setSize(530, 200);
		addActor(pnlA);
		pnlA.alignMiddle();
		pnlA.setY(getHeight() * 0.49f);

		Panel pnlB = new Panel();
		pnlB.setSize(530, 230);
		addActor(pnlB);
		pnlB.alignMiddle();
		pnlB.setY(getHeight() * 0.18f);

		String strMsgB = "Unlimited Re-Solve (Arcade)\nUnlimited Re-Shuffle (Extreme)";
		String strMsgC = "All Above Features\nUnlock All Arcade Puzzles\nNo Advertisement";

		Label labelMsgB = new Label(strMsgB, Resource.getLabelStyle("blue-outlined-normal"));
		labelMsgB.setAlignment(Align.center);
		labelMsgB.setFontScale(0.9f);
		addActor(labelMsgB);
		alignChildMiddle(labelMsgB);
		labelMsgB.setY(getHeight() * 0.62f);

		Label labelMsgC = new Label(strMsgC, Resource.getLabelStyle("blue-outlined-normal"));
		labelMsgC.setAlignment(Align.center);
		labelMsgC.setFontScale(0.9f);
		addActor(labelMsgC);
		alignChildMiddle(labelMsgC);
		labelMsgC.setY(getHeight() * 0.3f);

		Label labelMsgD = new Label("only 4.77 $", Resource.getLabelStyle("white-small"));
		labelMsgD.setAlignment(Align.center);
		labelMsgD.setFontScale(0.9f);
		// addActor(labelMsgD);
		alignChildMiddle(labelMsgD);
		labelMsgD.setY(getHeight() * 0.25f);

		AtlasManager atlas = Resource.getAtlasManager();
		Button btnClose = new Button(atlas.getRegion("icon-cancel"));
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide();
			}
		});

		btnUpgradeA = new Button(atlas.getRegion("icon-check"));
		btnUpgradeA.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.debug("UpgradeOverlay", "Upgrading to A");
				if (PurchaseSystem.installed()) {
					lblPriceA.setText("Purchasing ...");
					btnUpgradeA.setVisible(false);
					if (Gdx.app.getType() == ApplicationType.Android) {
						PurchaseSystem.purchase(Config.GOOGLE_PRODUCT_ID_UPGRADE_A);
						// PurchaseSystem.purchase(Config.GOOGLE_TEST_PURCHASED);
					} else if (Gdx.app.getType() == ApplicationType.iOS) {
						PurchaseSystem.purchase(Config.APPLE_PRODUCT_ID_UPGRADE_A);
					}
				}
			}
		});

		btnUpgradeB = new Button(atlas.getRegion("icon-check"));
		btnUpgradeB.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.debug("UpgradeOverlay", "Upgrading to B");
				if (PurchaseSystem.installed()) {
					lblPriceB.setText("Purchasing ...");
					btnUpgradeB.setVisible(false);
					if (Gdx.app.getType() == ApplicationType.Android) {
						PurchaseSystem.purchase(Config.GOOGLE_PRODUCT_ID_UPGRADE_B);
					} else if (Gdx.app.getType() == ApplicationType.iOS) {
						PurchaseSystem.purchase(Config.APPLE_PRODUCT_ID_UPGRADE_B);
					}
				}
			}
		});

		btnRestore = new Button(atlas.getRegion("icon-restore"));
		btnRestore.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				Gdx.app.debug("UpgradeOverlay", "Restoring");
				if (PurchaseSystem.installed()) {
					lblRestore.setVisible(true);
					lblRestore.setText("Restoring ...");
					btnRestore.setVisible(false);
					PurchaseSystem.purchaseRestore();
				}
			}
		});

		btnUpgradeA.setX(getWidth() * 0.67f);
		btnUpgradeA.setY((getHeight() * 0.505f));
		addActor(btnUpgradeA);
		btnUpgradeA.setVisible(false);

		btnUpgradeB.setX(getWidth() * 0.67f);
		btnUpgradeB.setY((getHeight() * 0.19f));
		addActor(btnUpgradeB);
		btnUpgradeB.setVisible(false);

		btnClose.setX(getWidth() * 0.18f);
		btnClose.setY((getHeight() * 0.05f));
		addActor(btnClose);

		btnRestore.setY(btnClose.getY());
		btnRestore.setX(getWidth() * 0.54f);
		addActor(btnRestore);
		btnRestore.setScale(0.8f);
		btnRestore.setVisible(false);

		lblPriceA = new Label("-", Resource.getLabelStyle("white-small"));
		lblPriceA.setAlignment(Align.center);
		lblPriceA.setFontScale(0.9f);
		addActor(lblPriceA);
		alignChildMiddle(lblPriceA);
		lblPriceA.setY(getHeight() * 0.54f);

		lblPriceB = new Label("-", Resource.getLabelStyle("white-small"));
		lblPriceB.setAlignment(Align.center);
		lblPriceB.setFontScale(0.9f);
		addActor(lblPriceB);
		alignChildMiddle(lblPriceB);
		lblPriceB.setY(getHeight() * 0.225f);

		lblRestore = new Label("Restoring ...", Resource.getLabelStyle("white-small"));
		lblRestore.setAlignment(Align.center);
		lblRestore.setFontScale(0.9f);
		addActor(lblRestore);
		lblRestore.setX(getWidth() * 0.55f);
		lblRestore.setY(getHeight() * 0.085f);
		lblRestore.setVisible(false);

		// msgBox = new MsgBox(this);
		// Checking purchased
		checkPuchased();
		created = true;

	}

	private void setupPurchaseSystem() {
		// Resource.log("UpgradeOverlay", "setupPurchaseSystem " + PurchaseSystem.hasManager());
		if (PurchaseSystem.hasManager()) {
			PurchaseManagerConfig config = new PurchaseManagerConfig();
			if (Gdx.app.getType() == ApplicationType.Android) {
				// Android
				config.addOffer(new Offer().setType(OfferType.ENTITLEMENT).setIdentifier(Config.GOOGLE_PRODUCT_ID_UPGRADE_A));
				config.addOffer(new Offer().setType(OfferType.ENTITLEMENT).setIdentifier(Config.GOOGLE_PRODUCT_ID_UPGRADE_B));
				config.addStoreParam(PurchaseManagerConfig.STORE_NAME_ANDROID_GOOGLE, Config.GOOGLE_STORE_KEY);

			} else if (Gdx.app.getType() == ApplicationType.iOS) {
				// iOS
				config.addOffer(new Offer().setType(OfferType.ENTITLEMENT).setIdentifier(Config.APPLE_PRODUCT_ID_UPGRADE_A));
				config.addOffer(new Offer().setType(OfferType.ENTITLEMENT).setIdentifier(Config.APPLE_PRODUCT_ID_UPGRADE_B));
				config.addStoreParam(PurchaseManagerConfig.STORE_NAME_IOS_APPLE, Config.APPLE_SKU);
			}
			// Install observer
			lblPriceA.setText("Please Wait ...");
			lblPriceB.setText("Please Wait ...");

			PurchaseObserver observer = new PurchaseObserver() {
				@Override
				public void handleInstall() {
					// Gdx.app.log("UpgradeOverlay", "handleInstall");
					// Resource.log("UpgradeOverlay", "handleInstall");
					updatePurchaseButtons(true);
				}

				@Override
				public void handleInstallError(Throwable arg0) {
					// Gdx.app.log("Observer", "handleInstallError " + arg0.getMessage());
					// Resource.log("Observer", "handleInstallError " + arg0.getMessage());
					updatePurchaseButtons(false);
				}

				@Override
				public void handlePurchase(Transaction arg0) {
					// Gdx.app.log("Observer", "handlePurchase " + arg0.getIdentifier());
					// Resource.log("Observer", "handlePurchase " + arg0.getIdentifier());
					String id = arg0.getIdentifier();
					if (!updatePuchased(id)) {
						new MsgBoxNew(UpgradeOverlay.this, "Purchase Error", "Please try again later.");
					}
				}

				@Override
				public void handlePurchaseCanceled() {
					// Gdx.app.log("Observer", "handlePurchaseCanceled");
					// Resource.log("Observer", "handlePurchaseCanceled");
					updatePurchaseButtons(true);
					// new MsgBoxNew(UpgradeOverlay.this, "Canceled", "Purchase canceled.");
				}

				@Override
				public void handlePurchaseError(Throwable arg0) {
					// Gdx.app.log("Observer", "handlePurchaseError " + arg0.getMessage());
					// Resource.log("Observer", "handlePurchaseError " + arg0.getMessage());
					String msg = arg0.getMessage().toLowerCase();
					updatePurchaseButtons(true);
					if (msg.contains("already owned")) {
						new MsgBoxNew(UpgradeOverlay.this, "Already Owned", "Please use restore button.");
					} else {
						new MsgBoxNew(UpgradeOverlay.this, "Purchase Error", "Please try again later.");
					}
				}

				@Override
				public void handleRestore(Transaction[] arg0) {
					// Gdx.app.log("Observer", "handleRestore " + arg0.length);
					// Resource.log("Observer", "handleRestore " + arg0.length);
					updatePurchaseButtons(true);
					if (arg0.length > 0) {
						for (Transaction tran : arg0) {
							updatePuchased(tran.getIdentifier());
						}
					} else {
						new MsgBoxNew(UpgradeOverlay.this, "Restore Error", "No previous purchase found.");
					}
				}

				@Override
				public void handleRestoreError(Throwable arg0) {
					// Gdx.app.log("Observer", "handleRestoreError " + arg0.getMessage());
					// Resource.log("Observer", "handleRestoreError " + arg0.getMessage());
					updatePurchaseButtons(true);
					new MsgBoxNew(UpgradeOverlay.this, "Restore Error", "Please try again later.");
				}

			};

			try {
				PurchaseSystem.install(observer, config);
			} catch (Exception e) {
				// Gdx.app.log("UpgradeOverlay", "PurchaseSystem InstallError " + e.getMessage());
				// Resource.log("UpgradeOverlay", "PurchaseSystem InstallError " + e.getMessage());
				updatePurchaseButtons(false);
			}

		}
	}

	private void removeAds() {
		IAdMob admob = (IAdMob) Resource.getInterface("admob");
		if (admob != null)
			admob.removeAds();
	}

	private void updatePurchaseButtons(boolean visible) {
		Gdx.app.debug("UpgradeOverlay", "updatePurchaseButtons " + visible);
		btnUpgradeA.setVisible(visible);
		btnUpgradeB.setVisible(visible);
		btnRestore.setVisible(visible);
		if (Gdx.app.getType() == ApplicationType.Android) {
			lblPriceA.setText(visible ? strAndroidPriceA : strAndroidPriceA + " - Not Available");
			lblPriceB.setText(visible ? strAndroidPriceB : strAndroidPriceB + " - Not Available");
		} else if (Gdx.app.getType() == ApplicationType.iOS) {
			lblPriceA.setText(visible ? strIosPriceA : strIosPriceA + " - Not Available");
			lblPriceB.setText(visible ? strIosPriceB : strIosPriceB + " - Not Available");
		} else {
			lblPriceA.setText("Not Available");
			lblPriceB.setText("Not Available");
		}
		if (btnRestore.isVisible()) {
			lblRestore.setVisible(false);
		}
		checkPuchased();
	}

	private void checkPuchased() {
		int currLevel = Resource.getSettings().getInteger("up_pr");
		if (currLevel >= 2) {
			// Premium level 2
			lblPriceB.setText("Current Upgrade");
			btnUpgradeB.setVisible(false);
		}
		if (currLevel >= 1) {
			// Premium level 1
			lblPriceA.setText("Current Upgrade");
			btnUpgradeA.setVisible(false);
		}
		if (btnUpgradeA.isVisible() || btnUpgradeB.isVisible()) {
			btnRestore.setVisible(true);
			lblRestore.setVisible(false);
		} else {
			btnRestore.setVisible(false);
		}
	}

	private boolean updatePuchased(String id) {
		Settings settings = Resource.getSettings();
		int current = settings.getInteger("up_pr");
		boolean ret = false;

		if (id.equals(Config.GOOGLE_PRODUCT_ID_UPGRADE_A)) {
			if (current < 1) {
				settings.setInteger("up_pr", 1);
				ret = true;
			}
		} else if (id.equals(Config.GOOGLE_PRODUCT_ID_UPGRADE_B)) {
			if (current < 2) {
				settings.setInteger("up_pr", 2);
				removeAds();
				ret = true;
			}
		} else if (id.equals(Config.APPLE_PRODUCT_ID_UPGRADE_A)) {
			if (current < 1) {
				settings.setInteger("up_pr", 1);
				ret = true;
			}
		} else if (id.equals(Config.APPLE_PRODUCT_ID_UPGRADE_B)) {
			if (current < 2) {
				settings.setInteger("up_pr", 2);
				removeAds();
				ret = true;
			}
		} else {
			// Do Nothing
			// Resource.getSettings().remove("up_pr");
			ret = false;
		}

		checkPuchased();
		return ret;
	}

	// private void hidePurchaseButtons() {
	// btnUpgradeA.setVisible(false);
	// btnUpgradeB.setVisible(false);
	//
	// }

	@Override
	public void show() {
		if (!created) {
			create();
		}
		// msgBox = new MsgBox(parentView);
		updatePurchaseButtons(false);
		super.show();
		// Setup purchase system
		setupPurchaseSystem();
	}

}
