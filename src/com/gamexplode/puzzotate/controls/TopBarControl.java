package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.View;
import com.gamexplode.core.scene.controls.Button;

public class TopBarControl extends Group {

	public static final int BUTTON_BACK = 10;
	public static final int BUTTON_LEADERBOARD = 11;
	public static final int BUTTON_RESET = 12;
	public static final int BUTTON_PREV = 13;
	public static final int BUTTON_NEXT = 14;
	public static final int BUTTON_INFO = 15;
	public static final int BUTTON_ERASE = 16;
	public static final int BUTTON_FRIENDS = 17;
	public static final int BUTTON_GLOBAL = 18;
	public static final int BUTTON_PLAY = 19;

	public static final int POS_LEFT = 1;
	public static final int POS_RIGHT = 2;

	private View view;
	private HorizontalGroup groupLeft;
	private HorizontalGroup groupRight;

	private float itemWidth = 100;

	public TopBarControl(View currentView) {
		view = currentView;
		// setDebug(true);
		create();
	}

	private void create() {

		setWidth(view.getWidth());
		setHeight(view.getHeight() * 0.1f);
		setY(view.getHeight() - getHeight());

		groupLeft = new HorizontalGroup();
		groupLeft.setDebug(getDebug());
		groupLeft.setWidth(getWidth() / 2);
		groupLeft.setHeight(getHeight());
		groupLeft.padLeft(10);
		addActor(groupLeft);

		groupRight = new HorizontalGroup();
		groupRight.setDebug(getDebug());
		groupRight.setWidth(getWidth() / 2);
		groupRight.setHeight(getHeight());
		groupRight.setX(getWidth() / 2);
		groupRight.reverse();
		groupRight.padRight(10);
		addActor(groupRight);

		view.addActor(this);
	}

	public void addButton(int pos, int button, final ClickListener clickCallback) {
		// super.addActor(actor);
		Button btn = createButton(button);
		btn.addClickListener(clickCallback);
		if (pos == POS_LEFT) {
			groupLeft.addActor(btn);
		} else {
			groupRight.addActor(btn);
		}
	}

	public void addText(int pos, String text) {
		LabelStyle labelStyleLarge = new LabelStyle();
		labelStyleLarge.font = Resource.getFont("white-outlined-normal");
		Label labelText = new Label(text, labelStyleLarge);
		labelText.setFontScale(1f);
		if (pos == POS_LEFT) {
			groupLeft.addActor(labelText);
		} else {
			groupRight.addActor(labelText);
		}
	}

	public void setSpace(int pos, float space) {
		setSpace(pos, space, true);
	}

	public void setSpace(int pos, float space, boolean padEdge) {
		if (pos == POS_LEFT) {
			groupLeft.space(space);
			if (padEdge)
				groupLeft.padLeft(space);
		} else {
			groupRight.space(space);
			if (padEdge)
				groupRight.padRight(space);
		}
	}

	public void triggerButton(int pos, int index) {
		if (pos == POS_LEFT) {
			Button btn = (Button) groupLeft.getChildren().get(index);
			btn.click();
		} else {
			Button btn = (Button) groupRight.getChildren().get(index);
			btn.click();
		}
	}

	public Button getButton(int pos, int index) {
		if (pos == POS_LEFT) {
			return (Button) groupLeft.getChildren().get(index);
		} else {
			return (Button) groupRight.getChildren().get(index);
		}
	}

	private Button createButton(int button) {
		AtlasManager atlas = Resource.getAtlasManager();
		Button btn = null;
		switch (button) {
			case BUTTON_BACK:
				btn = new Button(atlas.getRegion("icon-return"));
				btn.setScale(0.9f);
				break;
			case BUTTON_LEADERBOARD:
				btn = new Button(atlas.getRegion("icon-leaderboard"));
				break;
			case BUTTON_RESET:
				btn = new Button(atlas.getRegion("icon-reset"));
				btn.setWidth(itemWidth);
				btn.setScale(1.1f);
				break;
			case BUTTON_PREV:
				btn = new Button(atlas.getRegion("icon-next"));
				btn.setWidth(itemWidth);
				break;
			case BUTTON_NEXT:
				TextureRegion t = new TextureRegion(atlas.getRegion("icon-next"));
				t.flip(true, false);
				btn = new Button(t);
				btn.setWidth(itemWidth);
				break;
			case BUTTON_INFO:
				btn = new Button(atlas.getRegion("icon-info"));
				break;
			case BUTTON_ERASE:
				btn = new Button(atlas.getRegion("icon-erase"));
				break;
			case BUTTON_FRIENDS:
				btn = new Button(atlas.getRegion("icon-friends"));
				break;
			case BUTTON_GLOBAL:
				btn = new Button(atlas.getRegion("icon-globe"));
				break;
			case BUTTON_PLAY:
				btn = new Button(atlas.getRegion("icon-play"));
				break;
			default:
				btn = null;
				break;
		}
		return btn;
	}

}
