package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Enum.Position;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Utils;

public class Turnstile extends Group {

	private final static FloatArray obsTop = new FloatArray(new float[] { 1.4f, 2.3f, 2.4f, 3.2f, 4.2f, 4.3f, 4.4f, 5.1f });
	private final static FloatArray obsBottom = new FloatArray(new float[] { 1.2f, 2.1f, 2.2f, 3.2f, 4.1f, 4.2f, 4.4f, 5.1f });
	private final static FloatArray obsLeft = new FloatArray(new float[] { 1.3f, 2.2f, 2.3f, 3.1f, 4.1f, 4.2f, 4.3f, 5.1f });
	private final static FloatArray obsRight = new FloatArray(new float[] { 1.1f, 2.1f, 2.4f, 3.1f, 4.1f, 4.3f, 4.4f, 5.1f });

	private int idOrigin;
	private int idBase;
	private int idRotation;
	private int[] type;
	private boolean[] wings; // [left, top, right, bottom]
	private int rotateCount;
	private boolean animatingError;
	private Image imgTurnstile;
	private Image imgError;

	public Turnstile(int id) {
		super();
		this.idOrigin = id;
		this.idBase = MathUtils.floor(id * 0.1f);
		this.wings = new boolean[4];
		type = splitId();
		animatingError = false;

		imgTurnstile = new Image(Resource.getAtlasManager().getRegion("turnstile-" + MathUtils.floor(id * 0.1f)));
		addActor(imgTurnstile);

		if (type[1] == 1 || type[1] == 2 || type[1] == 4) {
			rotateCount = 4;
		} else if (type[1] == 3) {
			rotateCount = 2;
		} else {
			rotateCount = 1;
		}

		// setDebug(true);
	}

	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		imgTurnstile.setSize(width, height);
		imgTurnstile.setPosition(0, 0);
	}

	public void animateError() {
		if (animatingError)
			return;
		animatingError = true;

		if (imgError == null) {
			imgError = new Image(Resource.getAtlasManager().getRegion("turnstile-" + MathUtils.floor(idOrigin * 0.1f) + "-error"));
			imgError.setSize(imgTurnstile.getWidth(), imgTurnstile.getHeight());
			imgError.setPosition(0, 0);
			imgError.getColor().a = 0.8f;
		}
		addActor(imgError);
		// final Color col = getColor().cpy();
		// setColor(Color.RED);
		// Resource.getSoundManager().playSound("switcherror");
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				// setColor(col);
				imgError.remove();
				animatingError = false;
			}
		}, 0.5f);
	}

	private int[] splitId() {
		String temp = Integer.toString(idOrigin);
		int[] newGuess = new int[temp.length()];
		for (int i = 0; i < temp.length(); i++)
			newGuess[i] = temp.charAt(i) - '0';
		return newGuess;
	}

	public void setAngle(int angle) {

		setOrigin(Align.center);
		angle = angle < 1 ? rotateCount : angle;
		angle--;
		angle = angle % rotateCount;
		angle++;
		setRotation(0);
		rotateBy(-(90 * (angle - 1)));
		idRotation = angle;

		// Reset to false
		int i = 0;

		for (i = 0; i < wings.length; i++) {
			wings[i] = false;
		}

		if (!isEmpty()) {
			int type = Utils.getDigit(idBase, 1);
			if (type == 1) {
				// [true, false, false, false]
				for (i = 0; i < wings.length; i++) {
					wings[i] = ((idRotation - 1) % 4 == i);
				}
			} else if (type == 2) {
				// [true, true, false, false]
				for (i = 0; i < wings.length; i++) {
					wings[i] = ((idRotation - 1) % 4 == i);
				}
				for (i = 0; i < wings.length; i++) {
					if (wings[i] == false) {
						wings[i] = ((idRotation) % 4 == i);
					}
				}
			} else if (type == 3) {
				// [true, true, false, false]
				for (i = 0; i < wings.length; i++) {
					wings[i] = ((idRotation - 1) % 4 == i);
				}
				for (i = 0; i < wings.length; i++) {
					if (wings[i] == false) {
						wings[i] = ((idRotation + 1) % 4 == i);
					}
				}
			} else if (type == 4) {
				// [true, true, true, false]
				for (i = 0; i < wings.length; i++) {
					wings[i] = ((idRotation - 1) % 4 == i);
				}
				for (i = 0; i < wings.length; i++) {
					if (wings[i] == false) {
						wings[i] = ((idRotation) % 4 == i);
					}
				}
				for (i = 0; i < wings.length; i++) {
					if (wings[i] == false) {
						wings[i] = ((idRotation + 1) % 4 == i);
					}
				}
			} else {
				for (i = 0; i < wings.length; i++) {
					wings[i] = true;
				}
			}
		}
	}

	public boolean hasWing(Position position) {
		switch (position) {
			case TOP:
				return wings[1];
			case BOTTOM:
				return wings[3];
			case LEFT:
				return wings[0];
			case RIGHT:
				return wings[2];
			default:
				return false;
		}
	}

	public boolean getWing(int pos) {
		if (0 <= pos && pos < 4) {
			return wings[pos];
		} else {
			return false;
		}
	}

	public boolean[] getWings() {
		return wings;
	}

	// public void resetAngle() {
	// setAngleById(idOrigin);
	// }

	public void setAngleById(int id) {
		float angle = (float) id * 0.1f;
		setAngle(MathUtils.round((angle - MathUtils.floor(angle)) * 10));
	}

	public void rotate(final int clockwise, boolean animate) {
		// float dur = animate ? 0.1f : 0;
		float dur = animate ? (Config.ROTATION_DURATION * 2) : 0;
		addAction(Actions.sequence(Actions.rotateBy(clockwise == 0 ? -90 : 90, dur), new Action() {
			@Override
			public boolean act(float delta) {
				setAngle(getAngle() + (clockwise == 0 ? 1 : -1));
				return true;
			}
		}));
	}

	public int getTransformedId() {
		return Integer.parseInt("" + idBase + idRotation);
	}

	public int getAngle() {
		return idRotation;
	}

	public boolean isChainable(Position position, Direction direction) {
		if (isObstacleTo(position)) {
			boolean clockwise = false;
			int type = Utils.getDigit(idBase, 1);
			// Always chainable or not if type is 1 or 5
			if (type == 1)
				return false;
			if (type == 5)
				return true;
			// Convert direction to clockwise or counter
			clockwise = (direction == Direction.LEFT || direction == Direction.DOWN) ? true : false;
			// Processing
			switch (position) {
				case LEFT:
					if (type == 2) {
						return getWing(clockwise ? 1 : 3);
					} else if (type == 3) {
						return getWing(0);
					} else if (type == 4) {
						return getWing(clockwise ? 1 : 3) || getWing(0);
					}
					break;
				case TOP:
					if (type == 2) {
						return getWing(clockwise ? 2 : 0);
					} else if (type == 3) {
						return getWing(1);
					} else if (type == 4) {
						return getWing(clockwise ? 2 : 0) || getWing(1);
					}
					break;
				case RIGHT:
					if (type == 2) {
						return getWing(clockwise ? 3 : 1);
					} else if (type == 3) {
						return getWing(2);
					} else if (type == 4) {
						return getWing(clockwise ? 3 : 1) || getWing(2);
					}
					break;
				case BOTTOM:
					if (type == 2) {
						return getWing(clockwise ? 0 : 2);
					} else if (type == 3) {
						return getWing(3);
					} else if (type == 4) {
						return getWing(clockwise ? 0 : 2) || getWing(3);
					}
					break;
				default:
					break;
			}
		}
		return false;
	}

	public boolean isObstacleTo(Position position) {
		if (Utils.getDigit(idOrigin, 3) == 1) {
			return false;
		} else {
			float rot = type[1] + (idRotation * 0.1f);
			switch (position) {
				case TOP:
					return obsTop.contains(rot);
				case BOTTOM:
					return obsBottom.contains(rot);
				case LEFT:
					return obsLeft.contains(rot);
				case RIGHT:
					return obsRight.contains(rot);
				default:
					// Middle and center
					return true;
			}
		}
	}

	public boolean isBolted() {
		return Utils.getDigit(idOrigin, 3) == 3;
	}

	public boolean isEmpty() {
		return Utils.getDigit(idOrigin, 3) == 1;
	}

}
