package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.gamexplode.core.scene.controls.Panel;

public class Overlay extends Panel {

	public Overlay() {
		super("overlay");
		setVisible(false);
		setAlpha(0.95f);
	}

	public void show() {
		show(true);
	}

	public void show(boolean animate) {
		if (!isVisible()) {
			setVisible(true);
			if (animate) {
				getColor().a = 0;
				addAction(new SequenceAction(Actions.delay(0.2f), Actions.fadeIn(0.2f)));
				// addAction(Actions.fadeIn(0.5f));
			}
		}
	}

	public void hide() {
		hide(true);
	}

	public void hide(boolean animate) {
		if (isVisible()) {
			if (animate) {
				addAction(new SequenceAction(Actions.fadeOut(0.3f), new Action() {
					@Override
					public boolean act(float delta) {
						setVisible(false);
						return true;
					}
				}));
			} else {
				setVisible(false);
			}
		}
	}

}
