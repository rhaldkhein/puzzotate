package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.SceneManager;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.Group;
import com.gamexplode.puzzotate.data.PuzzleManager;
import com.gamexplode.puzzotate.views.ArcadePuzzleView;

public class ArcadeMatchOverlay extends Overlay {

	private static float PITCH;
	// private static final String[] HEAD_LABELS = new String[] { "Good", "Very Good", "Perfect" };
	private Label labelHead;
	private Image[] imageStars = new Image[3];
	private Group groupStars;
	private Actor btnClose;
	private Actor btnReset;
	private Actor btnNext;
	private ArcadePuzzleView puzzleView;
	private Label labelBest;
	private Label labelNewBest;

	// private static final RepeatAction actBlink = Actions.forever(new SequenceAction(Actions.alpha(1, 0.2f), Actions.delay(0.2f), Actions.alpha(
	// 0.3f,
	// 0.2f)));

	public ArcadeMatchOverlay(ArcadePuzzleView view) {
		super();
		puzzleView = view;
		puzzleView.addActor(this);
	}

	private void create() {

		setSize(600, 600);
		alignCenter();

		groupStars = new Group();
		addActor(groupStars);
		// groupStars.setSize(200, 100);
		// groupStars.alignMiddle();

		for (int i = imageStars.length - 1; i >= 0; i--) {
			imageStars[i] = new Image(Resource.getAtlasManager().getRegion("icon-star-big-on"));
			imageStars[i].setOrigin(Align.center);
			groupStars.addActor(imageStars[i]);
		}

		labelHead = new Label("-", Resource.getLabelStyle("white-outlined-large"));
		labelHead.setAlignment(Align.center);
		labelHead.setFontScale(1f);
		addActor(labelHead);
		alignChildMiddle(labelHead);
		labelHead.setY(getWidth() * 0.8f);

		labelBest = new Label("-", Resource.getLabelStyle("white-outlined-normal"));
		labelBest.setAlignment(Align.center);
		labelBest.setFontScale(1f);
		addActor(labelBest);
		alignChildMiddle(labelBest);
		labelBest.setY(getWidth() * 0.35f);

		labelNewBest = new Label("New", Resource.getLabelStyle("red-outlined-normal"));
		labelNewBest.setAlignment(Align.center);
		labelNewBest.setFontScale(1f);
		addActor(labelNewBest);
		alignChildMiddle(labelNewBest);
		labelNewBest.setY(labelBest.getY());
		// labelNewBest.addAction(actBlink);

		AtlasManager atlas = Resource.getAtlasManager();
		btnClose = new Button(atlas.getRegion("icon-cancel"));
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide();
			}
		});

		btnReset = new Button(atlas.getRegion("icon-reset"));
		btnReset.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				SceneManager manager = (SceneManager) getStage().getRoot().getUserObject();
				ArcadePuzzleView pv = (ArcadePuzzleView) manager.getCurrentScene().getView();
				pv.reset();
			}
		});

		btnNext = new Button(atlas.getRegion("icon-next-word"));
		btnNext.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				SceneManager manager = (SceneManager) getStage().getRoot().getUserObject();
				manager.getCurrentScene().setView(new ArcadePuzzleView(PuzzleManager.getPuzzles(PuzzleManager.ARCADE)
																					.getNextOf(puzzleView.getData().id)));
			}
		});

		btnClose.setX((getWidth() * 0.5f) - (btnClose.getWidth()) - (getWidth() * 0.19f));
		btnClose.setY((getHeight() * 0.12f));
		addActor(btnClose);

		btnReset.setX((getWidth() * 0.5f) - (btnReset.getWidth() * 0.7f));
		btnReset.setY((getHeight() * 0.135f));
		addActor(btnReset);

		btnNext.setX((getWidth() * 0.5f) + (getWidth() * 0.12f));
		btnNext.setY((getHeight() * 0.12f));
		addActor(btnNext);

	}

	public void show(int stars, boolean newBest) {
		// newBest = true;
		if (labelHead == null) {
			create();
		}

		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				Resource.getSoundManager().playSound("success");
			}
		}, 0.3f);

		// labelHead.setText(HEAD_LABELS[stars - 1] + "!");
		labelHead.setText("Level Complete!");
		labelBest.setText("Best: " + Resource.getSettings(Config.SETTINGS_ARCADE).getInteger("ar_b" + puzzleView.getData().id));

		float gap = imageStars[0].getWidth() * 1f;
		float x = 0;
		for (int i = imageStars.length - 1; i >= 0; i--) {
			// imageStars[i].setY(getHeight() * 0.5f);
			if (i < stars) {
				imageStars[i].setVisible(true);
				imageStars[i].setX(x);
				x += gap;
				if (i == 0) {
					groupStars.setWidth(imageStars[0].getRight());
					groupStars.setHeight(imageStars[0].getHeight());
					groupStars.alignMiddle();
					groupStars.setYPercent(0.45f);
					groupStars.setOrigin(Align.center);
					groupStars.setScale(0.8f);
				}
			} else {
				imageStars[i].setVisible(false);
			}
		}
		// newBest = true;
		if (newBest) {
			labelBest.setX(getWidth() * 0.47f);
			labelBest.setAlignment(Align.left);
			labelNewBest.setVisible(true);
			labelNewBest.setX(getWidth() * 0.32f);
		} else {
			labelBest.setAlignment(Align.center);
			labelNewBest.setVisible(false);
			alignChildMiddle(labelBest);
		}

		for (int i = imageStars.length - 1; i >= 0; i--) {
			if (imageStars[i].isVisible()) {
				imageStars[i].getColor().a = 0;
			}
		}

		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				animateStars();
			}
		}, 0.5f);

		super.show();
	}

	private void animateStars() {
		float delay = 1f;
		PITCH = 0.9f;
		for (int i = imageStars.length - 1; i >= 0; i--) {
			if (imageStars[i].isVisible()) {
				imageStars[i].getColor().a = 1;
				imageStars[i].setScale(0f);
				imageStars[i].addAction(new SequenceAction(Actions.delay(delay), new Action() {
					@Override
					public boolean act(float delta) {
						if (ArcadeMatchOverlay.this.isVisible()) {
							Resource.getSoundManager().playSound("star", PITCH);
						}
						PITCH += 0.1f;
						return true;
					}
				}, Actions.scaleTo(1, 1, 0.5f, Interpolation.bounceOut)));
				delay += 0.5f;
			}
		}

	}

}
