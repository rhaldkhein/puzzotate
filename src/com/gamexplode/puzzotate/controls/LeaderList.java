package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;

public class LeaderList extends Group {

	private VerticalGroup list;
	private ScrollPane pane;
	private int count;

	public LeaderList() {
		super();
		// setDebug(true);
		list = new VerticalGroup();
		pane = new ScrollPane(list);
		super.addActor(pane);
		clear();
	}

	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		pane.setSize(width, height);
	}

	public void clear() {
		count = 0;
		list.clear();
	}

	public void addItem(LeaderListItem item) {
		item.setRank(++count);
		list.addActor(item);
	}

}
