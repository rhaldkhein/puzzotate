package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.controls.Button;
import com.gamexplode.core.scene.controls.Group;

public class MsgBox extends Overlay {

	private Label labelHead;
	private Label labelMsgA;
	private Group g;

	public MsgBox(Group view) {
		super();
		g = view;
		view.addActor(this);
		setSize(500, 400);
		alignCenter();
		
		labelHead = new Label("-", Resource.getLabelStyle("white-outlined-large"));
		labelHead.setAlignment(Align.center);
		labelHead.setFontScale(0.9f);
		addActor(labelHead);
		alignChildMiddle(labelHead);
		labelHead.setY(getHeight() * 0.68f);

		labelMsgA = new Label("-", Resource.getLabelStyle("white-small"));
		labelMsgA.setAlignment(Align.center);
		labelMsgA.setFontScale(0.9f);
		addActor(labelMsgA);
		alignChildMiddle(labelMsgA);
		labelMsgA.setY(getHeight() * 0.56f);
		
		AtlasManager atlas = Resource.getAtlasManager();
		Button btnClose = new Button(atlas.getRegion("icon-cancel"));
		btnClose.addListener(new ClickListener() {
			@Override
			public void clicked(InputEvent event, float x, float y) {
				super.clicked(event, x, y);
				hide();
			}
		});
		
		btnClose.setX(getWidth() * 0.18f);
		btnClose.setY((getHeight() * 0.18f));
		addActor(btnClose);
	}
	
	public void show(String head, String sub) {
		Gdx.app.debug("MsgBox", "Show " + g.hashCode());
		labelHead.setText(head);
		labelMsgA.setText(sub);
		super.show();
	}

}
