package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.BooleanArray;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectIntMap;
import com.badlogic.gdx.utils.ObjectIntMap.Entry;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Enum.Position;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Utils;
import com.gamexplode.core.scene.controls.Group;
import com.gamexplode.puzzotate.data.PuzzleData;

public class Puzzle extends Group {

	private PuzzleData data;
	private int blockLength;
	private int blockSpace;
	private Block selectedBlock;

	private IntMap<Block> mapBlocks;
	private IntMap<Turnstile> mapTurnstile;
	private float[][] blockPoints;
	private float[][] gridPoints;
	private Array<Rotation> rotations;

	private ObjectIntMap<Turnstile> rotationsTurnstile;

	private boolean cancelMove;
	private boolean headRotate;
	private boolean canMove;
	// private boolean forceShuffle;
	private boolean animate;
	private boolean shuffling;

	private Callback<Object> matchCallback;
	private Callback<Integer> statusCallback;

	// private Callback<Object> moveCallback;

	/**
	 * There is a formula for this but as for now, this is fine.
	 */
	public Puzzle(PuzzleData data) {
		super();
		// setDebug(true);
		this.data = data;
		blockSpace = 6;
		cancelMove = false;
		canMove = true;
		// forceShuffle = false;
		rotationsTurnstile = new ObjectIntMap<Turnstile>();
		animate = true;
		shuffling = false;
		setDetaultData();
		// Hide puzzle for shuffle, shown later after shuffle
		validateData();
		create();
	}

	private void setDetaultData() {
		if (data.shuffle_type == 0) {
			data.shuffle_type = Shuffler.TYPE_PERCENT;
			data.shuffle_percent = 30;
		}
	}

	private void validateData() {

		int size = data.width * data.height;

		// Validate pattern
		if (size != data.pattern.length) {
			// data.pattern = fixIntArray(data.pattern, size, 111);
			throw new Error("Pattern must be propotion to its puzzle size for pizzle " + data.id);
		}

		// Shuffle usage order. Preset first then Automate
		if (data.shuffle_preset != null) {
			// Validate shuffle pre-set and make sure it equal to pattern
			if (data.pattern.length != data.shuffle_preset.length) {
				throw new Error("Shuffle pre-set length must be equal to pattern " + data.id);
			}

			IntArray pattern = new IntArray(data.pattern);
			for (int i = pattern.size - 1; i >= 0; i--) {
				pattern.set(i, MathUtils.floor(pattern.get(i) * 0.1f));
			}
			IntArray shuffle = new IntArray(data.shuffle_preset);
			for (int i = shuffle.size - 1; i >= 0; i--) {
				shuffle.set(i, MathUtils.floor(shuffle.get(i) * 0.1f));
			}
			pattern.sort();
			shuffle.sort();
			if (!pattern.equals(shuffle)) {
				throw new Error("Suffle pre-set value must contain all items in pattern " + data.id);
			}

		} else if (data.shuffle_automate != null) {
			if (data.shuffle_automate.length == 0) {
				throw new Error("Shuffle automate must not be empty for puzzle " + data.id);
			}
		}
		// else {
		// forceShuffle = true;
		// // throw new Error("Pattern must be shuffled for pizzle " + data.id);
		// }

		// Validate or fix grid items
		if (data.grid_objects == null || data.grid_objects.length == 0) {
			data.grid_objects = new int[(data.width * data.height) + data.width + data.height + 1];
			for (int i = 0; i < data.grid_objects.length; i++) {
				data.grid_objects[i] = 111;
			}
		} else if (data.grid_objects.length != (size + data.width + data.height + 1)) {
			throw new Error("Grid items is not proportion to puzzle size for pizzle " + data.id);
		}

		// if (data.grid_objects.length != (size + data.width + data.height + 1)) {
		// throw new Error("Grid items is not proportion to puzzle size for pizzle id " + data.id);
		// }
		// shuffle.shuffle();
		// data.shuffle_preset = shuffle.toArray();

		// Validate shuffle automate

	}

	public Vector2 convertIndexToVector2(int index, int width, boolean fix) {
		if (fix && index < 0) {
			return new Vector2(-1, -1);
		}
		return new Vector2(index % width, MathUtils.floor(index / width));
	}

	public int convertVector2ToIndex(Vector2 vector, int width, int height, boolean fix) {
		if (fix && (!(-1 < vector.x && vector.x < width) || !(-1 < vector.y && vector.y < height))) {
			return -1;
		}
		return (int) (vector.y * width + vector.x);
	}

	public int getGridPointByIndex(int index, int width) {
		return index + MathUtils.floor(index / width);
	}

	public Array<Block> getBlocksByVector(Vector2 origin) {
		Array<Block> blocks = new Array<Block>();
		Block block;
		Vector2 tar = new Vector2(origin);
		int iA = data.rotate_size - 1, iB, x = (int) tar.x;
		for (; iA > -1; iA--) {
			for (iB = data.rotate_size - 1; iB > -1; iB--) {
				block = mapBlocks.get(convertVector2ToIndex(tar, data.width, data.height, true));
				if (block != null) {
					blocks.add(block);
				}
				tar.x--;
			}
			tar.y++;
			tar.x = x;
		}
		return blocks;
	}

	public Rotation getRotation(int index, Direction direction, int sizePuzzleWidth, int sizePuzzleHeight, int sizeRotate) {

		Rotation rotation = new Rotation(this, data);
		rotation.setDirection(direction);
		int gap = 0;

		Vector2 target = convertIndexToVector2(index, data.width, false);
		rotation.setTarget(target);

		switch (direction) {
			case DOWN:
				target.x -= data.rotate_size - 1;
				target.y -= data.rotate_size - 1;
				break;
			case UP:
				target.x -= data.rotate_size - 1;
				// target.y -= data.rotate_size - 1;
				break;
			case RIGHT:
				// target.x -= data.rotate_size - 1;
				// target.y -= data.rotate_size - 1;
				break;
			case LEFT:
				target.x -= data.rotate_size - 1;
				// target.y -= data.rotate_size - 1;
				break;
			default:
				break;
		}

		// Gdx.app.debug("Puzzle", "getRotation() " + target + " | " + (target.x + data.rotate_size));

		if (target.x < 0) {
			gap = (int) Math.abs(target.x);
			// rotation.setError(102, "Unable to get all rotating blocks");
			// errCode = 102;
			rotation.setError(102);
			// return rotation;
		}

		if ((target.x + data.rotate_size) > data.width) {
			Vector2 tar = new Vector2(target);
			tar.x += data.rotate_size - 1;
			rotation.setError(102);
			for (Block block : getBlocksByVector(tar)) {
				// Gdx.app.debug("rotation", "" + block);
				rotation.addErrorBlock(block);
			}
			return rotation;
		}

		int pointer = convertVector2ToIndex(target, data.width, data.height, false), iA = 0, iB = 0, pointerOrigin = pointer;

		Block pointBlock;
		int idx = -1;
		for (iA = 0; iA < data.rotate_size; iA++) {
			if (gap > 0 && pointerOrigin == pointer) {
				pointer += gap;
			}
			for (iB = 0; iB < data.rotate_size - gap; iB++) {
				idx = pointer + iA + iB;
				pointBlock = mapBlocks.get(idx);
				if (pointBlock != null) {
					// if (pointBlock == null) {
					// rotation.setError(102, "Unable to get all rotating blocks.");
					// return rotation;
					// }
					if (pointBlock.isBolted()) {
						// rotation.setError(103, "A block is bolted", pointBlock);
						// errCode = 103;
						// errActors.add(pointBlock);
						rotation.setError(103);
						rotation.addErrorBlock(pointBlock);
					}
					// if (pointBlock.getInRotation()) {
					// // rotation.setError(104, "A block is already in rotation", pointBlock);
					// // errCode = 104;
					// // errActors.add(pointBlock);
					// rotation.setError(104);
					// rotation.addErrorActor(pointBlock);
					// return rotation;
					// }
					rotation.addBlock(pointBlock, idx);
					pointBlock.setInRotation(true);
				} else {
					// rotation.setError(102, "Unable to get all rotating blocks");
					// errCode = 102;
					rotation.setError(102);
				}
			}
			pointer += data.width - 1;
		}

		// Get grid items
		if (!rotation.error()) {

			iA = 0;
			iB = 0;
			int i = 0;
			int gridPointer = getGridPointByIndex(pointerOrigin, data.width);
			int gridPointerOrigin = gridPointer;
			int[] points = new int[(data.rotate_size * data.rotate_size) + (data.rotate_size * 2) + 1];
			for (; iA <= data.rotate_size; iA++) {
				gridPointer = gridPointerOrigin + ((data.width + 1) * iA);
				iB = 0;
				for (; iB <= data.rotate_size; iB++) {
					// points.add(gridPointer);
					points[i] = gridPointer;
					gridPointer++;
					i++;
				}
			}

			// Bottom
			IntArray dynPoints = new IntArray(points);
			iA = 1;
			int[] pointsEdge = new int[data.rotate_size - 1];
			for (; iA < data.rotate_size; iA++) {
				pointsEdge[iA - 1] = points[iA];
				dynPoints.removeValue(pointsEdge[iA - 1]);
			}
			processTurnstile(pointsEdge, Position.BOTTOM, direction, rotation);

			// Top
			iA = 1;
			i = points.length - data.rotate_size - 1;
			for (; iA < data.rotate_size; iA++) {
				pointsEdge[iA - 1] = points[i + iA];
				dynPoints.removeValue(pointsEdge[iA - 1]);
				// processTurnstile(points[i + iA], Position.TOP, direction);
			}
			processTurnstile(pointsEdge, Position.TOP, direction, rotation);

			// Left
			iA = 1;
			i = data.rotate_size + 1;
			for (; iA < data.rotate_size; iA++) {
				pointsEdge[iA - 1] = points[i * iA];
				dynPoints.removeValue(pointsEdge[iA - 1]);
			}
			processTurnstile(pointsEdge, Position.LEFT, direction, rotation);

			// Right
			iA = 1;
			i = data.rotate_size + 1;
			for (; iA < data.rotate_size; iA++) {
				pointsEdge[iA - 1] = points[data.rotate_size + (i * iA)];
				dynPoints.removeValue(pointsEdge[iA - 1]);
			}
			processTurnstile(pointsEdge, Position.RIGHT, direction, rotation);

			// Inside
			dynPoints.removeRange(0, 1);
			dynPoints.removeRange(dynPoints.size - 2, dynPoints.size - 1);
			processTurnstile(dynPoints.toArray(), Position.MIDDLE, direction, rotation);

		}
		return rotation;
	}

	private void processTurnstile(int[] points, Position position, Direction direction, Rotation rotation) {
		for (int i = 0; i < points.length; i++) {
			int point = points[i];
			Turnstile ts = mapTurnstile.get(point);
			if (ts.isObstacleTo(position) && ts.isBolted()) {
				rotation.setError(105, true);
				rotation.addErrorTurnstile(ts);
			} else {
				if (position == Position.MIDDLE) {
					if (i == (float) (points.length - 1) / 2) {
						// Width center
						// Rotate this turnstile of not empty
						rotationsTurnstile.put(ts, direction == Direction.DOWN || direction == Direction.LEFT ? 0 : 1);
					} else {
						if (Utils.getDigit(ts.getTransformedId(), 3) != 1) {
							rotation.setError(108);
							rotation.addErrorTurnstile(ts);
						}
					}
				} else {
					Vector2 target = new Vector2(rotation.getTarget());
					if (ts.isChainable(position, direction)) {
						// Add another rotation
						switch (position) {
							case LEFT:
								target.x--;
								break;
							case TOP:
								target.x += data.rotate_size - 1;
								target.y += data.rotate_size;
								break;
							case RIGHT:
								target.x += data.rotate_size + 1;
								break;
							case BOTTOM:
								target.x += data.rotate_size - 1;
								target.y -= data.rotate_size;
								break;
							default:
								break;
						}

						if (target.x >= data.width || target.y >= data.width || target.x < 0 || target.y < 0) {
							rotation.setError(106);
							rotation.addErrorTurnstile(ts);
							Block b;
							Vector2 tar = new Vector2(target);
							int iA = data.rotate_size - 1, iB, x = (int) tar.x;
							for (; iA > -1; iA--) {
								for (iB = data.rotate_size - 1; iB > -1; iB--) {
									b = mapBlocks.get(convertVector2ToIndex(tar, data.width, data.height, true));
									if (b != null) {
										rotation.addErrorBlock(b);
									}
									tar.x--;
								}
								tar.y++;
								tar.x = x;
							}
							return;
						}

						headRotate = false;
						int response = rotateBlocks(convertVector2ToIndex(target, data.width, data.height, true), getOppositeDirection(direction));
						rotation.setError(response);
						if (response != 100) {
							rotation.addErrorTurnstile(ts);
						} else {
							// rotationsTurnstile
							rotationsTurnstile.put(ts, direction == Direction.DOWN || direction == Direction.LEFT ? 1 : 0);
						}
						return;
					} else {
						if (ts.isObstacleTo(position)) {
							rotationsTurnstile.put(ts, direction == Direction.DOWN || direction == Direction.LEFT ? 1 : 0);
						}
					}
				}
			}
		}
		return;
	}

	private Direction getOppositeDirection(Direction direction) {
		boolean clockwise = (direction == Direction.LEFT || direction == Direction.DOWN) ? true : false;
		if (clockwise) {
			return Direction.LEFT;
		} else {
			return Direction.UP;
		}
	}

	// private String getSpace(int space) {
	// String str = "";
	// for (int i = 0; i < space; i++) {
	// str += "-";
	// }
	// return str;
	// }

	public int rotateBlocks(int index, Direction direction) {
		// Gdx.app.debug("Rotate Blocks", "" + index + " | " + mapBlocks.get(index).getInRotation() + " | " + direction);
		// Validations
		if (mapBlocks.get(index).getInRotation()) {
			// rotateLayer--;
			return 100;
		}
		// Process
		Rotation rotation = getRotation(index, direction, data.width, data.height, data.rotate_size);
		// Gdx.app.debug("Error Check", "" + rotation.getError());
		switch (rotation.getError()) {
			case 100:
				// case 201:
				// No error
				rotations.add(rotation);
				break;
			case 102:
				if (headRotate) {
					if (animate) {
						for (final Block actor : rotation.getBlocks()) {
							actor.animateError();
						}
					}
				}
			case 103:
			case 104:
			case 105:
			case 106:
			case 107:
			case 108:
				if (animate) {
					// Gdx.app.debug("Puzzle", "Rotate Blocks | " + rotation.getBlocks().size + " | " + rotation.getErrorBlocks().size);
					for (final Block actor : rotation.getBlocks()) {
						actor.animateError();
					}
					for (final Block actor : rotation.getErrorBlocks()) {
						actor.animateError();
					}
					for (final Turnstile actor : rotation.getErrorTurnstiles()) {
						actor.animateError();
					}
				}
				rotation.clear();
				cancelMove = true;
				break;
			default:
				// Error
				cancelMove = true;
				break;
		}
		// rotateLayer--;
		return rotation.getError();
	}

	public boolean move(Direction direction, final Callback<Boolean> done) {
		boolean lMove = false;
		if (selectedBlock != null) {
			move(mapBlocks.findKey(selectedBlock, true, -1), direction, done);
			lMove = true;
		}
		selectedBlock = null;
		return lMove;
	}

	public int move(float indexAndDirection, Callback<Boolean> done) {
		int idx = MathUtils.floor(indexAndDirection);
		int dir = MathUtils.round((indexAndDirection - idx) * 10);
		return move(idx, dir == 1 ? Direction.LEFT : Direction.UP, done);
	}

	public int move(int index, Direction direction, final Callback<Boolean> done) {

		// Gdx.app.debug("Puzzle", "move() " + index + " | " + convertIndexToVector2(index, data.width, true) + " | " + direction + " | " + canMove);
		// Gdx.app.debug("Puzzle", "Move | " + canMove);

		if (!canMove)
			return 100;

		// Check and try fixing the index and direction
		Vector2 vec = convertIndexToVector2(index, data.width, true);
		boolean outOfBounds = false;
		Direction newDir = Direction.NONE;
		switch (direction) {
			case RIGHT:
				if (vec.y + data.rotate_size > data.height) {
					vec.y -= data.rotate_size - 1;
					vec.x += data.rotate_size - 1;
					newDir = Direction.LEFT;
					outOfBounds = true;
				}
				break;
			case LEFT:
				if (vec.y + data.rotate_size > data.height && vec.x - (data.rotate_size - 1) > -1) {
					vec.y -= data.rotate_size - 1;
					newDir = Direction.UP;
					outOfBounds = true;
				}
				break;
			case UP:
				if (vec.x - (data.rotate_size - 1) < 0 && vec.y + data.rotate_size <= data.height) {
					vec.x += data.rotate_size - 1;
					newDir = Direction.LEFT;
					outOfBounds = true;
				}
				break;
			case DOWN:
				if (vec.x - (data.rotate_size - 1) < 0) {
					vec.y -= data.rotate_size - 1;
					vec.x += data.rotate_size - 1;
					newDir = Direction.UP;
					outOfBounds = true;
				}
				break;
			default:
				break;
		}
		// Change move
		if (outOfBounds) {
			int newIndex = convertVector2ToIndex(vec, data.width, data.height, true);
			if (newIndex > -1) {
				move(newIndex, newDir, done);
				return 100;
			}
		}

		// Correct index and direction
		rotations.clear();
		rotationsTurnstile.clear();
		cancelMove = false;
		headRotate = true;
		final int result = rotateBlocks(index, direction);
		// Gdx.app.debug("Puzzle", "result " + result + " | " + rotations.size);
		if (animate) {
			if (result != 100) {
				Resource.getSoundManager().playSound("error4");
			} else {
				Resource.getSoundManager().playSound("rotate1");
			}
		}

		// Gdx.app.debug("Puzzle", "Cancel Move | " + cancelMove);

		if (!cancelMove) {
			for (Entry<Turnstile> entry : rotationsTurnstile) {
				entry.key.rotate(entry.value, animate);
			}
		}

		final BooleanArray ba = new BooleanArray();

		for (final Rotation rotation : rotations) {
			if (cancelMove) {
				rotation.clear();
			} else {
				addActor(rotation);
				rotation.ready();
				canMove = false;
				// Gdx.app.debug("test", "A");
				rotation.rotate(animate, new Action() {
					@Override
					public boolean act(float delta) {
						// Gdx.app.debug("test", "B");
						// Update each rotation
						// mapBlocks.
						int keyFrom, keyTo;
						IntMap<Block> container = new IntMap<Block>();
						Array<Integer> idxFrom = rotation.getIndexesFrom();
						Array<Integer> idxTo = rotation.getIndexesTo();
						// Block block;

						for (int i = 0, l = idxFrom.size; i < l; i++) {
							keyFrom = idxFrom.get(i);
							container.put(keyFrom, mapBlocks.remove(keyFrom));
						}

						for (int i = 0, l = idxTo.size; i < l; i++) {
							keyFrom = idxFrom.get(i);
							keyTo = idxTo.get(i);
							// block = container.get(keyFrom);
							// block.setAngle(rotation.getRotateDirection() == Direction.RIGHT ? block.getAngle() - 1 : block.getAngle() + 1);
							mapBlocks.put(keyTo, container.get(keyFrom));
						}

						container.clear();
						// Check arrangement
						if (rotations.indexOf(rotation, true) == rotations.size - 1) {
							arrangeBlocks();
							if (isMatch()) {
								matchCallback.apply(null);
							}
						}
						// rotations.ha
						canMove = true;
						// Gdx.app.debug("Puzzle", "" + rotations.size + " | " + ba.size);
						ba.add(true);
						if (done != null && result == 100 && rotations.size == ba.size) {
							done.apply(true);
						}
						return false;
					}
				});
				// Gdx.app.debug("test", "C");
			}
		}
		return result;
	}

	private void events() {
		addListener(new InputListener() {
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				if (event.getTarget().getParent() instanceof Block) {
					selectedBlock = (Block) event.getTarget().getParent();
				}
				return true;
			}

			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				selectedBlock = null;
			}
		});
	}

	public Block getSelectedBlock() {
		return selectedBlock;
	}

	private boolean isMatch() {
		boolean err = false;
		int hit = 0;
		for (IntMap.Entry<Block> entry : mapBlocks) {
			if (data.pattern[entry.key] != entry.value.getTransformedId()) {
				err = true;
			} else {
				hit++;
			}
		}
		if (!shuffling && statusCallback != null) {
			statusCallback.apply(MathUtils.floor(((float) hit / mapBlocks.size) * 100));
		}
		if (!err) {
			// MATCH HIT!!!
			if (!shuffling && matchCallback != null) {
				return true;
			}
		}
		return false;
	}

	private void create() {

		mapBlocks = new IntMap<Block>();
		mapTurnstile = new IntMap<Turnstile>();
		rotations = new Array<Rotation>();

		int i = 0, len = data.width * data.height;

		blockPoints = new float[len][2];
		gridPoints = new float[len + data.width + data.height + 1][2];

		// Get longest length
		int maxLength = data.width > data.height ? data.width : data.height;
		int containerLength = 0;
		if (maxLength <= 3) {
			// containerLength = 480;
			containerLength = 480;
		} else if (maxLength == 4) {
			// containerLength = 520;
			containerLength = 540;
		} else if (maxLength == 5) {
			// containerLength = 560;
			containerLength = 600;
		} else {
			// containerLength = 600;
			containerLength = 600;
		}
		setSize(containerLength, containerLength);

		// Get block length
		blockLength = (int) (getWidth() / maxLength) - blockSpace;

		setWidth((data.width * blockLength) + (blockSpace * data.width));
		setHeight((data.height * blockLength) + (blockSpace * data.height));

		// Add grid objects
		int iA = 0, iB = 0, pX = 0, pY = 0, iC = 0;
		int w = (int) (getWidth() / data.width);
		int h = (int) (getHeight() / data.height);
		for (; iA < data.height + 1; iA++) {
			for (iB = 0; iB < data.width + 1; iB++) {
				gridPoints[i][0] = pX;
				gridPoints[i][1] = pY;
				if (iA < data.height && iB < data.width) {
					blockPoints[iC][0] = pX + blockSpace / 2;
					blockPoints[iC][1] = pY + blockSpace / 2;
					iC++;
				}
				pX += w;
				i++;
			}
			pY += h;
			pX = 0;
		}

		Turnstile turnstile;
		for (i = 0, len = data.grid_objects.length; i < len; i++) {
			// turnstile = getTurnstile(data.grid_objects[i]);
			turnstile = getTurnstile(data.grid_objects_preset != null ? data.grid_objects_preset[i] : data.grid_objects[i]);
			addActor(turnstile);
			mapTurnstile.put(i, turnstile);
			turnstile.setX(gridPoints[i][0] - turnstile.getWidth() / 2);
			turnstile.setY(gridPoints[i][1] - turnstile.getHeight() / 2);
			turnstile.setOrigin(Align.center);
		}

		// Place all blocks
		Block block;
		for (i = 0, len = data.width * data.height; i < len; i++) {
			block = getBlock(data.shuffle_preset == null ? data.pattern[i] : data.shuffle_preset[i]);
			addActor(block);
			mapBlocks.put(i, block);
			block.setX(blockPoints[i][0]);
			block.setY(blockPoints[i][1]);
		}

		// Add event listeners
		events();

		// If shuffle automate
		// if (forceShuffle) {
		// // System will try to shuffle, this means that preset or automate was not set
		// // shuffle();
		// } else {
		// if(data.shuffle_preset != null){
		//
		// }
		// // if (data.shuffle_preset == null && data.shuffle_automate != null) {
		// // Gdx.app.debug("shuffle", "automate");
		// // } else {
		// // animate = true;
		// // }
		// }
	}

	public void shuffle() {
		animate = false;
		shuffling = true;
		// Gdx.app.debug("Puzzle", "Shuffle Type | " + data.shuffle_type);
		Shuffler shuffler = new Shuffler(this, data.shuffle_type);
		shuffler.setLevel(data.shuffle_level);
		shuffler.shuffle(new Callback<Object>() {
			@Override
			public void apply(Object t) {
				// Gdx.app.debug("Puzzle", "Shuffle Done");
				animate = true;
				shuffling = false;
			}
		});
	}

	public boolean isShuffling() {
		return shuffling;
	}

	private void arrangeBlocks() {
		int i = 0, l = mapBlocks.size;
		for (; i < l; i++) {
			mapBlocks.get(i).setX(blockPoints[i][0]);
			mapBlocks.get(i).setY(blockPoints[i][1]);
		}
	}

	private Turnstile getTurnstile(int id) {
		Turnstile turnstile = new Turnstile(id);
		turnstile.setSize(blockLength * 0.75f, blockLength * 0.75f);
		// turnstile.resetAngle();
		turnstile.setAngleById(id);
		return turnstile;
	}

	private Block getBlock(int id) {
		Block block = new Block(id);
		block.setSize(blockLength, blockLength);
		block.setAngleById(id);
		return block;
	}

	public int getBlockLength() {
		return blockLength;
	}

	public int getBlockSpace() {
		return blockSpace;
	}

	public PuzzleData getData() {
		return data;
	}

	public IntMap<Block> getBlockMap() {
		return mapBlocks;
	}

	public IntMap<Turnstile> getTurnstileMap() {
		return mapTurnstile;
	}

	public int[] getCurrentStateBlocks() {
		int[] state = new int[mapBlocks.size];
		int i = 0;
		for (IntMap.Entry<Block> entry : mapBlocks) {
			state[i] = entry.value.getTransformedId();
			i++;
		}
		return state;
	}

	public int[] getCurrentStateTurnstiles() {
		int[] state = new int[mapTurnstile.size];
		int i = 0;
		for (IntMap.Entry<Turnstile> entry : mapTurnstile) {
			state[i] = entry.value.getTransformedId();
			i++;
		}
		return state;
	}

	public int getBlockIndex(Block block) {
		return mapBlocks.findKey(block, true, -1);
	}

	public int getTurnstileIndex(Turnstile turnstile) {
		return mapTurnstile.findKey(turnstile, true, -1);
	}

	public void setMatchCallback(Callback<Object> callback) {
		matchCallback = callback;
	}

	public void setStatusCallback(Callback<Integer> callback) {
		statusCallback = callback;
	}

	// public void setMoveCallback(Callback<Object> callback) {
	// moveCallback = callback;
	// }

	public void setAnimate(boolean anim) {
		animate = anim;
	}

	public void setBlocksVisible(boolean visible) {
		for (IntMap.Entry<Block> entry : mapBlocks) {
			entry.value.setVisible(visible);
		}
	}

}
