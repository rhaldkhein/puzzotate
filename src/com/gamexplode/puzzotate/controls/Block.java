package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.Resource;

public class Block extends Group {

	private int idOrigin;
	private int idBase;
	private int idRotation;
	private int[] type;
	private boolean inRotation;
	private int rotateCount;
	private boolean animatingError;
	private Image imgBlock;
	private Image imgError;
	private Image imgBolt;

	public Block(int id) {
		super();
		// setDebug(true);
		// Gdx.app.debug("Block", "" + id);
		this.idOrigin = id;
		this.idBase = MathUtils.floor(id * 0.1f);
		type = splitId();
		animatingError = false;
		create();
	}

	private void create() {
		inRotation = false;
		imgBlock = new Image(Resource.getAtlasManager().getRegion("block-" + MathUtils.floor(idOrigin * 0.01f) + (type[2] == 2 ? 1 : type[2])));
		addActor(imgBlock);
		// Add bolt
		if (type[2] == 2) {
			imgBolt = new Image(Resource.getAtlasManager().getRegion("bolt"));
			addActor(imgBolt);
		}

		if (type[0] == 2 || type[0] == 3) {
			rotateCount = 2;
		} else if (type[0] == 4 || type[0] == 5 || type[0] == 7) {
			rotateCount = 4;
		} else {
			rotateCount = 1;
		}
	}

	@Override
	public void setSize(float width, float height) {
		super.setSize(width, height);
		imgBlock.setSize(width, height);
		imgBlock.setPosition(0, 0);
		if (type[2] == 2) {
			imgBolt.setSize(width * 0.8f, height * 0.8f);
			imgBolt.setPosition((width / 2) - (imgBolt.getWidth() / 2), (height / 2) - (imgBolt.getHeight() / 2));
		}
	}

	// public float getBlock() {
	// return (float) (id + (rotation * 0.1f));
	// }

	public void setAngle(int angle) {
		setOrigin(Align.center);
		angle = angle < 1 ? rotateCount : angle;
		angle--;
		angle = angle % rotateCount;
		angle++;
		setRotation(0);
		rotateBy(-(90 * (angle - 1)));
		idRotation = angle;
		// Gdx.app.debug("angle block", "" + idRotation);
	}

	public void rotate(Direction direction) {
		// if (direction == Direction.LEFT || direction == Direction.DOWN) {
		// // Clockwise
		// if (type[0] == 2 || type[0] == 3) {
		// // if(idRotation == 2)
		//
		// } else {
		//
		// }
		// } else {
		// // Counter clockwise
		// }
	}

	public void refreshAngle() {
		// setAngle(angle);
	}

	public int getAngle() {
		return idRotation;
	}

	public void animateError() {
		// actor.setColor(Color.RED);
		if (animatingError)
			return;
		animatingError = true;

		if (imgError == null) {
			imgError = new Image(Resource.getAtlasManager().getRegion("block-error"));
			imgError.setSize(imgBlock.getWidth(), imgBlock.getHeight());
			imgError.setPosition(0, 0);
			// imgError.getColor().r = 0.5f;
			// imgError.getColor().g = 0.5f;
			// imgError.getColor().b = 0.5f;
			imgError.getColor().a = 0.5f;
		}

		// final Color col = imgBlock.getColor().cpy();
		// imgBlock.setColor(Color.RED);
		addActor(imgError);
		Timer.schedule(new Timer.Task() {
			@Override
			public void run() {
				// imgBlock.setColor(col);
				imgError.remove();
				animatingError = false;
			}
		}, 0.5f);
	}

	public void setAngleById(int id) {
		// float fId = id * 0.1f;
		// idRotation = MathUtils.round((float) ((fId - Math.floor(fId)) * 10));
		// setOrigin(Align.center);
		// rotateBy(-(90 * (idRotation - 1)));
		float angle = (float) id * 0.1f;
		setAngle(MathUtils.round((angle - MathUtils.floor(angle)) * 10));
	}

	public boolean isBolted() {
		// Gdx.app.debug("tag", "" + splitId()[2]);
		return type[2] == 2;
	}

	private int[] splitId() {
		String temp = Integer.toString(idOrigin);
		int[] newGuess = new int[temp.length()];
		for (int i = 0; i < temp.length(); i++)
			newGuess[i] = temp.charAt(i) - '0';
		return newGuess;
	}

	public void setInRotation(boolean rotating) {
		inRotation = rotating;
	}

	public boolean getInRotation() {
		return inRotation;
	}

	public int getTransformedId() {
		return Integer.parseInt("" + idBase + idRotation);
	}

	public int getBlockColor() {
		return type[1];
	}

	public int getBlockBase() {
		return type[0];
	}

}
