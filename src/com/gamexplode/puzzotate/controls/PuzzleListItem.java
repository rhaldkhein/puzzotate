package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Resource;

public class PuzzleListItem extends Group {

	private int puzzleId;
	private int type;
	private Label labelIndex;
	private Label labelTime;
	private StarGroup starGroup;
	private boolean lock;
	private Image imgLock;

	public PuzzleListItem(int type) {
		this.type = type;
	}

	public void setLabel(String label) {
		labelIndex.setText(label);
	}

	public void setScore(String score) {
		if (labelTime != null) {
			labelTime.setText(score);
		}
	}

	public void setPuzzleId(int id) {
		puzzleId = id;
	}

	public int getPuzzleId() {
		return puzzleId;
	}

	public void init() {

		// setDebug(true);
		// type = 2;
		setOrigin(Align.center);

		Image bg = new Image(Resource.getAtlasManager().getRegion("listitem-" + type));
		bg.setWidth(getWidth());
		bg.setHeight(getHeight());
		addActor(bg);

		LabelStyle labelStyleA = new LabelStyle();
		labelStyleA.font = Resource.getFont("white-outlined-large");
		labelStyleA.font.getData().setScale(1f);
		labelIndex = new Label("-", labelStyleA);
		// labelIndex.setFontScale(0.9f);
		labelIndex.setAlignment(Align.center);
		labelIndex.setX((getWidth() * 0.5f) - (labelIndex.getWidth() / 2));
		labelIndex.setY((getHeight() * 0.60f) - (labelIndex.getHeight() / 2));
		addActor(labelIndex);

		LabelStyle labelStyleB = new LabelStyle();
		labelStyleB.font = Resource.getFont((type == 2 ? "black" : "white") + "-small");
		labelTime = new Label("0", labelStyleB);
		labelTime.setFontScale(0.65f);
		labelTime.setAlignment(Align.center);
		labelTime.setWidth(getWidth());
		labelTime.setX((getWidth() * 0.5f) - (labelTime.getWidth() / 2));
		labelTime.setY((getHeight() * 0.22f) - (labelTime.getHeight() / 2));
		addActor(labelTime);

		starGroup = new StarGroup(0);
		// starGroup.setDebug(true);
		starGroup.setSpace(starGroup.getStarWidth() * 0.7f);
		starGroup.setScale(0.5f);
		starGroup.setY(getHeight() * 0.78f);
		// starGroup.setX((getWidth() * 0.50f) - (starGroup.getWidth() * 0.5f));
		starGroup.setX((getWidth() * 0.15f));
		addActor(starGroup);

		setListener();
	}

	public void setStar(int stars) {
		starGroup.setStars(stars);
	}

	private void setListener() {
		addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				// isPressed = true;
				onTouchDown(event, x, y, pointer, button);
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				// isPressed = false;
				onTouchUp(event, x, y, pointer, button);
			}
		});
	}

	public void setLock(boolean lock) {
		this.lock = lock;
		if (lock) {
			if (imgLock == null) {
				imgLock = new Image(Resource.getAtlasManager().getRegion("icon-lock"));
				imgLock.setOrigin(Align.center);
				imgLock.setScale(0.8f);
				imgLock.setPosition((getWidth() / 2) - (imgLock.getWidth() / 2), (getHeight() * 0.58f) - (imgLock.getHeight() / 2));
				addActor(imgLock);
			}
			imgLock.setVisible(true);
		} else {
			if (imgLock != null)
				imgLock.setVisible(false);
		}
	}

	public boolean isLock() {
		return lock;
	}

	public void onTouchDown(InputEvent event, float x, float y, int pointer, int button) {
		setScale(0.9f);
	}

	public void onTouchUp(InputEvent event, float x, float y, int pointer, int button) {
		setScale(1);
	}

	// public StarGroup getStarGroup() {
	// return starGroup;
	// }

	public void setStarSpace(float space) {
		starGroup.setSpace(space);
		// starGroup.setX((getWidth() * 0.5f) - (starGroup.getWidth() * 0.5f));
		starGroup.setX((getWidth() * 0.15f));
	}

}
