package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Callback;
import com.gamexplode.core.Resource;

public class LeaderListItem extends Group {

	private static LabelStyle labelStyleA;
	private static LabelStyle labelStyleB;

	// private Player player;
	private int pBest;
	private int pStars;
	private Label pLabelRank;
	private String pName;
	private String pPicture;

	public LeaderListItem(String name, String picture, int stars, int best, float width, float height) {
		// this.player = player;
		this.pName = name;
		this.pPicture = picture;
		this.pStars = stars;
		this.pBest = best;
		// setDebug(true);
		setSize(width, height);
		create();
	}

	public void setRank(int rank) {
		pLabelRank.setText(String.valueOf(rank));
	}

	private void create() {

		if (labelStyleA == null) {
			labelStyleA = new LabelStyle();
			labelStyleA.font = Resource.getFont("white-outlined-normal");
		}

		if (labelStyleB == null) {
			labelStyleB = new LabelStyle();
			labelStyleB.font = Resource.getFont("blue-outlined-normal");
		}

		labelStyleA.font.getData().setScale(1);
		labelStyleB.font.getData().setScale(1);

		pLabelRank = new Label("1", labelStyleA);
		// labelRank.setDebug(true);
		pLabelRank.setSize(getWidth() * 0.12f, getHeight());
		pLabelRank.setAlignment(Align.center);
		pLabelRank.setFontScale(1f);
		addActor(pLabelRank);

		final Image img = new Image(Resource.getAtlasManager().getRegion("acc-image-dummy"));
		// img.setDebug(true);
		img.setHeight(getHeight() * 0.9f);
		img.setWidth(img.getHeight());
		img.setX(pLabelRank.getWidth());
		img.setY((getHeight() / 2) - (img.getHeight() / 2));
		addActor(img);

		Resource.getHttpClient().image(pPicture, new Callback<TextureRegion>() {
			@Override
			public void apply(TextureRegion t) {
				img.setDrawable(new TextureRegionDrawable(t));
			}
		});

		// String name = player.first_name + " " + player.last_name;
		// pName = "Ronald Kevin Villanueva";
		final Label labelName = new Label(pName, labelStyleA);
		// labelName.setDebug(true);
		float nameWidth = getWidth() * 0.42f;
		if (labelName.getTextBounds().x > nameWidth) {
			labelName.setText(pName.substring(0, Math.min(pName.length(), 12)));
		}
		labelName.setSize(nameWidth, getHeight());
		labelName.setAlignment(Align.left);
		labelName.setFontScale(0.9f);
		labelName.setX(img.getX() + img.getWidth() + 15);
		addActor(labelName);

		StarGroup starGroup = new StarGroup(pStars);
		// starGroup.setDebug(true);
		starGroup.setOrigin(Align.center);
		starGroup.setScale(0.5f);
		starGroup.setSpace(27);
		starGroup.setX(getWidth() * 0.545f);
		starGroup.setY((getHeight() * 0.47f) - (starGroup.getHeight() / 2));
		addActor(starGroup);

		final Label labelBest = new Label(formatTime(pBest), labelStyleB);
		// final Label labelBest = new Label("88.88 m", labelStyleB);
		// labelBest.setDebug(true);
		labelBest.setSize(getWidth() * 0.21f, getHeight());
		labelBest.setAlignment(Align.right);
		labelBest.setFontScale(0.9f);
		labelBest.setX(starGroup.getRight());
		// labelBest.
		addActor(labelBest);

	}

	public static String formatTime(int sec) {
		if (sec >= 3600) {
			int h = sec / 3600;
			int m = (sec % 3600) / 60;
			// Hours
			return String.format("%d.%02d h", h, m);
		} else if (sec >= 60) {
			int m = (sec % 3600) / 60;
			int s = sec % 60;
			// Minutes
			return String.format("%d.%02d m", m, s);
		} else {
			int s = sec % 60;
			// Seconds
			return s + " s";
		}
	}

}
