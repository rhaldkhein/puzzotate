package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Resource;

public class Loading extends Group {

	private Image img;
	private RepeatAction act;
	private Label label;

	public Loading() {
		create();
	}

	private void create() {

		img = new Image(Resource.getAtlasManager().getRegion("image-icon-loading"));
		img.setOrigin(Align.center);
		act = Actions.forever(Actions.rotateBy(360, 3));
		img.addAction(act);
		setSize(img.getWidth(), img.getHeight());
		addActor(img);

		LabelStyle lsWhiteOutlined = new LabelStyle();
		lsWhiteOutlined.font = Resource.getFont("white-small");
		lsWhiteOutlined.font.getData().setScale(1);

		label = new Label("Loading...", lsWhiteOutlined);
		// label.setDebug(true);
		label.setFontScale(1.2f);
		label.setX((getWidth() / 2) - (label.getWidth() / 2));
		label.setY(0 - label.getHeight() - (getWidth() * 0.5f));
		label.setAlignment(Align.center);
		addActor(label);

	}

	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
	}

	public void setText(String text) {
		label.setText(text);
	}

}
