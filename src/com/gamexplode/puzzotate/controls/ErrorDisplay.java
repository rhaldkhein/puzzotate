package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.Align;
import com.gamexplode.core.Resource;

public class ErrorDisplay extends Group {
	
	private Image img;
	private Label label;

	public ErrorDisplay() {
		create();
		// setDebug(true);
	}

	private void create() {

		img = new Image(Resource.getAtlasManager().getRegion("image-cloude-error"));
		img.setOrigin(Align.center);
		setSize(img.getWidth(), img.getHeight());
		img.setY(img.getX() + (getWidth() * 0.2f));
		addActor(img);

		LabelStyle lsWhite = new LabelStyle();
		lsWhite.font = Resource.getFont("white-small");
		lsWhite.font.getData().setScale(1);

		label = new Label("Error", lsWhite);
		// label.setDebug(true);
		label.setFontScale(1f);
		label.setX((getWidth() / 2) - (label.getWidth() / 2));
		label.setY(0 - label.getHeight() - (getWidth() * 0.2f));
		label.setAlignment(Align.center);
		addActor(label);

	}

	public void setText(String text) {
		label.setText(text);
	}
}
