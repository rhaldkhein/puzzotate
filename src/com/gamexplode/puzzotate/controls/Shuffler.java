package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.IntMap;
import com.gamexplode.core.Callback;
import com.gamexplode.puzzotate.data.PuzzleData;

public class Shuffler {

	public static final int TYPE_RANDOM = 1;
	public static final int TYPE_COMPLEX = 2;
	public static final int TYPE_PERCENT = 3;

	private Puzzle pPuzzle;
	private int pLevel;
	private PuzzleData pData;
	private int[] pIndexes;
	private int[] pPattern;
	private int pSize;
	private int pCount;
	private int pType;
	private int pLimit;
	private IntMap<Block> mapPuzzleBlocks;
	private int pPreShuffleCount;
	private boolean pDone;

	private int lastIndex;
	private int lastDirection; // 1 is LEFT others RIGHT

	public Shuffler(Puzzle puzzle) {
		this(puzzle, TYPE_RANDOM);
	}

	public Shuffler(Puzzle puzzle, int type) {
		this.pPuzzle = puzzle;
		pData = puzzle.getData();
		pSize = pData.width * pData.height;
		// Defaults;
		pLevel = 2;
		pType = type;
		mapPuzzleBlocks = puzzle.getBlockMap();
		create();
	}

	private void create() {
		IntArray tmpIndexes = new IntArray();
		for (int i = 0; i < pSize; i++) {
			isOutOfBounds(i);
			if (!isOutOfBounds(i)) {
				tmpIndexes.add(i);
			}
		}
		pIndexes = tmpIndexes.toArray();
		pPattern = pData.pattern;
	}

	private boolean isOutOfBounds(int index) {
		int left = index - (pData.rotate_size - 1);
		if (left >= 0) {
			if ((MathUtils.floor(index / pData.width) == MathUtils.floor(left / pData.width))
					&& (index < (pData.width * pData.height) - (pData.width * (pData.rotate_size - 1)))) {
				return false;
			}
		}
		return true;
	}

	private void move(final Callback<?> callback) {
		if (pLimit < 1) {
			if (!pDone && callback != null) {
				callback.apply(null);
				pDone = true;
			}
			return;
		}
		Gdx.app.postRunnable(new Runnable() {
			@Override
			public void run() {
				if (!pDone) {
					// Pre shuffle it first
					if (pPreShuffleCount > 0) {
						int resPre = pPuzzle.move(random(), new Callback<Boolean>() {
							@Override
							public void apply(Boolean t) {
								move(callback);
							}
						});
						if (resPre == 100) {
							pPreShuffleCount--;
						} else {
							move(callback);
						}
					} else {
						// We need to limit the shuffle to avoid infinite shuffle.
						pLimit--;
						if (pType == TYPE_COMPLEX) {
							int resCom = pPuzzle.move(random(), new Callback<Boolean>() {
								@Override
								public void apply(Boolean t) {
									if (!isComplex()) {
										move(callback);
									} else {
										if (callback != null) {
											callback.apply(null);
											pDone = true;
										}
									}
								}
							});
							if (resCom != 100) {
								move(callback);
							}
						} else if (pType == TYPE_PERCENT) {
							int resCom = pPuzzle.move(random(), new Callback<Boolean>() {
								@Override
								public void apply(Boolean t) {
									int per = getPercent();
									if (0 < per && per < pData.shuffle_percent) {
										if (callback != null) {
											callback.apply(null);
											pDone = true;
										}
									} else {
										move(callback);
									}
								}
							});
							if (resCom != 100) {
								move(callback);
							}
						} else if (pType == TYPE_RANDOM) {
							int res = pPuzzle.move(random(), new Callback<Boolean>() {
								@Override
								public void apply(Boolean t) {
									move(callback);
								}
							});
							if (pCount > 0) {
								if (res == 100) {
									pCount--;
								} else {
									move(callback);
								}
							} else {
								if (callback != null) {
									callback.apply(null);
									pDone = true;
								}
							}
						} else {
							throw new Error("Unable to recognize shuffle type for puzzle " + pData.id);
						}
					}
				}
			}
		});
	}

	public void setLevel(int level) {
		this.pLevel = level == 0 ? 2 : level;
	}

	public void setType(int type) {
		pType = type;
	}

	public void shuffle(final Callback<?> callback) {
		pCount = (pSize * pLevel) + 1;
		lastIndex = -1;
		lastDirection = 0;
		pLimit = 50; // We need to limit the shuffle to avoid infinite shuffle.
		pPreShuffleCount = 25;
		pDone = false;
		move(callback);
	}

	private float random() {
		int rIdx;
		int rDir;
		do {
			rIdx = pIndexes[MathUtils.random(pIndexes.length - 1)];
			rDir = MathUtils.randomBoolean() ? 1 : 2;
		} while (lastIndex == rIdx && lastDirection != rDir);
		lastIndex = rIdx;
		lastDirection = rDir;
		return (float) (rIdx + (rDir * 0.1));
	}

	private boolean isComplex() {
		// pPattern
		int hit = 0;
		for (IntMap.Entry<Block> entry : mapPuzzleBlocks) {
			if (pPattern[entry.key] == entry.value.getTransformedId()) {
				hit++;
			}
			if (hit > pData.nmb) {
				return false;
			}
		}
		return true;
	}

	private int getPercent() {
		int hit = 0;
		for (IntMap.Entry<Block> entry : mapPuzzleBlocks) {
			if (pPattern[entry.key] == entry.value.getTransformedId()) {
				hit++;
			}
		}
		return MathUtils.floor(((float) hit / mapPuzzleBlocks.size) * 100);
	}

}
