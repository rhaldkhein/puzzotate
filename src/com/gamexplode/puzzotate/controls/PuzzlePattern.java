package com.gamexplode.puzzotate.controls;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.IntArray;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Utils;
import com.gamexplode.core.scene.controls.Group;
import com.gamexplode.puzzotate.data.PuzzleData;

public class PuzzlePattern extends Group {

	private PuzzleData data;
	private int blockLength;

	public PuzzlePattern(PuzzleData data) {
		super();
		this.data = data;
		setSize(220, 220);
		validateData();
		create();
	}

	private void validateData() {
		int size = data.width * data.height;
		// Validate pattern
		if (size != data.pattern.length) {
			// data.pattern = fixIntArray(data.pattern, size, 111);
			throw new Error("Pattern must be propotion to its puzzle size for pizzle " + data.id);
		}
	}

	private void create() {
		int x = 0, y = 0, sx = 0, i = 0, len = data.width * data.height;
		float gap;
		// Get longest length
		int maxLength = data.width > data.height ? data.width : data.height;
		// Get block length
		blockLength = (int) (getWidth() / maxLength);
		// Center blocks
		if (data.width > data.height) {
			y = (int) ((getHeight() / 2) - ((blockLength * data.height) / 2));
		} else {
			sx = (int) ((getWidth() / 2) - ((blockLength * data.width) / 2));
			x = sx;
		}
		// Place all blocks
		gap = blockLength * 0f;
		blockLength = blockLength - (int) gap;
		Image block;
		for (; i < len; i++) {
			block = getBlock(data.pattern[i]);
			addActor(block);
			block.setX(getX() + x);
			block.setY(y);
			x += blockLength + gap;
			if (i != 0 && (i + 1) % data.width == 0) {
				y += blockLength + gap;
				x = sx;
			}
		}
	}

	private Image getBlock(int id) {
		IntArray ids = Utils.splitByDigit(id);
		ids.set(2, 1);
		int idx = Utils.joinIntArray(ids, "");
		Image block = new Image(Resource.getAtlasManager().getRegion("pattern-block-" + MathUtils.floor(idx * 0.1f)));
		block.setSize(blockLength, blockLength);
		float fId = id * 0.1f;
		int rotation = MathUtils.round((float) ((fId - Math.floor(fId)) * 10));
		block.setOrigin(Align.center);
		block.rotateBy(-(90 * (rotation - 1)));
		return block;
	}

}
