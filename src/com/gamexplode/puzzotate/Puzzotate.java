package com.gamexplode.puzzotate;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.pay.PurchaseSystem;
import com.badlogic.gdx.utils.Timer;
import com.gamexplode.core.Config;
import com.gamexplode.core.IAdMob;
import com.gamexplode.core.IBase;
import com.gamexplode.core.Resource;
import com.gamexplode.core.scene.SceneManager;
import com.gamexplode.puzzotate.scenes.MainScene;

public class Puzzotate extends ApplicationAdapter {

	private SceneManager sceneManager;

	public Puzzotate(IBase... interfaces) {
		Resource.initialize();
		for (IBase i : interfaces) {
			Resource.addInterface(i.getName(), i);
		}
	}

	@Override
	public void create() {

		sceneManager = new SceneManager();
		sceneManager.setBackgroundColor(new Color(0.3f, 0.3f, 0.3f, 1));
		sceneManager.setScene(new MainScene());
		// sceneManager.setBackgroundColor(new Color(0.1f, 0.1f, 0.1f, 1));
		// sceneManager.setScene(new TestScene());

		if (Config.PROD) {
			if (Resource.getSettings().getInteger("up_pr") <= 1) {
				Timer.schedule(new Timer.Task() {
					@Override
					public void run() {
						// Load up ads
						IAdMob admob = (IAdMob) Resource.getInterface("admob");
						if (admob != null) {
							admob.setGender(Resource.getSettings().getString("gd"));
							admob.loadAds();
						}
					}
				}, 1);
			}
		}

	}

	@Override
	public void render() {
		sceneManager.render();
	}

	@Override
	public void dispose() {
		sceneManager.dispose();
		PurchaseSystem.dispose();
	}

	@Override
	public void pause() {
		sceneManager.pause();
	}

	@Override
	public void resume() {
		sceneManager.resume();
	}

	@Override
	public void resize(int width, int height) {
		sceneManager.resize(width, height);
	}

}
