package com.gamexplode.puzzotate.data;

import com.badlogic.gdx.utils.IntMap;
import com.gamexplode.puzzotate.demo.DemoPuzzles;

public class PuzzleManager {
	
	// DO NOT CHANGE THE VALUE! Because it is used for settings.
	public final static int ARCADE = 111;
	public final static int EXTREME = 112;
	public final static int DEMO = 113;

	private static IntMap<Puzzles> mapPuzzles;
	
	public static void init() {
		mapPuzzles = new IntMap<Puzzles>();
	}

	private static Puzzles loadPuzzles(int typePuzzle) {
		Puzzles puzzles;
		switch (typePuzzle) {
			case ARCADE:
				puzzles = new ArcadePuzzles();
				break;
			case EXTREME:
				puzzles = new ExtremePuzzles();
				break;
			case DEMO:
				puzzles = new DemoPuzzles();
				break;
			default:
				throw new Error("Unable to determine puzzles type.");
		}
		mapPuzzles.put(typePuzzle, puzzles);
		return puzzles;
	}

	public static Puzzles getPuzzles(int typePuzzle) {
		Puzzles puzzles = mapPuzzles.get(typePuzzle);
		if (puzzles == null) {
			puzzles = loadPuzzles(typePuzzle);
		}
		return puzzles;
	}

}
