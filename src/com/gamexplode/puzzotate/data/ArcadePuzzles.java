package com.gamexplode.puzzotate.data;

public class ArcadePuzzles extends Puzzles {

	public ArcadePuzzles() {
		super(PuzzleManager.ARCADE);
	}

	@Override
	protected void create() {
		super.create();

		PuzzleData d1 = new PuzzleData();
		d1.id = 1;
		d1.width = 3;
		d1.height = 2;
		d1.rotate_size = 2;
		d1.arcade_move_min = 1;
		d1.pattern = new int[] { 6111, 6111, 1111, 6111, 6111, 1111 };
		d1.shuffle_preset = new int[] { 6111, 1111, 1111, 6111, 6111, 6111 };
		d1.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d1);

		PuzzleData d2 = new PuzzleData();
		d2.id = 2;
		d2.width = 3;
		d2.height = 2;
		d2.rotate_size = 2;
		d2.arcade_move_min = 1;
		d2.pattern = new int[] { 6311, 6311, 6311, 1111, 1111, 1111 };
		d2.shuffle_preset = new int[] { 6311, 1111, 6311, 6311, 1111, 1111 };
		d2.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d2);

		PuzzleData d3 = new PuzzleData();
		d3.id = 3;
		d3.width = 3;
		d3.height = 2;
		d3.rotate_size = 2;
		d3.arcade_move_min = 1;
		d3.pattern = new int[] { 6411, 1111, 6411, 6411, 1111, 6411 };
		d3.shuffle_preset = new int[] { 1111, 1111, 6411, 6411, 6411, 6411 };
		d3.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d3);

		PuzzleData d4 = new PuzzleData();
		d4.id = 4;
		d4.width = 3;
		d4.height = 2;
		d4.rotate_size = 2;
		d4.arcade_move_min = 2;
		d4.pattern = new int[] { 1111, 6211, 6211, 1111, 1111, 6211 };
		d4.shuffle_preset = new int[] { 1111, 1111, 6211, 6211, 1111, 6211 };
		d4.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d4);

		PuzzleData d5 = new PuzzleData();
		d5.id = 5;
		d5.width = 3;
		d5.height = 2;
		d5.rotate_size = 2;
		d5.arcade_move_min = 2;
		d5.pattern = new int[] { 1111, 6111, 1111, 6111, 6111, 6111 };
		d5.shuffle_preset = new int[] { 1111, 6111, 6111, 6111, 1111, 6111 };
		d5.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d5);

		PuzzleData d6 = new PuzzleData();
		d6.id = 6;
		d6.width = 3;
		d6.height = 2;
		d6.rotate_size = 2;
		d6.arcade_move_min = 2;
		d6.pattern = new int[] { 1111, 6311, 6311, 1111, 6311, 6311 };
		d6.shuffle_preset = new int[] { 6311, 6311, 6311, 1111, 6311, 1111 };
		d6.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d6);

		PuzzleData d7 = new PuzzleData();
		d7.id = 7;
		d7.width = 2;
		d7.height = 3;
		d7.rotate_size = 2;
		d7.arcade_move_min = 2;
		d7.pattern = new int[] { 6411, 1111, 6411, 6411, 6411, 1111 };
		d7.shuffle_preset = new int[] { 6411, 6411, 1111, 1111, 6411, 6411 };
		d7.grid_objects = new int[] { 111, 111, 111, 111, 113, 111, 111, 113, 111, 111, 111, 111 };
		addPuzzle(d7);

		PuzzleData d8 = new PuzzleData();
		d8.id = 8;
		d8.width = 3;
		d8.height = 3;
		d8.rotate_size = 2;
		d8.arcade_move_min = 3;
		d8.pattern = new int[] { 6111, 6111, 6111, 6111, 6111, 6111, 1111, 1111, 1111 };
		d8.shuffle_preset = new int[] { 6111, 6111, 1111, 1111, 6111, 6111, 6111, 6111, 1111 };
		d8.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d8);

		PuzzleData d9 = new PuzzleData();
		d9.id = 9;
		d9.width = 3;
		d9.height = 3;
		d9.rotate_size = 2;
		d9.arcade_move_min = 3;
		d9.pattern = new int[] { 6211, 1111, 1111, 6211, 6211, 1111, 6211, 6211, 6211 };
		d9.shuffle_preset = new int[] { 6211, 1111, 6211, 1111, 6211, 6211, 6211, 6211, 1111 };
		d9.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 111, 111, 111, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d9);

		PuzzleData d10 = new PuzzleData();
		d10.id = 10;
		d10.width = 3;
		d10.height = 3;
		d10.rotate_size = 2;
		d10.arcade_move_min = 4;
		d10.pattern = new int[] { 1111, 1111, 6311, 1111, 1111, 6311, 6311, 6311, 6311 };
		d10.shuffle_preset = new int[] { 6311, 1111, 1111, 1111, 6311, 6311, 1111, 6311, 6311 };
		d10.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 113, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d10);

		PuzzleData d11 = new PuzzleData();
		d11.id = 11;
		d11.width = 3;
		d11.height = 3;
		d11.rotate_size = 2;
		d11.arcade_move_min = 5;
		d11.pattern = new int[] { 1111, 1111, 1111, 6411, 1111, 6411, 6411, 6411, 6411 };
		d11.shuffle_preset = new int[] { 6411, 1111, 6411, 6411, 1111, 6411, 1111, 6411, 1111 };
		d11.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d11);

		PuzzleData d12 = new PuzzleData();
		d12.id = 12;
		d12.width = 3;
		d12.height = 3;
		d12.rotate_size = 2;
		d12.arcade_move_min = 4;
		d12.pattern = new int[] { 6111, 1111, 6111, 1111, 6111, 1111, 6111, 1111, 6111 };
		d12.shuffle_preset = new int[] { 1111, 6111, 1111, 6111, 6111, 6111, 1111, 6111, 1111 };
		d12.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 112, 111, 111, 113, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d12);

		PuzzleData d13 = new PuzzleData();
		d13.id = 13;
		d13.width = 3;
		d13.height = 3;
		d13.rotate_size = 2;
		d13.arcade_move_min = 4;
		d13.pattern = new int[] { 1111, 1111, 1111, 1111, 6411, 6311, 1111, 6311, 6311 };
		d13.shuffle_preset = new int[] { 6411, 6311, 1111, 6311, 6311, 1111, 1111, 1111, 1111 };
		d13.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 113, 111, 111, 113, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d13);

		PuzzleData d14 = new PuzzleData();
		d14.id = 14;
		d14.width = 3;
		d14.height = 3;
		d14.rotate_size = 2;
		d14.arcade_move_min = 4;
		d14.pattern = new int[] { 1111, 6411, 6411, 1111, 6111, 6411, 1111, 1111, 1111 };
		d14.shuffle_preset = new int[] { 6411, 1111, 6111, 1111, 6411, 1111, 1111, 1111, 6411 };
		d14.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 113, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d14);

		PuzzleData d15 = new PuzzleData();
		d15.id = 15;
		d15.width = 3;
		d15.height = 3;
		d15.rotate_size = 2;
		d15.arcade_move_min = 6;
		d15.pattern = new int[] { 6111, 6111, 6111, 1111, 6311, 1111, 1111, 1111, 1111 };
		d15.shuffle_preset = new int[] { 1111, 1111, 1111, 6111, 6311, 6111, 1111, 6111, 1111 };
		d15.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 112, 111, 111, 111, 112, 111, 111, 111, 111, 111 };
		addPuzzle(d15);

		PuzzleData d16 = new PuzzleData();
		d16.id = 16;
		d16.width = 3;
		d16.height = 3;
		d16.rotate_size = 2;
		d16.arcade_move_min = 6;
		d16.pattern = new int[] { 6311, 1111, 1111, 6311, 6411, 1111, 6311, 6411, 1111 };
		d16.shuffle_preset = new int[] { 6311, 6411, 6311, 6411, 1111, 6311, 1111, 1111, 1111 };
		d16.grid_objects = new int[] { 111, 111, 111, 111, 111, 114, 113, 111, 111, 113, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d16);

		PuzzleData d17 = new PuzzleData();
		d17.id = 17;
		d17.width = 3;
		d17.height = 3;
		d17.rotate_size = 2;
		d17.arcade_move_min = 6;
		d17.pattern = new int[] { 1111, 1111, 1111, 6111, 6411, 1111, 6111, 6411, 1111 };
		d17.shuffle_preset = new int[] { 6411, 1111, 6111, 1111, 6111, 1111, 1111, 1111, 6411 };
		d17.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 111, 111, 111, 114, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d17);

		PuzzleData d18 = new PuzzleData();
		d18.id = 18;
		d18.width = 3;
		d18.height = 3;
		d18.rotate_size = 2;
		d18.arcade_move_min = 5;
		d18.pattern = new int[] { 1111, 1111, 1111, 6211, 6311, 6211, 6211, 6311, 6211 };
		d18.shuffle_preset = new int[] { 1111, 6211, 6311, 6211, 1111, 6211, 6311, 6211, 1111 };
		d18.grid_objects = new int[] { 111, 111, 111, 111, 111, 114, 113, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d18);

		PuzzleData d19 = new PuzzleData();
		d19.id = 19;
		d19.width = 4;
		d19.height = 3;
		d19.rotate_size = 3;
		d19.arcade_move_min = 2;
		d19.pattern = new int[] { 6211, 1111, 1111, 6311, 6211, 1111, 1111, 6311, 6211, 1111, 1111, 6311 };
		d19.shuffle_preset = new int[] { 1111, 1111, 6211, 6311, 1111, 1111, 6211, 6311, 1111, 1111, 6211, 6311 };
		d19.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d19);

		PuzzleData d20 = new PuzzleData();
		d20.id = 20;
		d20.width = 4;
		d20.height = 3;
		d20.rotate_size = 3;
		d20.arcade_move_min = 2;
		d20.pattern = new int[] { 6411, 6211, 6211, 1111, 6411, 6211, 6211, 1111, 1111, 1111, 1111, 1111 };
		d20.shuffle_preset = new int[] { 1111, 6211, 6211, 6411, 1111, 6211, 6211, 6411, 1111, 1111, 1111, 1111 };
		d20.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d20);

		PuzzleData d21 = new PuzzleData();
		d21.id = 21;
		d21.width = 3;
		d21.height = 4;
		d21.rotate_size = 3;
		d21.arcade_move_min = 2;
		d21.pattern = new int[] { 6211, 6111, 6111, 6211, 6111, 6111, 1111, 1111, 1111, 1111, 1111, 1111 };
		d21.shuffle_preset = new int[] { 6111, 6111, 1111, 1111, 1111, 1111, 6111, 6211, 1111, 6111, 6211, 1111 };
		d21.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d21);

		PuzzleData d22 = new PuzzleData();
		d22.id = 22;
		d22.width = 3;
		d22.height = 4;
		d22.rotate_size = 3;
		d22.arcade_move_min = 3;
		d22.pattern = new int[] { 1111, 1111, 1111, 6311, 6311, 6311, 6311, 6311, 6311, 1111, 1111, 1111 };
		d22.shuffle_preset = new int[] { 6311, 6311, 1111, 1111, 1111, 1111, 6311, 1111, 6311, 6311, 1111, 6311 };
		d22.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d22);

		PuzzleData d23 = new PuzzleData();
		d23.id = 23;
		d23.width = 4;
		d23.height = 3;
		d23.rotate_size = 3;
		d23.arcade_move_min = 3;
		d23.pattern = new int[] { 6411, 1111, 6411, 1111, 6411, 1111, 6411, 1111, 6411, 1111, 6411, 1111 };
		d23.shuffle_preset = new int[] { 1111, 1111, 6411, 1111, 6411, 6411, 6411, 6411, 1111, 1111, 6411, 1111 };
		d23.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d23);

		PuzzleData d24 = new PuzzleData();
		d24.id = 24;
		d24.width = 3;
		d24.height = 4;
		d24.rotate_size = 3;
		d24.arcade_move_min = 3;
		d24.pattern = new int[] { 6111, 1111, 1111, 6211, 6111, 1111, 1111, 6211, 6111, 1111, 1111, 6211 };
		d24.shuffle_preset = new int[] { 6111, 6211, 1111, 6211, 6111, 6211, 6111, 1111, 1111, 1111, 1111, 1111 };
		d24.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d24);

		PuzzleData d25 = new PuzzleData();
		d25.id = 25;
		d25.width = 3;
		d25.height = 2;
		d25.rotate_size = 2;
		d25.arcade_move_min = 2;
		d25.pattern = new int[] { 1111, 5312, 1111, 6411, 5314, 6411 };
		d25.shuffle_preset = new int[] { 5314, 6411, 5311, 1111, 6411, 1111 };
		d25.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d25);

		PuzzleData d26 = new PuzzleData();
		d26.id = 26;
		d26.width = 2;
		d26.height = 3;
		d26.rotate_size = 2;
		d26.arcade_move_min = 2;
		d26.pattern = new int[] { 1111, 1111, 6111, 5412, 6111, 5414 };
		d26.shuffle_preset = new int[] { 1111, 5413, 6111, 1111, 5413, 6111 };
		d26.grid_objects = new int[] { 111, 111, 111, 111, 112, 111, 111, 114, 111, 111, 111, 111 };
		addPuzzle(d26);

		PuzzleData d27 = new PuzzleData();
		d27.id = 27;
		d27.width = 3;
		d27.height = 3;
		d27.rotate_size = 2;
		d27.arcade_move_min = 3;
		d27.pattern = new int[] { 1111, 5213, 5211, 1111, 1111, 1111, 1111, 5113, 5111 };
		d27.shuffle_preset = new int[] { 1111, 5212, 1111, 5211, 1111, 5113, 1111, 5114, 1111 };
		d27.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 112, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d27);

		PuzzleData d28 = new PuzzleData();
		d28.id = 28;
		d28.width = 3;
		d28.height = 3;
		d28.rotate_size = 2;
		d28.arcade_move_min = 3;
		d28.pattern = new int[] { 1111, 5312, 1111, 6211, 5314, 6211, 1111, 1111, 1111 };
		d28.shuffle_preset = new int[] { 6211, 1111, 1111, 1111, 5311, 1111, 1111, 6211, 5312 };
		d28.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 114, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d28);

		PuzzleData d29 = new PuzzleData();
		d29.id = 29;
		d29.width = 3;
		d29.height = 3;
		d29.rotate_size = 2;
		d29.arcade_move_min = 3;
		d29.pattern = new int[] { 1111, 6311, 1111, 1111, 5412, 1111, 1111, 5414, 1111 };
		d29.shuffle_preset = new int[] { 6311, 1111, 1111, 1111, 5413, 5413, 1111, 1111, 1111 };
		d29.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 112, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d29);

		PuzzleData d30 = new PuzzleData();
		d30.id = 30;
		d30.width = 3;
		d30.height = 2;
		d30.rotate_size = 2;
		d30.arcade_move_min = 4;
		d30.pattern = new int[] { 5112, 1111, 6211, 5114, 1111, 6211 };
		d30.shuffle_preset = new int[] { 1111, 5111, 6211, 5114, 6211, 1111 };
		d30.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d30);

		PuzzleData d31 = new PuzzleData();
		d31.id = 31;
		d31.width = 3;
		d31.height = 3;
		d31.rotate_size = 2;
		d31.arcade_move_min = 3;
		d31.pattern = new int[] { 1111, 6311, 1111, 6311, 5213, 5211, 1111, 6311, 1111 };
		d31.shuffle_preset = new int[] { 6311, 1111, 5212, 5212, 6311, 1111, 1111, 1111, 6311 };
		d31.grid_objects = new int[] { 111, 111, 111, 111, 111, 114, 112, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d31);

		PuzzleData d32 = new PuzzleData();
		d32.id = 32;
		d32.width = 3;
		d32.height = 3;
		d32.rotate_size = 2;
		d32.arcade_move_min = 3;
		d32.pattern = new int[] { 1111, 5312, 1111, 1111, 5314, 1111, 6111, 6111, 6111 };
		d32.shuffle_preset = new int[] { 1111, 1111, 1111, 6111, 6111, 5314, 6111, 1111, 5312 };
		d32.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 112, 111, 111, 112, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d32);

		PuzzleData d33 = new PuzzleData();
		d33.id = 33;
		d33.width = 3;
		d33.height = 3;
		d33.rotate_size = 2;
		d33.arcade_move_min = 4;
		d33.pattern = new int[] { 1111, 1111, 1111, 1111, 5213, 5211, 1111, 5413, 5411 };
		d33.shuffle_preset = new int[] { 5213, 1111, 5212, 1111, 1111, 1111, 5414, 1111, 5411 };
		d33.grid_objects = new int[] { 111, 111, 111, 111, 111, 114, 112, 111, 111, 113, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d33);

		PuzzleData d34 = new PuzzleData();
		d34.id = 34;
		d34.width = 3;
		d34.height = 3;
		d34.rotate_size = 2;
		d34.arcade_move_min = 2;
		d34.pattern = new int[] { 1111, 6311, 1111, 6311, 4113, 5111, 1111, 5114, 1111 };
		d34.shuffle_preset = new int[] { 4111, 6311, 1111, 6311, 1111, 5111, 1111, 5114, 1111 };
		d34.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d34);

		PuzzleData d35 = new PuzzleData();
		d35.id = 35;
		d35.width = 3;
		d35.height = 3;
		d35.rotate_size = 2;
		d35.arcade_move_min = 4;
		d35.pattern = new int[] { 6111, 1111, 1111, 5212, 6111, 1111, 4214, 5211, 6111 };
		d35.shuffle_preset = new int[] { 6111, 1111, 4214, 5213, 1111, 1111, 6111, 5214, 6111 };
		d35.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 111, 111, 113, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d35);

		PuzzleData d36 = new PuzzleData();
		d36.id = 36;
		d36.width = 3;
		d36.height = 2;
		d36.rotate_size = 2;
		d36.arcade_move_min = 4;
		d36.pattern = new int[] { 5313, 4312, 6211, 6211, 4314, 5311 };
		d36.shuffle_preset = new int[] { 6211, 4311, 4313, 6211, 5311, 5311 };
		d36.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d36);

		PuzzleData d37 = new PuzzleData();
		d37.id = 37;
		d37.width = 3;
		d37.height = 3;
		d37.rotate_size = 2;
		d37.arcade_move_min = 3;
		d37.pattern = new int[] { 1111, 5413, 5421, 1111, 6311, 6311, 1111, 6311, 6311 };
		d37.shuffle_preset = new int[] { 1111, 1111, 5421, 6311, 5414, 6311, 6311, 1111, 6311 };
		d37.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 111, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d37);

		PuzzleData d38 = new PuzzleData();
		d38.id = 38;
		d38.width = 3;
		d38.height = 3;
		d38.rotate_size = 2;
		d38.arcade_move_min = 4;
		d38.pattern = new int[] { 6411, 1111, 6411, 1111, 5112, 1111, 1111, 5124, 1111 };
		d38.shuffle_preset = new int[] { 1111, 1111, 6411, 6411, 1111, 5113, 1111, 5124, 1111 };
		d38.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 112, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d38);

		PuzzleData d39 = new PuzzleData();
		d39.id = 39;
		d39.width = 3;
		d39.height = 3;
		d39.rotate_size = 2;
		d39.arcade_move_min = 4;
		d39.pattern = new int[] { 1111, 1111, 1111, 5212, 6111, 1111, 4224, 5211, 1111 };
		d39.shuffle_preset = new int[] { 1111, 5212, 5214, 1111, 1111, 6111, 4224, 1111, 1111 };
		d39.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 114, 111, 111, 111, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d39);

		PuzzleData d40 = new PuzzleData();
		d40.id = 40;
		d40.width = 3;
		d40.height = 3;
		d40.rotate_size = 2;
		d40.arcade_move_min = 7;
		d40.pattern = new int[] { 1111, 1111, 5122, 1111, 1111, 5114, 5323, 5311, 6211 };
		d40.shuffle_preset = new int[] { 1111, 5311, 5122, 5114, 6211, 1111, 5323, 1111, 1111 };
		d40.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 111, 111, 111, 111, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d40);

		PuzzleData d41 = new PuzzleData();
		d41.id = 41;
		d41.width = 4;
		d41.height = 2;
		d41.rotate_size = 2;
		d41.arcade_move_min = 5;
		d41.pattern = new int[] { 1111, 1111, 5313, 5321, 1111, 1111, 5413, 5421 };
		d41.shuffle_preset = new int[] { 1111, 5314, 1111, 5321, 1111, 5412, 1111, 5421 };
		d41.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 112, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d41);

		PuzzleData d42 = new PuzzleData();
		d42.id = 42;
		d42.width = 4;
		d42.height = 3;
		d42.rotate_size = 2;
		d42.arcade_move_min = 7;
		d42.pattern = new int[] { 5422, 1111, 1111, 5222, 4414, 5411, 5213, 4211, 1111, 1111, 1111, 1111 };
		d42.shuffle_preset = new int[] { 5422, 1111, 5411, 5222, 1111, 1111, 1111, 1111, 4411, 5211, 1111, 4214 };
		d42.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 112, 111, 111, 111, 112, 113, 114, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d42);

		PuzzleData d43 = new PuzzleData();
		d43.id = 43;
		d43.width = 4;
		d43.height = 3;
		d43.rotate_size = 2;
		d43.arcade_move_min = 6;
		d43.pattern = new int[] { 1111, 6411, 6411, 1111, 1111, 5112, 5212, 1111, 1111, 5124, 5224, 1111 };
		d43.shuffle_preset = new int[] { 6411, 1111, 1111, 6411, 1111, 5112, 5212, 1111, 1111, 5124, 5224, 1111 };
		d43.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 112, 113, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d43);

		PuzzleData d44 = new PuzzleData();
		d44.id = 44;
		d44.width = 3;
		d44.height = 4;
		d44.rotate_size = 2;
		d44.arcade_move_min = 4;
		d44.pattern = new int[] { 1111, 5422, 1111, 6111, 5414, 6111, 6111, 5312, 6111, 1111, 5324, 1111 };
		d44.shuffle_preset = new int[] { 1111, 5422, 1111, 5314, 6111, 6111, 6111, 6111, 5412, 1111, 5324, 1111 };
		d44.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 113, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d44);

		PuzzleData d45 = new PuzzleData();
		d45.id = 45;
		d45.width = 4;
		d45.height = 4;
		d45.rotate_size = 3;
		d45.arcade_move_min = 6;
		d45.pattern = new int[] { 6121, 6111, 1111, 1111, 6111, 6111, 1111, 1111, 1111, 1111, 6211, 6211, 1111, 1111, 6211, 6221 };
		d45.shuffle_preset = new int[] { 6121, 1111, 1111, 6111, 1111, 1111, 6111, 6111, 6211, 6211, 1111, 1111, 6211, 1111, 1111, 6221 };
		d45.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d45);

		PuzzleData d46 = new PuzzleData();
		d46.id = 46;
		d46.width = 4;
		d46.height = 4;
		d46.rotate_size = 3;
		d46.arcade_move_min = 4;
		d46.pattern = new int[] { 5212, 5222, 5322, 5312, 5214, 5214, 5314, 5314, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111 };
		d46.shuffle_preset = new int[] { 5212, 5222, 5322, 5312, 5314, 5314, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 5212, 5212 };
		d46.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d46);

		PuzzleData d47 = new PuzzleData();
		d47.id = 47;
		d47.width = 4;
		d47.height = 4;
		d47.rotate_size = 3;
		d47.arcade_move_min = 8;
		d47.pattern = new int[] { 1111, 1111, 1111, 1111, 1111, 6411, 6411, 1111, 1111, 6411, 6411, 1111, 1111, 1111, 1111, 1111 };
		d47.shuffle_preset = new int[] { 6411, 1111, 1111, 6411, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 6411, 1111, 1111, 6411 };
		d47.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d47);

		PuzzleData d48 = new PuzzleData();
		d48.id = 48;
		d48.width = 4;
		d48.height = 4;
		d48.rotate_size = 3;
		d48.arcade_move_min = 4;
		d48.pattern = new int[] { 1111, 1111, 5112, 5122, 1111, 1111, 5114, 5114, 5213, 5211, 1111, 1111, 5223, 5211, 1111, 1111 };
		d48.shuffle_preset = new int[] { 1111, 1111, 5211, 5122, 1111, 1111, 1111, 5213, 5114, 1111, 1111, 5213, 5223, 5112, 5112, 1111 };
		d48.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d48);

		PuzzleData d49 = new PuzzleData();
		d49.id = 49;
		d49.width = 4;
		d49.height = 2;
		d49.rotate_size = 2;
		d49.arcade_move_min = 3;
		d49.pattern = new int[] { 6411, 6411, 6411, 6411, 6311, 6311, 6311, 6311 };
		d49.shuffle_preset = new int[] { 6311, 6411, 6311, 6311, 6411, 6411, 6411, 6311 };
		d49.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 232, 112, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d49);

		PuzzleData d50 = new PuzzleData();
		d50.id = 50;
		d50.width = 3;
		d50.height = 2;
		d50.rotate_size = 2;
		d50.arcade_move_min = 3;
		d50.pattern = new int[] { 6411, 6111, 6411, 6411, 6111, 6411 };
		d50.shuffle_preset = new int[] { 6411, 6411, 6111, 6111, 6411, 6411 };
		d50.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 231, 111, 111, 111, 111, 111 };
		addPuzzle(d50);

		PuzzleData d51 = new PuzzleData();
		d51.id = 51;
		d51.width = 3;
		d51.height = 3;
		d51.rotate_size = 2;
		d51.arcade_move_min = 8;
		d51.pattern = new int[] { 1111, 6311, 6111, 1111, 6311, 6111, 1121, 1111, 1111 };
		d51.shuffle_preset = new int[] { 6111, 1111, 1111, 6311, 1111, 1111, 1121, 6311, 6111 };
		d51.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 231, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d51);

		PuzzleData d52 = new PuzzleData();
		d52.id = 52;
		d52.width = 3;
		d52.height = 3;
		d52.rotate_size = 2;
		d52.arcade_move_min = 8;
		d52.pattern = new int[] { 6211, 1111, 6111, 1111, 1111, 1111, 6211, 1111, 6111 };
		d52.shuffle_preset = new int[] { 1111, 1111, 1111, 6211, 6211, 1111, 6111, 6111, 1111 };
		d52.grid_objects = new int[] { 111, 111, 111, 111, 111, 232, 111, 111, 111, 114, 231, 111, 111, 111, 111, 111 };
		addPuzzle(d52);

		PuzzleData d53 = new PuzzleData();
		d53.id = 53;
		d53.width = 3;
		d53.height = 3;
		d53.rotate_size = 2;
		d53.arcade_move_min = 8;
		d53.pattern = new int[] { 1111, 6211, 1111, 1111, 6211, 1111, 6311, 6311, 6311 };
		d53.shuffle_preset = new int[] { 6311, 6311, 6211, 1111, 6311, 6211, 1111, 1111, 1111 };
		d53.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 112, 111, 111, 224, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d53);

		PuzzleData d54 = new PuzzleData();
		d54.id = 54;
		d54.width = 3;
		d54.height = 3;
		d54.rotate_size = 2;
		d54.arcade_move_min = 8;
		d54.pattern = new int[] { 1111, 1111, 1111, 6111, 1111, 6411, 6111, 1111, 6411 };
		d54.shuffle_preset = new int[] { 6111, 1111, 6411, 1111, 1111, 1111, 6411, 1111, 6111 };
		d54.grid_objects = new int[] { 111, 111, 111, 111, 111, 113, 112, 111, 111, 232, 232, 111, 111, 111, 111, 111 };
		addPuzzle(d54);

		PuzzleData d55 = new PuzzleData();
		d55.id = 55;
		d55.width = 4;
		d55.height = 3;
		d55.rotate_size = 2;
		d55.arcade_move_min = 9;
		d55.pattern = new int[] { 1111, 6411, 6211, 1111, 1111, 6411, 6211, 1111, 1111, 6411, 6211, 1111 };
		d55.shuffle_preset = new int[] { 6211, 1111, 1111, 6411, 6211, 1111, 1111, 6411, 6211, 1111, 1111, 6411 };
		d55.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 113, 223, 113, 111, 111, 113, 221, 112, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d55);

		PuzzleData d56 = new PuzzleData();
		d56.id = 56;
		d56.width = 4;
		d56.height = 3;
		d56.rotate_size = 2;
		d56.arcade_move_min = 8;
		d56.pattern = new int[] { 1111, 1111, 1111, 6111, 1111, 1111, 6111, 6311, 1111, 6111, 6311, 6311 };
		d56.shuffle_preset = new int[] { 1111, 6111, 6111, 6111, 1111, 1111, 1111, 1111, 1111, 6311, 6311, 6311 };
		d56.grid_objects = new int[] { 111, 111, 312, 111, 111, 111, 112, 111, 111, 111, 313, 111, 114, 111, 311, 111, 111, 111, 111, 111 };
		addPuzzle(d56);

		PuzzleData d57 = new PuzzleData();
		d57.id = 57;
		d57.width = 3;
		d57.height = 4;
		d57.rotate_size = 2;
		d57.arcade_move_min = 13;
		d57.pattern = new int[] { 1111, 1111, 1111, 6311, 6311, 6311, 6211, 6211, 6211, 1111, 1111, 1111 };
		d57.shuffle_preset = new int[] { 6211, 1111, 1111, 6211, 6211, 1111, 6311, 6311, 1111, 6311, 1111, 1111 };
		d57.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 231, 111, 111, 232, 111, 111, 111, 111, 231, 111, 111, 111, 111, 111 };
		addPuzzle(d57);

		PuzzleData d58 = new PuzzleData();
		d58.id = 58;
		d58.width = 3;
		d58.height = 4;
		d58.rotate_size = 2;
		d58.arcade_move_min = 12;
		d58.pattern = new int[] { 1111, 1111, 1111, 1111, 5412, 1111, 1111, 5414, 1111, 1111, 1111, 1111 };
		d58.shuffle_preset = new int[] { 1111, 1111, 1111, 1111, 5414, 1111, 1111, 5412, 1111, 1111, 1111, 1111 };
		d58.grid_objects = new int[] { 111, 111, 111, 111, 111, 232, 232, 111, 111, 112, 112, 111, 111, 232, 232, 111, 111, 111, 111, 111 };
		addPuzzle(d58);

		PuzzleData d59 = new PuzzleData();
		d59.id = 59;
		d59.width = 5;
		d59.height = 2;
		d59.rotate_size = 2;
		d59.arcade_move_min = 7;
		d59.pattern = new int[] { 1111, 6411, 6111, 6411, 1111, 1111, 6411, 6111, 6411, 1111 };
		d59.shuffle_preset = new int[] { 6411, 1111, 6411, 1111, 6411, 6111, 1111, 6411, 1111, 6111 };
		d59.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 113, 224, 223, 112, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d59);

		PuzzleData d60 = new PuzzleData();
		d60.id = 60;
		d60.width = 4;
		d60.height = 4;
		d60.rotate_size = 2;
		d60.arcade_move_min = 9;
		d60.pattern = new int[] { 1111, 1111, 1111, 1111, 1111, 5112, 5212, 1111, 1121, 5114, 5214, 1121, 1121, 1111, 1111, 1121 };
		d60.shuffle_preset = new int[] { 1111, 1111, 1111, 1111, 5212, 1111, 1111, 5112, 1121, 1111, 1111, 1121, 1121, 5214, 5114, 1121 };
		d60.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				113,
				224,
				114,
				111,
				111,
				111,
				224,
				111,
				111,
				111,
				112,
				222,
				112,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d60);

		PuzzleData d61 = new PuzzleData();
		d61.id = 61;
		d61.width = 4;
		d61.height = 3;
		d61.rotate_size = 2;
		d61.arcade_move_min = 9;
		d61.pattern = new int[] { 1111, 6211, 6211, 1111, 5313, 2311, 2311, 5311, 1111, 6211, 6211, 1111 };
		d61.shuffle_preset = new int[] { 1111, 2311, 2311, 1111, 5311, 6211, 6211, 5313, 6211, 1111, 1111, 6211 };
		d61.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 114, 113, 112, 111, 111, 114, 114, 112, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d61);

		PuzzleData d62 = new PuzzleData();
		d62.id = 62;
		d62.width = 4;
		d62.height = 3;
		d62.rotate_size = 2;
		d62.arcade_move_min = 10;
		d62.pattern = new int[] { 1111, 6311, 6311, 1111, 1111, 5412, 5212, 1111, 5413, 4411, 4214, 5211 };
		d62.shuffle_preset = new int[] { 6311, 5414, 5214, 6311, 5214, 1111, 1111, 5414, 1111, 4211, 4414, 1111 };
		d62.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 112, 111, 114, 111, 111, 112, 113, 114, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d62);

		PuzzleData d63 = new PuzzleData();
		d63.id = 63;
		d63.width = 3;
		d63.height = 4;
		d63.rotate_size = 2;
		d63.arcade_move_min = 8;
		d63.pattern = new int[] { 5413, 2411, 5411, 1111, 6211, 1111, 1111, 6211, 1111, 5113, 2111, 5111 };
		d63.shuffle_preset = new int[] { 1111, 5412, 2412, 5414, 1111, 6211, 6211, 1111, 5112, 2112, 5114, 1111 };
		d63.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 111, 111, 111, 112, 114, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d63);

		PuzzleData d64 = new PuzzleData();
		d64.id = 64;
		d64.width = 3;
		d64.height = 4;
		d64.rotate_size = 2;
		d64.arcade_move_min = 13;
		d64.pattern = new int[] { 4113, 5111, 1111, 2112, 1111, 5312, 5114, 1111, 2312, 1111, 5313, 4311 };
		d64.shuffle_preset = new int[] { 5112, 2112, 1111, 5113, 4111, 1111, 1111, 4313, 5311, 1111, 2312, 5314 };
		d64.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 114, 111, 111, 112, 112, 111, 111, 114, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d64);

		PuzzleData d65 = new PuzzleData();
		d65.id = 65;
		d65.width = 3;
		d65.height = 3;
		d65.rotate_size = 2;
		d65.arcade_move_min = 8;
		d65.pattern = new int[] { 1111, 5213, 4212, 5412, 1111, 5214, 4414, 5411, 1111 };
		d65.shuffle_preset = new int[] { 1111, 5211, 5211, 4213, 1111, 4413, 1111, 5411, 5411 };
		d65.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 114, 111, 111, 111, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d65);

		PuzzleData d66 = new PuzzleData();
		d66.id = 66;
		d66.width = 3;
		d66.height = 3;
		d66.rotate_size = 2;
		d66.arcade_move_min = 9;
		d66.pattern = new int[] { 5313, 2311, 5311, 1111, 1111, 1111, 5213, 2211, 5211 };
		d66.shuffle_preset = new int[] { 5213, 1111, 5211, 2311, 1111, 2211, 5311, 1111, 5313 };
		d66.grid_objects = new int[] { 111, 111, 111, 111, 111, 112, 111, 111, 111, 113, 113, 111, 111, 111, 111, 111 };
		addPuzzle(d66);

		PuzzleData d67 = new PuzzleData();
		d67.id = 67;
		d67.width = 4;
		d67.height = 4;
		d67.rotate_size = 3;
		d67.arcade_move_min = 7;
		d67.pattern = new int[] { 1111, 1111, 6411, 1111, 1111, 6411, 1111, 6111, 6411, 1111, 6111, 1111, 1111, 6111, 1111, 1111 };
		d67.shuffle_preset = new int[] { 6411, 1111, 6411, 1111, 1111, 6111, 1111, 1111, 1111, 1111, 6411, 1111, 1111, 6111, 1111, 6111 };
		d67.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d67);

		PuzzleData d68 = new PuzzleData();
		d68.id = 68;
		d68.width = 4;
		d68.height = 4;
		d68.rotate_size = 3;
		d68.arcade_move_min = 8;
		d68.pattern = new int[] { 2111, 2111, 2111, 2111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 2411, 2411, 2411, 2411 };
		d68.shuffle_preset = new int[] { 2412, 2412, 2412, 2412, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 2112, 2112, 2112, 2112 };
		d68.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d68);

		PuzzleData d69 = new PuzzleData();
		d69.id = 69;
		d69.width = 4;
		d69.height = 4;
		d69.rotate_size = 3;
		d69.arcade_move_min = 12;
		d69.pattern = new int[] { 1111, 1111, 1111, 1111, 1111, 4213, 4212, 1111, 1111, 4214, 4211, 1111, 1111, 1111, 1111, 1111 };
		d69.shuffle_preset = new int[] { 4211, 1111, 1111, 4211, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 4211, 1111, 1111, 4211 };
		d69.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d69);

		PuzzleData d70 = new PuzzleData();
		d70.id = 70;
		d70.width = 4;
		d70.height = 3;
		d70.rotate_size = 3;
		d70.arcade_move_min = 10;
		d70.pattern = new int[] { 1111, 5312, 5312, 1111, 1111, 7314, 7312, 1111, 1111, 5314, 5314, 1111 };
		d70.shuffle_preset = new int[] { 1111, 5314, 5314, 1111, 7311, 1111, 1111, 1111, 7313, 1111, 5314, 5314 };
		d70.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d70);

		PuzzleData d71 = new PuzzleData();
		d71.id = 71;
		d71.width = 4;
		d71.height = 3;
		d71.rotate_size = 2;
		d71.arcade_move_min = 7;
		d71.pattern = new int[] { 5413, 7413, 7413, 5411, 1111, 5414, 5414, 1111, 1111, 1111, 1111, 1111 };
		d71.shuffle_preset = new int[] { 5413, 5411, 1111, 7411, 5413, 5411, 1111, 7411, 1111, 1111, 1111, 1111 };
		d71.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 112, 113, 111, 111, 112, 112, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d71);

		PuzzleData d72 = new PuzzleData();
		d72.id = 72;
		d72.width = 3;
		d72.height = 4;
		d72.rotate_size = 2;
		d72.arcade_move_min = 9;
		d72.pattern = new int[] { 5212, 1111, 1111, 7214, 2211, 5211, 2212, 1111, 1111, 5214, 1111, 1111 };
		d72.shuffle_preset = new int[] { 1111, 1111, 1111, 1111, 1111, 1111, 2211, 7212, 5212, 2211, 5212, 5212 };
		d72.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 112, 111, 111, 114, 114, 111, 111, 112, 114, 111, 111, 111, 111, 111 };
		addPuzzle(d72);

		PuzzleData d73 = new PuzzleData();
		d73.id = 73;
		d73.width = 4;
		d73.height = 4;
		d73.rotate_size = 2;
		d73.arcade_move_min = 9;
		d73.pattern = new int[] { 1111, 1111, 1111, 1111, 5313, 2311, 5311, 6111, 6311, 5113, 2111, 5111, 1111, 1111, 1111, 1111 };
		d73.shuffle_preset = new int[] { 2311, 6311, 5312, 5313, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 1111, 5111, 5114, 6111, 2111 };
		d73.grid_objects = new int[] {
				111,
				111,
				312,
				111,
				111,
				111,
				113,
				111,
				112,
				111,
				313,
				111,
				114,
				111,
				311,
				111,
				113,
				111,
				114,
				111,
				111,
				111,
				314,
				111,
				111 };
		addPuzzle(d73);

		PuzzleData d74 = new PuzzleData();
		d74.id = 74;
		d74.width = 4;
		d74.height = 4;
		d74.rotate_size = 2;
		d74.arcade_move_min = 10;
		d74.pattern = new int[] { 1111, 4413, 2411, 5411, 4413, 8411, 5411, 1111, 2412, 5414, 6211, 1111, 5414, 1111, 1111, 1111 };
		d74.shuffle_preset = new int[] { 1111, 4413, 5412, 5411, 4413, 8411, 1111, 2412, 5413, 1111, 1111, 1111, 5414, 2411, 1111, 6211 };
		d74.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				322,
				111,
				112,
				111,
				111,
				111,
				324,
				114,
				111,
				111,
				114,
				112,
				114,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d74);

		PuzzleData d75 = new PuzzleData();
		d75.id = 75;
		d75.width = 4;
		d75.height = 4;
		d75.rotate_size = 2;
		d75.arcade_move_min = 8;
		d75.pattern = new int[] { 5312, 1111, 1111, 5212, 7314, 2311, 5311, 2212, 2312, 5213, 2211, 7212, 5314, 1111, 1111, 5214 };
		d75.shuffle_preset = new int[] { 5312, 7214, 5212, 5212, 2311, 1111, 1111, 2211, 2311, 1111, 1111, 2211, 5314, 5314, 7312, 5214 };
		d75.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				313,
				111,
				112,
				111,
				311,
				111,
				113,
				111,
				113,
				111,
				313,
				111,
				113,
				111,
				311,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d75);

		PuzzleData d76 = new PuzzleData();
		d76.id = 76;
		d76.width = 4;
		d76.height = 4;
		d76.rotate_size = 2;
		d76.arcade_move_min = 14;
		d76.pattern = new int[] { 6211, 5412, 5112, 6211, 1111, 2412, 2112, 1111, 1111, 2412, 2112, 1111, 6211, 5414, 5114, 6211 };
		d76.shuffle_preset = new int[] { 5412, 2112, 2412, 5112, 2412, 5114, 5414, 2112, 1111, 1111, 1111, 1111, 6211, 6211, 6211, 6211 };
		d76.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				232,
				113,
				232,
				111,
				111,
				113,
				111,
				113,
				111,
				111,
				232,
				111,
				232,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d76);

		PuzzleData d77 = new PuzzleData();
		d77.id = 77;
		d77.width = 4;
		d77.height = 4;
		d77.rotate_size = 2;
		d77.arcade_move_min = 13;
		d77.pattern = new int[] { 5322, 1111, 1111, 5322, 4314, 5311, 5313, 4311, 4413, 5411, 5413, 4412, 5424, 1111, 1111, 5424 };
		d77.shuffle_preset = new int[] { 5322, 5312, 5312, 5322, 1111, 4313, 4312, 1111, 1111, 4414, 4411, 1111, 5424, 5414, 5414, 5424 };
		d77.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				113,
				111,
				111,
				111,
				111,
				244,
				111,
				111,
				111,
				111,
				113,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d77);

		PuzzleData d78 = new PuzzleData();
		d78.id = 78;
		d78.width = 4;
		d78.height = 4;
		d78.rotate_size = 2;
		d78.arcade_move_min = 14;
		d78.pattern = new int[] { 5113, 7113, 7113, 5111, 1111, 5114, 5114, 1111, 1111, 6411, 6411, 1111, 1121, 1111, 1111, 1121 };
		d78.shuffle_preset = new int[] { 1111, 7114, 7112, 1111, 1111, 5113, 5111, 1111, 5112, 6411, 6411, 5112, 1121, 1111, 1111, 1121 };
		d78.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				114,
				332,
				112,
				111,
				111,
				114,
				113,
				113,
				111,
				111,
				111,
				112,
				112,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d78);

		PuzzleData d79 = new PuzzleData();
		d79.id = 79;
		d79.width = 4;
		d79.height = 4;
		d79.rotate_size = 2;
		d79.arcade_move_min = 5;
		d79.pattern = new int[] { 1111, 5313, 5311, 1111, 5412, 1111, 5212, 1111, 5414, 5213, 4211, 1111, 1111, 1111, 1111, 1111 };
		d79.shuffle_preset = new int[] { 5314, 5314, 5214, 1111, 1111, 1111, 1111, 5213, 5411, 1111, 4211, 1111, 5411, 1111, 1111, 1111 };
		d79.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				113,
				231,
				114,
				111,
				111,
				232,
				111,
				111,
				111,
				111,
				114,
				111,
				324,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d79);

		PuzzleData d80 = new PuzzleData();
		d80.id = 80;
		d80.width = 4;
		d80.height = 4;
		d80.rotate_size = 2;
		d80.arcade_move_min = 1;
		d80.pattern = new int[] { 1111, 5113, 5111, 1111, 5312, 6211, 6211, 5312, 5314, 6211, 6211, 5314, 1111, 5113, 5111, 1111 };
		d80.shuffle_preset = new int[] { 5311, 1111, 6211, 5114, 6211, 5112, 5311, 1111, 1111, 5313, 5114, 6211, 5112, 6211, 1111, 5313 };
		d80.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				222,
				111,
				111,
				111,
				111,
				111,
				221,
				111,
				111,
				111,
				224,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d80);

		PuzzleData d81 = new PuzzleData();
		d81.id = 81;
		d81.width = 4;
		d81.height = 4;
		d81.rotate_size = 2;
		d81.arcade_move_min = 9;
		d81.pattern = new int[] { 6411, 1111, 1111, 6211, 1111, 5412, 5212, 1111, 1111, 5414, 5214, 1111, 6411, 1111, 1111, 6211 };
		d81.shuffle_preset = new int[] { 1111, 1111, 1111, 1111, 5214, 6211, 6411, 5412, 5214, 6211, 6411, 5412, 1111, 1111, 1111, 1111 };
		d81.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				112,
				111,
				111,
				111,
				331,
				251,
				331,
				111,
				111,
				114,
				112,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d81);

		PuzzleData d82 = new PuzzleData();
		d82.id = 82;
		d82.width = 4;
		d82.height = 4;
		d82.rotate_size = 2;
		d82.arcade_move_min = 5;
		d82.pattern = new int[] { 1111, 1111, 1111, 1121, 1111, 5213, 4212, 1111, 5313, 4312, 5214, 1111, 1111, 5314, 1111, 1111 };
		d82.shuffle_preset = new int[] { 1111, 5213, 1111, 1121, 1111, 1111, 4314, 1111, 5312, 1111, 1111, 5214, 4212, 5311, 1111, 1111 };
		d82.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				113,
				112,
				111,
				111,
				111,
				243,
				111,
				112,
				111,
				111,
				113,
				243,
				113,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d82);

		PuzzleData d83 = new PuzzleData();
		d83.id = 83;
		d83.width = 4;
		d83.height = 4;
		d83.rotate_size = 2;
		d83.arcade_move_min = 12;
		d83.pattern = new int[] { 1111, 5412, 5122, 1111, 5412, 2412, 2112, 5122, 5424, 2412, 2112, 5114, 1111, 5424, 5114, 1111 };
		d83.shuffle_preset = new int[] { 2111, 2111, 5122, 1111, 2411, 2411, 1111, 5122, 5424, 1111, 5113, 5412, 1111, 5424, 5112, 5413 };
		d83.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				112,
				111,
				111,
				111,
				112,
				112,
				113,
				111,
				111,
				111,
				114,
				113,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d83);

		PuzzleData d84 = new PuzzleData();
		d84.id = 84;
		d84.width = 4;
		d84.height = 4;
		d84.rotate_size = 2;
		d84.arcade_move_min = 10;
		d84.pattern = new int[] { 1111, 1111, 1111, 1111, 6311, 5212, 4313, 5311, 5213, 4211, 5314, 6211, 1111, 1111, 1111, 1111 };
		d84.shuffle_preset = new int[] { 1111, 1111, 1111, 1111, 4311, 6311, 5314, 5214, 4212, 6211, 5211, 5313, 1111, 1111, 1111, 1111 };
		d84.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				114,
				251,
				114,
				111,
				111,
				111,
				331,
				111,
				111,
				111,
				114,
				251,
				114,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d84);

	}

}