package com.gamexplode.puzzotate.data;

import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;

public class ExtremePuzzles extends Puzzles {

	public ExtremePuzzles() {
		super(PuzzleManager.EXTREME);
	}

	public void removeScores() {
		Settings settings = Resource.getSettings(Config.SETTINGS_EXTREME);
		for (Entry<PuzzleData> entry : getPuzzles()) {
			settings.remove("b" + entry.value.id);
			settings.remove("s" + entry.value.id);
			settings.remove("p" + entry.value.id);
			settings.remove("a" + entry.value.id);
		}
	}

	public void replaceScores(JsonValue arrayScores) {
		JsonValue objScore;
		Settings settings = Resource.getSettings(Config.SETTINGS_EXTREME);
		IntMap<Integer> mapScore = new IntMap<Integer>();
		IntMap<Integer> mapStars = new IntMap<Integer>();
		for (int i = arrayScores.size - 1; i >= 0; i--) {
			objScore = arrayScores.get(i);
			mapScore.put(objScore.getInt("type_id"), objScore.getInt("score"));
			mapStars.put(objScore.getInt("type_id"), objScore.getInt("stars"));
		}
		int id, star, best;
		for (Entry<PuzzleData> entry : getPuzzles()) {
			id = entry.value.id;
			if (!mapScore.containsKey(id)) {
				settings.remove("b" + id);
				settings.remove("s" + id);
				settings.remove("p" + id);
				settings.remove("a" + id);
			} else {
				star = mapStars.get(id);
				best = mapScore.get(id);
				settings.setInteger("b" + id, best);
				settings.setInteger("s" + id, star);
				if (star >= 3) {
					PuzzleState ps = new PuzzleState();
					ps.id = id;
					ps.seconds = best;
					ps.puzzle = entry.value.pattern;
					ps.turnstile = entry.value.grid_objects;
					Json json = new Json();
					settings.setString("a" + ps.id, json.toJson(ps));
				} else {
					settings.remove("p" + id);
					settings.remove("a" + id);
				}
			}

		}
	}

	@Override
	protected void create() {
		super.create();

		PuzzleData d1 = new PuzzleData();
		d1.id = 1;
		d1.width = 3;
		d1.height = 3;
		d1.rotate_size = 2;
		d1.extreme_star_scope = new int[] { 55, 75, 100 };
		d1.pattern = new int[] { 6111, 6111, 6111, 6211, 6211, 6211, 6311, 6311, 6311 };
		addPuzzle(d1);

		PuzzleData dTest = new PuzzleData();
		dTest.id = 1;
		dTest.width = 2;
		dTest.height = 2;
		dTest.rotate_size = 2;
		dTest.pattern = new int[] { 6111, 6111, 6211, 6211 };
		// addPuzzle(dTest);

		PuzzleData d2 = new PuzzleData();
		d2.id = 2;
		d2.width = 3;
		d2.height = 3;
		d2.rotate_size = 2;
		d2.extreme_star_scope = new int[] { 55, 75, 100 };
		d2.pattern = new int[] { 6111, 6111, 6111, 6211, 6311, 6211, 6411, 6411, 6411 };
		addPuzzle(d2);

		PuzzleData d3 = new PuzzleData();
		d3.id = 3;
		d3.width = 4;
		d3.height = 2;
		d3.rotate_size = 2;
		d3.extreme_star_scope = new int[] { 55, 75, 100 };
		d3.pattern = new int[] { 5112, 6211, 6211, 5112, 5114, 6311, 6311, 5114 };
		addPuzzle(d3);

		PuzzleData d4 = new PuzzleData();
		d4.id = 4;
		d4.width = 5;
		d4.height = 2;
		d4.rotate_size = 2;
		d4.extreme_star_scope = new int[] { 60, 80, 100 };
		d4.pattern = new int[] { 6211, 5113, 4112, 6311, 6311, 6211, 6211, 4114, 5111, 6311 };
		addPuzzle(d4);

		PuzzleData d5 = new PuzzleData();
		d5.id = 5;
		d5.width = 5;
		d5.height = 2;
		d5.rotate_size = 2;
		d5.extreme_star_scope = new int[] { 60, 80, 100 };
		d5.pattern = new int[] { 5313, 4312, 6211, 4113, 5111, 5313, 4311, 6211, 4114, 5111 };
		addPuzzle(d5);

		PuzzleData d6 = new PuzzleData();
		d6.id = 6;
		d6.width = 3;
		d6.height = 3;
		d6.rotate_size = 2;
		d6.nmb = 2;
		d6.extreme_star_scope = new int[] { 55, 75, 100 };
		d6.pattern = new int[] { 6111, 5213, 4222, 5212, 6311, 5214, 4224, 5211, 6411 };
		addPuzzle(d6);

		PuzzleData d7 = new PuzzleData();
		d7.id = 7;
		d7.width = 3;
		d7.height = 3;
		d7.rotate_size = 2;
		d7.extreme_star_scope = new int[] { 55, 75, 100 };
		d7.pattern = new int[] { 5213, 2211, 5211, 6311, 6111, 6311, 5413, 2411, 5411 };
		addPuzzle(d7);

		PuzzleData d8 = new PuzzleData();
		d8.id = 8;
		d8.width = 3;
		d8.height = 3;
		d8.rotate_size = 2;
		d8.nmb = 2;
		d8.extreme_star_scope = new int[] { 55, 75, 100 };
		d8.pattern = new int[] { 6311, 5212, 6311, 5213, 3212, 5211, 6111, 5214, 6111 };
		d8.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 251, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d8);

		PuzzleData d9 = new PuzzleData();
		d9.id = 9;
		d9.width = 3;
		d9.height = 3;
		d9.rotate_size = 2;
		d9.extreme_star_scope = new int[] { 55, 75, 100 };
		d9.pattern = new int[] { 6311, 4213, 5211, 4213, 3212, 5211, 5214, 5214, 6311 };
		addPuzzle(d9);

		PuzzleData d10 = new PuzzleData();
		d10.id = 10;
		d10.width = 4;
		d10.height = 3;
		d10.rotate_size = 2;
		d10.extreme_star_scope = new int[] { 60, 80, 100 };
		d10.pattern = new int[] { 6311, 5212, 5412, 6311, 4213, 4211, 4414, 4412, 5214, 6311, 6311, 5414 };
		d10.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 251, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d10);

		PuzzleData d11 = new PuzzleData();
		d11.id = 11;
		d11.width = 4;
		d11.height = 3;
		d11.rotate_size = 3;
		d11.shuffle_type = 3;
		d11.shuffle_percent = 50; // 30
		d11.extreme_star_scope = new int[] { 60, 80, 100 };
		d11.pattern = new int[] { 6311, 6311, 6211, 6211, 6311, 6311, 6211, 6211, 6311, 6311, 6211, 6211 };
		addPuzzle(d11);

		PuzzleData d12 = new PuzzleData();
		d12.id = 12;
		d12.width = 4;
		d12.height = 3;
		d12.rotate_size = 2;
		d12.nmb = 2;
		d12.extreme_star_scope = new int[] { 60, 80, 100 };
		d12.pattern = new int[] { 4213, 5211, 5313, 4312, 2212, 6211, 6311, 2312, 4224, 5211, 5313, 4321 };
		d12.grid_objects = new int[] { 111, 111, 111, 111, 111, 111, 111, 221, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111, 111 };
		addPuzzle(d12);

		PuzzleData d13 = new PuzzleData();
		d13.id = 13;
		d13.width = 5;
		d13.height = 3;
		d13.rotate_size = 2;
		d13.extreme_star_scope = new int[] { 65, 85, 100 };
		d13.pattern = new int[] { 6211, 5312, 6211, 5312, 6211, 5313, 3312, 2311, 3311, 5311, 6211, 5314, 6211, 5314, 6211 };
		addPuzzle(d13);

		PuzzleData d14 = new PuzzleData();
		d14.id = 14;
		d14.width = 5;
		d14.height = 3;
		d14.rotate_size = 2;
		d14.extreme_star_scope = new int[] { 65, 85, 100 };
		d14.pattern = new int[] { 5313, 2311, 2311, 2311, 4312, 4213, 5211, 6111, 5313, 4311, 4214, 2211, 2211, 2211, 5211 };
		addPuzzle(d14);

		PuzzleData d15 = new PuzzleData();
		d15.id = 15;
		d15.width = 5;
		d15.height = 3;
		d15.rotate_size = 2;
		d15.extreme_star_scope = new int[] { 65, 85, 100 };
		d15.pattern = new int[] { 6211, 5212, 5112, 6311, 6311, 6211, 5214, 2112, 5312, 6311, 6211, 6211, 5114, 5314, 6311 };
		d15.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d15);

		PuzzleData d16 = new PuzzleData();
		d16.id = 16;
		d16.width = 4;
		d16.height = 4;
		d16.rotate_size = 3;
		d16.extreme_star_scope = new int[] { 65, 85, 100 };
		d16.pattern = new int[] { 6111, 6111, 6211, 6211, 6111, 6111, 6211, 6211, 6311, 6311, 6411, 6411, 6311, 6311, 6411, 6411 };
		addPuzzle(d16);

		PuzzleData d17 = new PuzzleData();
		d17.id = 17;
		d17.width = 4;
		d17.height = 4;
		d17.rotate_size = 2;
		d17.extreme_star_scope = new int[] { 65, 85, 100 };
		d17.pattern = new int[] { 5423, 2411, 2411, 5421, 5413, 2411, 2411, 5411, 5113, 2111, 2111, 5111, 5123, 2111, 2111, 5121 };
		d17.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d17);

		PuzzleData d18 = new PuzzleData();
		d18.id = 18;
		d18.width = 4;
		d18.height = 4;
		d18.rotate_size = 2;
		d18.extreme_star_scope = new int[] { 65, 85, 100 };
		d18.pattern = new int[] { 6411, 6111, 6111, 6411, 6411, 6111, 6111, 6411, 6411, 6111, 6111, 6411, 6411, 6111, 6111, 6411 };
		d18.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				251,
				111,
				251,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d18);

		PuzzleData d19 = new PuzzleData();
		d19.id = 19;
		d19.width = 4;
		d19.height = 4;
		d19.rotate_size = 2;
		d19.extreme_star_scope = new int[] { 65, 85, 100 };
		d19.pattern = new int[] { 6111, 6111, 6111, 6111, 6411, 5412, 5412, 6411, 6411, 5414, 5414, 6411, 6111, 6111, 6111, 6111 };
		d19.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				331,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d19);

		PuzzleData d20 = new PuzzleData();
		d20.id = 20;
		d20.width = 4;
		d20.height = 4;
		d20.rotate_size = 2;
		d20.extreme_star_scope = new int[] { 65, 85, 100 };
		d20.pattern = new int[] { 4113, 4112, 4213, 4212, 4114, 4111, 4214, 4211, 4313, 4312, 4413, 4412, 4314, 4311, 4414, 4411 };
		addPuzzle(d20);

		PuzzleData d21 = new PuzzleData();
		d21.id = 21;
		d21.width = 5;
		d21.height = 4;
		d21.rotate_size = 2;
		d21.extreme_star_scope = new int[] { 70, 90, 100 };
		d21.pattern = new int[] {
				6211,
				6211,
				5112,
				6211,
				6211,
				5113,
				2111,
				8111,
				2111,
				5111,
				5113,
				2111,
				8111,
				2111,
				5111,
				6211,
				6211,
				5114,
				6211,
				6211 };
		addPuzzle(d21);

		PuzzleData d22 = new PuzzleData();
		d22.id = 22;
		d22.width = 5;
		d22.height = 4;
		d22.rotate_size = 2;
		d22.extreme_star_scope = new int[] { 70, 90, 100 };
		d22.pattern = new int[] {
				4213,
				2211,
				7213,
				2211,
				4212,
				2212,
				6311,
				2212,
				6311,
				2212,
				2212,
				6311,
				2212,
				6311,
				2212,
				4214,
				2211,
				7211,
				2211,
				4211 };
		addPuzzle(d22);

		PuzzleData d23 = new PuzzleData();
		d23.id = 23;
		d23.width = 5;
		d23.height = 4;
		d23.rotate_size = 3;
		d23.extreme_star_scope = new int[] { 70, 90, 100 };
		d23.pattern = new int[] {
				6311,
				6311,
				6311,
				6311,
				6311,
				6311,
				6411,
				6411,
				6411,
				6311,
				6311,
				6411,
				6411,
				6411,
				6311,
				6311,
				6311,
				6311,
				6311,
				6311 };
		addPuzzle(d23);

		PuzzleData d24 = new PuzzleData();
		d24.id = 24;
		d24.width = 4;
		d24.height = 5;
		d24.rotate_size = 2;
		d24.shuffle_type = 3;
		d24.shuffle_percent = 50; // 30
		d24.extreme_star_scope = new int[] { 70, 90, 100 };
		d24.pattern = new int[] {
				4423,
				2411,
				2411,
				4422,
				4414,
				2411,
				2411,
				4411,
				5123,
				2111,
				2111,
				5121,
				4413,
				2411,
				2411,
				4412,
				4424,
				2411,
				2411,
				4421 };
		d24.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				241,
				111,
				111,
				111,
				111,
				241,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d24);

		PuzzleData d25 = new PuzzleData();
		d25.id = 25;
		d25.width = 5;
		d25.height = 5;
		d25.rotate_size = 2;
		d25.extreme_star_scope = new int[] { 70, 90, 100 };
		d25.pattern = new int[] {
				4313,
				4312,
				4113,
				2111,
				4112,
				4314,
				4311,
				2112,
				6411,
				2112,
				4113,
				2111,
				8111,
				2111,
				4111,
				2112,
				6411,
				2112,
				4213,
				4212,
				4114,
				2111,
				4111,
				4214,
				4211 };
		d25.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d25);

		PuzzleData d26 = new PuzzleData();
		d26.id = 26;
		d26.width = 4;
		d26.height = 4;
		d26.rotate_size = 2;
		d26.extreme_star_scope = new int[] { 65, 85, 100 };
		d26.pattern = new int[] { 5113, 5111, 5213, 5211, 5113, 5111, 5213, 5211, 5313, 5311, 5413, 5411, 5313, 5311, 5413, 5411 };
		d26.grid_objects = new int[] {
				111,
				111,
				312,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				313,
				111,
				111,
				111,
				311,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				314,
				111,
				111 };
		addPuzzle(d26);

		PuzzleData d27 = new PuzzleData();
		d27.id = 27;
		d27.width = 5;
		d27.height = 3;
		d27.rotate_size = 2;
		d27.extreme_star_scope = new int[] { 65, 85, 100 };
		d27.pattern = new int[] { 6211, 4313, 2311, 7313, 5311, 6211, 2312, 6211, 2312, 6211, 5313, 7311, 2311, 4311, 6211 };
		d27.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				232,
				231,
				111,
				111,
				111,
				231,
				111,
				111,
				232,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d27);

		PuzzleData d28 = new PuzzleData();
		d28.id = 28;
		d28.width = 5;
		d28.height = 4;
		d28.rotate_size = 2;
		d28.extreme_star_scope = new int[] { 70, 90, 100 };
		d28.pattern = new int[] {
				4413,
				2411,
				7413,
				2411,
				4412,
				4414,
				2411,
				7411,
				2411,
				4411,
				4113,
				2111,
				7113,
				2111,
				4112,
				4114,
				2111,
				7111,
				2111,
				4111 };
		d28.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d28);

		PuzzleData d29 = new PuzzleData();
		d29.id = 29;
		d29.width = 4;
		d29.height = 4;
		d29.rotate_size = 2;
		d29.shuffle_type = 3;
		d29.shuffle_percent = 50;
		d29.extreme_star_scope = new int[] { 65, 85, 100 };
		d29.pattern = new int[] { 6211, 6311, 6211, 6311, 6311, 6211, 6311, 6211, 6211, 6311, 6211, 6311, 6311, 6211, 6311, 6211 };
		d29.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				223,
				111,
				111,
				111,
				234,
				111,
				242,
				111,
				111,
				111,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d29);

		PuzzleData d30 = new PuzzleData();
		d30.id = 30;
		d30.width = 5;
		d30.height = 5;
		d30.rotate_size = 2;
		d30.extreme_star_scope = new int[] { 70, 90, 100 };
		d30.pattern = new int[] {
				6421,
				6311,
				6111,
				6311,
				6421,
				6311,
				6311,
				6111,
				6311,
				6311,
				6111,
				6111,
				6111,
				6111,
				6111,
				6311,
				6311,
				6111,
				6311,
				6311,
				6421,
				6311,
				6111,
				6311,
				6421 };
		d30.grid_objects = new int[] {
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				251,
				251,
				111,
				111,
				111,
				111,
				251,
				251,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111,
				111 };
		addPuzzle(d30);

		PuzzleData d31 = new PuzzleData();
		d31.id = 31;
		d31.width = 4;
		d31.height = 4;
		d31.rotate_size = 3;
		d31.shuffle_type = 3;
		d31.shuffle_percent = 50;
		d31.extreme_star_scope = new int[] { 65, 85, 100 };
		d31.pattern = new int[] { 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111, 2111 };
		addPuzzle(d31);

		PuzzleData d32 = new PuzzleData();
		d32.id = 32;
		d32.width = 4;
		d32.height = 4;
		d32.rotate_size = 3;
		d32.extreme_star_scope = new int[] { 65, 85, 100 };
		d32.pattern = new int[] { 4413, 7413, 7413, 4412, 7414, 8411, 8411, 7412, 7414, 8411, 8411, 7412, 4414, 7411, 7411, 4411 };
		addPuzzle(d32);

		PuzzleData d33 = new PuzzleData();
		d33.id = 33;
		d33.width = 5;
		d33.height = 4;
		d33.rotate_size = 3;
		d33.extreme_star_scope = new int[] { 70, 90, 100 };
		d33.pattern = new int[] {
				4113,
				4112,
				4113,
				4112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				2112,
				4114,
				4111,
				4114,
				4111 };
		addPuzzle(d33);

		PuzzleData d34 = new PuzzleData();
		d34.id = 34;
		d34.width = 4;
		d34.height = 4;
		d34.rotate_size = 3;
		d34.extreme_star_scope = new int[] { 65, 85, 100 };
		d34.pattern = new int[] { 4313, 2311, 2311, 4312, 2312, 4213, 4212, 2312, 2312, 4214, 4211, 2312, 4314, 2311, 2311, 4311 };
		addPuzzle(d34);

		PuzzleData d35 = new PuzzleData();
		d35.id = 35;
		d35.width = 4;
		d35.height = 4;
		d35.rotate_size = 3;
		d35.extreme_star_scope = new int[] { 65, 85, 100 };
		d35.pattern = new int[] { 4413, 4412, 4313, 4312, 4414, 8411, 8311, 4311, 4113, 8111, 8211, 4212, 4114, 4111, 4214, 4211 };
		addPuzzle(d35);

		// [4114,4112,4414,4412,4212,4114,4112,4414,4214,4212,4114,4112,4312,4214,4212,4114]

		PuzzleData d36 = new PuzzleData();
		d36.id = 36;
		d36.width = 4;
		d36.height = 4;
		d36.rotate_size = 2;
		d36.extreme_star_scope = new int[] { 65, 85, 100 };
		d36.pattern = new int[] { 4114, 4112, 4414, 4412, 4212, 4114, 4112, 4414, 4214, 4212, 4114, 4112, 4312, 4214, 4212, 4114 };
		addPuzzle(d36);

	}
}
