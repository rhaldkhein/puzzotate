package com.gamexplode.puzzotate.data;

public class PuzzleData {
	public int id;
	public int type;
	public String name;
	public int width;
	public int height;
	public int rotate_size;
	public int shuffle_type;
	public int shuffle_level;
	public int shuffle_percent;
	public int nmb;
	public int arcade_move_min;
	public int[] pattern;
	public int[] shuffle_preset;
	public int[] shuffle_automate;
	public int[] grid_objects;
	public int[] grid_objects_preset;
	public int[] extreme_star_scope;
}
