package com.gamexplode.puzzotate.data;

public class PuzzleState {
	public int id;
	public int seconds;
	public int[] puzzle;
	public int[] turnstile;
}
