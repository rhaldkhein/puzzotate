package com.gamexplode.puzzotate.data;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.IntMap.Entry;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.gamexplode.core.Config;

public class Puzzles {

	private IntMap<PuzzleData> mapPuzzles;
	private int pType;

	public Puzzles(int type) {
		mapPuzzles = new IntMap<PuzzleData>();
		pType = type;
		create();
		for (Entry<PuzzleData> entry : mapPuzzles) {
			entry.value.type = pType;
		}
	}
	
	public int getType() {
		return pType;
	}

	protected void create() {
		// Override this
	}
	
	public void addPuzzle(PuzzleData data) {
		mapPuzzles.put(data.id, data);
	}

	public IntMap<PuzzleData> getPuzzles() {
		return mapPuzzles;
	}

	public PuzzleData getPuzzle(int id) {
		return mapPuzzles.get(id);
	}

	public PuzzleData getLast() {
		return mapPuzzles.values().toArray().peek();
	}

	public PuzzleData getFirst() {
		return mapPuzzles.values().toArray().first();
	}

	public PuzzleData getNextOf(int id) {
		boolean proceed = false;
		for (Entry<PuzzleData> entry : getPuzzles()) {
			if (proceed) {
				return entry.value;
			}
			if (id == entry.value.id) {
				proceed = true;
			}
		}
		return getFirst();
	}

	public PuzzleData getPrevOf(int id) {
		PuzzleData previous = null;
		for (Entry<PuzzleData> entry : getPuzzles()) {
			if (id == entry.value.id) {
				break;
			}
			previous = entry.value;
		}
		if (previous == null) {
			return getLast();
		} else {
			return previous;
		}
	}

	public void readFiles() {

		FileHandle[] files;
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			files = Gdx.files.internal("./bin/" + Config.DIR_PUZZLES + "/").list(".json");
		} else {
			files = Gdx.files.internal(Config.DIR_PUZZLES + "/").list(".json");
		}
		for (FileHandle file : files) {
			try {
				JsonValue root = new JsonReader().parse(file.readString());
				JsonValue puzz;
				for (int i = root.size - 1; i >= 0; i--) {
					puzz = root.get(i);
					PuzzleData data = new PuzzleData();
					data.id = puzz.getInt("id");
					data.width = puzz.getInt("width");
					data.height = puzz.getInt("height");
					data.rotate_size = puzz.getInt("rsize");
					data.nmb = puzz.has("nmb") ? puzz.getInt("nmb") : 0;
					data.pattern = puzz.get("pattern").asIntArray();
					data.shuffle_type = puzz.has("shuffle_type") ? puzz.getInt("shuffle_type") : 2;
					data.shuffle_level = puzz.has("shuffle_level") ? puzz.getInt("shuffle_level") : 0;
					data.grid_objects = puzz.has("grid") ? puzz.get("grid").asIntArray() : null;
					addPuzzle(data);
				}
			} catch (Exception e) {
				Gdx.app.debug("Puzzles", "Error | " + e.getMessage());
			}
		}

	}

}
