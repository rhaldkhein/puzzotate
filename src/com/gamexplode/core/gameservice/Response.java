package com.gamexplode.core.gameservice;

import com.badlogic.gdx.utils.JsonValue;

public class Response {

	public final static int TYPE_STRING = 100;
	public final static int TYPE_JSONVALUE = 101;
	
	private int pType;
	private int pCode;
	private String pDataString;
	private JsonValue pDataJsonValue;
	private boolean pError;
	
	public Response(String data) {
		this(100, data);
	}
	
	public Response(int code, String data) {
		this.pCode = code;
		this.pDataString = data;
		pType = TYPE_STRING;
		checkError();
	}
	
	public Response(JsonValue data) {
		this(100, data);
	}
	
	public Response(int code, JsonValue data) {
		pCode = code;
		pDataJsonValue = data;
		pType =  TYPE_JSONVALUE;
		checkError();
	}
	
	private void checkError(){
		if (pCode != 100) {
			pError = true;
		}
	}

	public int code() {
		return pCode;
	}
	
	public int type() {
		return pType;
	}

	public String dataString() {
		return pDataString;
	}
	
	public JsonValue dataJsonValue() {
		return pDataJsonValue;
	}
	
	public boolean error() {
		return pError;
	}

}
