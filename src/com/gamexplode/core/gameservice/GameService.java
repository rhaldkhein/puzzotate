package com.gamexplode.core.gameservice;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.gamexplode.core.Callback;
import com.gamexplode.core.HttpClient;
import com.gamexplode.core.Resource;
import com.gamexplode.core.Settings;
import com.gamexplode.core.Utils;
import com.gamexplode.core.facebook.IFacebook;

public class GameService {

	public static final int OK = 80;
	public static final int ERROR = 90;
	public static final int ACCOUNT_REPLACE = 94;
	public static final int ACCOUNT_OLD = 100;
	public static final int ACCOUNT_NEW = 101;
	public static final int STATUS = 102;
	public static final int FRIENDS = 103;
	public static final int DONE = 104;

	private static final String prefix = "gsa_";

	private String pDomain;
	private String pProtocol;
	private Array<String> pFriends;
	private Player pMe;
	private IFacebook pFb;
	private boolean pIsDone;
	private boolean pIsInit;
	private boolean pAccess;
	private boolean pIsFetchedMe;
	private boolean pIsOpen;
	private HttpClient httpClient;
	private String pCsrfToken;

	public GameService() {
		// Nothing goes here
	}

	public void initialize(String domain, boolean secure) {
		if (!pIsInit) {
			httpClient = Resource.getHttpClient();
			pMe = new Player();
			pFriends = new Array<String>();
			Settings settings = Resource.getSettings();
			if (!settings.getString(prefix + "id").isEmpty()) {
				pMe.id = settings.getString(prefix + "id");
				pMe.first_name = settings.getString(prefix + "fn");
				pMe.last_name = settings.getString(prefix + "ln");
				pMe.picture = settings.getString(prefix + "pi");
				pMe.gender = settings.getString(prefix + "gd");
			}
			pIsOpen = false;
			pIsFetchedMe = false;
			pIsInit = true;
			pDomain = domain;
			pProtocol = secure ? "https://" : "http://";
		}
	}

	public void open(String appId, IFacebook facebook, final Callback<String> statusCallback) {
		if (pIsInit && facebook != null) {
			pIsDone = false;
			pFb = facebook;
			pFb.setStatusChangeCallback(new Callback<String>() {
				@Override
				public void apply(String status) {
					if (status == "OPENED") {
						pIsOpen = true;
						// fetchMe();
					} else {
						pIsDone = false;
					}
					statusCallback.apply(status);
				}
			});
			pFb.open();
		}
	}

	public void login(final Callback<Boolean> callback) {
		Map<String, String> parameters = new HashMap<String, String>();
		addAccess(parameters);
		httpClient.post(pProtocol + pDomain + "/player/login", parameters, new HttpResponseListener() {
			@Override
			public void handleHttpResponse(HttpResponse httpResponse) {
				String strJson = httpResponse.getResultAsString();
				JsonValue root = new JsonReader().parse(strJson);
				if (!root.getBoolean("error")) {
					if (!pAccess && root.getBoolean("access")) {
						pAccess = true;
					}
					JsonValue data = root.get("data");
					pMe.new_player = data.getBoolean("new_account");
					pCsrfToken = data.getString("csrf_token");
					callback.apply(true);
				} else {
					callback.apply(false);
				}
			}

			@Override
			public void failed(Throwable t) {
				if (t.getMessage() != null)
					callback.apply(false);
			}

			@Override
			public void cancelled() {

			}
		});
	}

	public void fetchMe(final Callback<Integer> callback) {
		// Gdx.app.debug("GameService", "Fetch Me | " + pIsDone + " | " + pIsOpen);
		if (!pIsDone && pIsOpen) {
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("access_token", pFb.getAccessToken());
			parameters.put("fields", "id,first_name,last_name,picture,gender");
			httpClient.get("https://graph.facebook.com/me", parameters, new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					// Gdx.app.debug("GameService", "Fetch Me Response | " + httpResponse.getStatus().getStatusCode());
					JsonValue root = new JsonReader().parse(httpResponse.getResultAsString());
					String fetched_id = root.getString("id");
					String saved_id = Resource.getSettings().getString(prefix + "id");
					pMe.id = fetched_id;
					pMe.first_name = root.getString("first_name");
					pMe.last_name = root.getString("last_name");
					pMe.picture = root.get("picture").get("data").getString("url");
					pMe.gender = root.getString("gender");
					pIsFetchedMe = true;
					if (!saved_id.equals(fetched_id)) {
						Resource.getSettings().setString(prefix + "id", pMe.id);
						Resource.getSettings().setString(prefix + "fn", pMe.first_name);
						Resource.getSettings().setString(prefix + "ln", pMe.last_name);
						Resource.getSettings().setString(prefix + "pi", pMe.picture);
						Resource.getSettings().setString(prefix + "gd", pMe.gender);
						if (saved_id.isEmpty()) {
							callback.apply(ACCOUNT_NEW);
						} else {
							callback.apply(ACCOUNT_REPLACE);
						}
					} else {
						callback.apply(ACCOUNT_OLD);
					}
				}

				@Override
				public void failed(Throwable t) {
					if (t.getMessage() != null && callback != null)
						callback.apply(ERROR);
				}

				@Override
				public void cancelled() {

				}
			});
		}
	}

	public void removeAccount() {
		Resource.getSettings().remove(prefix + "id");
		Resource.getSettings().remove(prefix + "fn");
		Resource.getSettings().remove(prefix + "ln");
		Resource.getSettings().remove(prefix + "pi");
		Resource.getSettings().remove(prefix + "gd");
	}

	public void fetchFriends(final Callback<Boolean> callback) {
		// Gdx.app.debug("GameService", "Fetch Friends | " + pIsDone + " | " + pIsFetchedMe);
		if (!pIsDone && pIsFetchedMe) {
			Map<String, String> parameters = new HashMap<String, String>();
			parameters.put("access_token", pFb.getAccessToken());
			parameters.put("limit", "5000");
			httpClient.get("https://graph.facebook.com/me/friends", parameters, new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					String strRes = httpResponse.getResultAsString();
					// Gdx.app.debug("GameService", "Fetch Friends Response | " + strRes);
					JsonValue root = new JsonReader().parse(strRes).get("data");
					pFriends.clear();
					for (int i = root.size - 1; i >= 0; i--) {
						pFriends.add(root.get(i).getString("id"));
					}
					pFriends.add(pMe.id);
					pIsDone = true;
					callback.apply(true);
				}

				@Override
				public void failed(Throwable t) {
					if (t.getMessage() != null)
						callback.apply(false);
				}

				@Override
				public void cancelled() {

				}
			});
		}
	}

	public void getScores(final Map<String, String> parameters, final Callback<Response> callback) {
		if (pIsDone) {
			addAccess(parameters);
			parameters.put("csrf_token", pCsrfToken);
			parameters.put(	"key",
							Utils.md5(Utils.xorString(	httpClient.getCookie(pDomain),
														Utils.getString(new int[] {
																4,
																51,
																12,
																28,
																44,
																42,
																49,
																8,
																30,
																29,
																56,
																16,
																58,
																47,
																24,
																24,
																47,
																25,
																43,
																59,
																41,
																39,
																49,
																35,
																41,
																34,
																14,
																8,
																52,
																33,
																2,
																23 }))));
			httpClient.post(pProtocol + pDomain + "/score/get", parameters, new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					String strJson = httpResponse.getResultAsString();
					// Gdx.app.debug("GameService", "Get Scores Response | " + strJson);
					JsonValue root = new JsonReader().parse(strJson);
					if (!root.getBoolean("error")) {
						if (!pAccess && root.getBoolean("access")) {
							pAccess = true;
						}
						//callback.apply(new Response(root.get("data").toString()));
						callback.apply(new Response(root.get("data")));
					} else {
						
						int errCode = root.get("data").getInt("code");
						if (errCode == 109) {
							// Authentication error
							pAccess = false;
							getScores(parameters, callback);
						} else {
							callback.apply(new Response(errCode, root.get("data")));
						}
					}
				}

				@Override
				public void failed(Throwable t) {
					if (t.getMessage() != null)
						callback.apply(new Response(200, t.getMessage()));
				}

				@Override
				public void cancelled() {

				}
			});
		}
	}

	public void getMyScores(final Callback<Response> callback) {
		// Gdx.app.debug("GameService", "Get My Scores | " + pIsFetchedMe);
		if (pIsFetchedMe) {
			Map<String, String> parameters = new HashMap<String, String>();
			addAccess(parameters);
			httpClient.post(pProtocol + pDomain + "/player/scores", parameters, new HttpResponseListener() {
				@Override
				public void handleHttpResponse(HttpResponse httpResponse) {
					
					String strJson = httpResponse.getResultAsString();
					// Gdx.app.debug("GameService", "Get My Scores Response | " + strJson);
					JsonValue root = new JsonReader().parse(strJson);
					if (!root.getBoolean("error")) {
						if (!pAccess && root.getBoolean("access")) {
							pAccess = true;
						}
						callback.apply(new Response(root.get("data")));
					} else {
						int errCode = root.get("data").getInt("code");
						if (errCode == 109) {
							// Authentication error
							pAccess = false;
							getMyScores(callback);
						} else {
							callback.apply(new Response(errCode, root.get("data")));
						}
					}
				}

				@Override
				public void failed(Throwable t) {
					if (t.getMessage() != null)
						callback.apply(new Response(200, t.getMessage()));
				}

				@Override
				public void cancelled() {

				}
			});
		}
	}

	private Map<String, String> addAccess(Map<String, String> parameters) {
		if (!pAccess) {
			parameters.put("token", pFb.getAccessToken());
		}
		return parameters;
	}

	public boolean isDone() {
		return pIsDone;
	}

	public boolean isInit() {
		return pIsInit;
	}

	public Player getMe() {
		return pMe;
	}

	public Array<String> getFriends() {
		return pFriends;
	}

	public IFacebook getFacebook() {
		return pFb;
	}

}
