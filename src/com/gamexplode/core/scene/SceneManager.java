package com.gamexplode.core.scene;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.gamexplode.core.Config;
import com.gamexplode.core.Resource;

public class SceneManager implements ApplicationListener {

	private Color backgroundColor;
	private Scene currentScene;

	public SceneManager() {
		// Set log level
		Gdx.app.setLogLevel(Config.LOG_LEVEL);
		backgroundColor = new Color(0.2f, 0.2f, 0.2f, 1);
		currentScene = null;
		// Initialize resource manager
		Resource.create();
		// Initialize http client
		Resource.getHttpClient().open();
	}

	@Override
	public void create() {

	}

	@Override
	public void resize(int width, int height) {
		if (currentScene != null)
			currentScene.getStage().getViewport().update(width, height);
	}

	@Override
	public void render() {
		
		Gdx.gl.glClearColor(backgroundColor.r, backgroundColor.g, backgroundColor.b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		if (currentScene != null) {
			currentScene.update();
			currentScene.draw();
		}
	}

	@Override
	public void pause() {
		Resource.save();
		currentScene.pause();
	}

	@Override
	public void resume() {
		currentScene.resume();
	}

	@Override
	public void dispose() {
		currentScene.dispose();
		Resource.dispose();
	}

	public void setScene(Scene scene) {
		currentScene = scene;
		currentScene.setSceneManager(this);
	}

	public Scene getCurrentScene() {
		return currentScene;
	}

	public void setBackgroundColor(Color color) {
		backgroundColor = color;
	}

}
