package com.gamexplode.core.scene;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.gamexplode.core.Config;
import com.gamexplode.core.Enum.Direction;
import com.gamexplode.core.scene.controls.Group;

public class View extends Group implements GestureListener, InputProcessor {

	private Scene scene;
	private boolean pPanning;

	public void create() {
		setSize(Config.WIDTH, Config.HEIGHT);
		setX((getStage().getWidth() / 2) - (getWidth() / 2));
	}

	public void dispose() {
		// Override this method
	}

	public void setScene(Scene scene) {
		this.scene = scene;
	}

	public Scene getScene() {
		return scene;
	}

	public void back() {
		// Override this method
	}

	public void alignMiddle( Actor actor, Actor container) {
		actor.setX((container.getWidth() / 2) - (actor.getWidth() / 2));
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		if (!pPanning) {
			if (deltaX > deltaY) {
				if (deltaX > Math.abs(deltaY)) {
					// Gdx.app.debug("tag", "Right");
					panMove(Direction.RIGHT);
				} else {
					// Gdx.app.debug("tag", "Up");
					panMove(Direction.UP);
				}
			} else {
				if (deltaY > Math.abs(deltaX)) {
					// Gdx.app.debug("tag", "Down");
					panMove(Direction.DOWN);
				} else {
					// Gdx.app.debug("tag", "Left");
					panMove(Direction.LEFT);
				}
			}
		}
		pPanning = true;
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		pPanning = false;
		return false;
	}

	public void panMove(Direction direction) {

	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		if (keycode == Keys.BACK) {
			back();
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

}
