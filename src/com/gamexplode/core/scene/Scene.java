package com.gamexplode.core.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.gamexplode.core.Config;

public class Scene implements GestureListener {

	private SceneStage stage;
	private SceneManager sceneManager;
	private InputMultiplexer input;
	private View view;
	private Image imgBackground;

	public Scene() {
		stage = null;
		// Setup multiple input first
		input = new InputMultiplexer();
	}

	public void setSceneManager(SceneManager manager) {
		// Link scene manager to scene
		sceneManager = manager;
		// Link scene manager to stage
		if (stage == null)
			throw new Error("Can't render scene, stage must be set.");
		stage.getRoot().setUserObject(manager);
		// Add input processors
		input.addProcessor(new GestureDetector(this));
		input.addProcessor(stage);
		Gdx.input.setInputProcessor(input);
	}

	public SceneManager getSceneManager() {
		return sceneManager;
	}

	public void setView(final View viewActor) {
		if (stage != null) {
			if (view != null) {
				view.dispose();
			}
			showView(viewActor);
		}
	}

	private void showView(View viewActor) {
		if (stage != null) {
			stage.clear();
			if (imgBackground != null) {
				stage.addActor(imgBackground);
			}
			stage.addActor(viewActor);
			view = viewActor;
			view.setScene(this);
			view.create();
		}
	}

	public View getView() {
		return view;
	}

	public void setBackgroundImage(Image image) {
		imgBackground = image;
	}

	public void setStage(SceneStage stage) {
		this.stage = stage;
	}

	public Stage getStage() {
		return stage;
	}

	public void update() {
		// Update the scene
		stage.act();
	}

	public void draw() {
		// Draw the scene and stage
		stage.draw();

	}

	public void pause() {
		// Override this
	}

	public void resume() {
		// Override this
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return view.touchDown(x, y, pointer, button);
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return view.tap(x, y, count, button);
	}

	@Override
	public boolean longPress(float x, float y) {
		return view.longPress(x, y);
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return view.fling(velocityX, velocityY, button);
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		return view.pan(x, y, deltaX, deltaY);
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return view.panStop(x, y, pointer, button);
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		return view.zoom(initialDistance, distance);
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return view.pinch(initialPointer1, initialPointer2, pointer1, pointer2);
	}

	public void dispose() {
		if (view != null)
			view.dispose();
		if (stage != null)
			stage.dispose();
	}

	public class SceneStage extends Stage {

		public SceneStage(Viewport viewport, Batch batch) {
			super(viewport, batch);
		}

		@Override
		public boolean keyUp(int keyCode) {
			view.keyUp(keyCode);
			if (Config.DEVEL) {
				if (keyCode == Keys.SPACE) {
					getStage().setDebugAll(false);
				}
			}
			return super.keyUp(keyCode);
		}

		@Override
		public boolean keyDown(int keyCode) {
			view.keyDown(keyCode);
			if (Config.DEVEL) {
				if (keyCode == Keys.SPACE) {
					getStage().setDebugAll(true);
				}
			}
			return super.keyDown(keyCode);
		}

		@Override
		public boolean keyTyped(char character) {
			view.keyTyped(character);
			return super.keyTyped(character);
		}

		@Override
		public boolean scrolled(int amount) {
			view.scrolled(amount);
			return super.scrolled(amount);
		}

		@Override
		public boolean touchDown(int screenX, int screenY, int pointer, int button) {
			view.touchDown(screenX, screenY, pointer, button);
			return super.touchDown(screenX, screenY, pointer, button);
		}

		@Override
		public boolean touchDragged(int screenX, int screenY, int pointer) {
			view.touchDragged(screenX, screenY, pointer);
			return super.touchDragged(screenX, screenY, pointer);
		}

		@Override
		public boolean touchUp(int screenX, int screenY, int pointer, int button) {
			view.touchUp(screenX, screenY, pointer, button);
			return super.touchUp(screenX, screenY, pointer, button);
		}

	}

}
