package com.gamexplode.core.scene.controls;

import com.badlogic.gdx.scenes.scene2d.Actor;

public class Group extends com.badlogic.gdx.scenes.scene2d.Group {

	public Group() {
		super();
	}

	public void alignMiddle() {
		setX((getParent().getWidth() / 2) - (getWidth() / 2));
	}

	public void alignMiddle(float offset) {
		alignMiddle();
		setX(getX() + (getParent().getWidth() * offset));
	}

	public void alignCenter() {
		setX((getParent().getWidth() / 2) - (getWidth() / 2));
		setY((getParent().getHeight() / 2) - (getHeight() / 2));
	}

	public void setYPercent(float percent) {
		setY(getParent().getHeight() * percent);
	}

	public void setXPercent(float percent) {
		setX(getParent().getWidth() * percent);
	}

	public float getHalfWidth() {
		return getWidth() / 2;
	}

	public float getHalfHeight() {
		return getHeight() / 2;
	}

	public void alignChildMiddle(Actor child) {
		child.setX((getWidth() / 2) - (child.getWidth() / 2));
	}
	
	public void alignChildCenter(Actor child) {
		child.setX((getWidth() / 2) - (child.getWidth() / 2));
		child.setY((getHeight() / 2) - (child.getHeight() / 2));
	}

}
