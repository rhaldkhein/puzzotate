package com.gamexplode.core.scene.controls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.gamexplode.core.Resource;

public class PanelTable extends Table {

	private Container<Actor> content;
	private float width;
	private float height;

	private Image cornerTopLeft;
	private Image cornerTopRight;
	private Image cornerBottomLeft;
	private Image cornerBottomRight;

	private Image fill1;
	private Image fill2;
	private Image fill3;
	private Image fill4;
	private Image fill5;

	public PanelTable() {
		super();
		// setDebug(true, true);
		create();
	}

	private void create() {

		Stack center = new Stack();
		content = new Container<Actor>();
		content.pad(-20);

		AtlasRegion region = Resource.getAtlasManager().getRegion("panel-1");
		AtlasRegion fill = Resource.getAtlasManager().getRegion("panel-2");

		Sprite corner1 = new Sprite(region);
		Sprite corner2 = new Sprite(region);
		Sprite corner3 = new Sprite(region);
		Sprite corner4 = new Sprite(region);

		corner2.flip(true, false);
		corner4.flip(true, true);
		corner3.flip(false, true);

		cornerTopLeft = new Image(corner1);
		cornerTopRight = new Image(corner2);
		cornerBottomLeft = new Image(corner3);
		cornerBottomRight = new Image(corner4);

		fill1 = new Image(fill);
		fill2 = new Image(fill);
		fill3 = new Image(fill);
		fill4 = new Image(fill);
		fill5 = new Image(fill);

		add(cornerTopLeft);
		add(fill1);
		add(cornerTopRight);
		row();
		add(fill2);
		center.add(fill3);
		center.add(content);
		add(center);
		add(fill4);
		row();
		add(cornerBottomLeft);
		add(fill5);
		add(cornerBottomRight);

		 setColor(new Color(0.8f, 0.8f, 0.8f, 0.75f));
		// setAlpha(0.75f);

	}

	@Override
	public void setColor(Color color) {
		cornerTopLeft.setColor(color);
		cornerTopRight.setColor(color);
		cornerBottomLeft.setColor(color);
		cornerBottomRight.setColor(color);
		fill1.setColor(color);
		fill2.setColor(color);
		fill3.setColor(color);
		fill4.setColor(color);
		fill5.setColor(color);
		// super.setColor(color);
	}

	public void setAlpha(float alpha) {
		cornerTopLeft.getColor().a = alpha;
		cornerTopRight.getColor().a = alpha;
		cornerBottomLeft.getColor().a = alpha;
		cornerBottomRight.getColor().a = alpha;
		fill1.getColor().a = alpha;
		fill2.getColor().a = alpha;
		fill3.getColor().a = alpha;
		fill4.getColor().a = alpha;
		fill5.getColor().a = alpha;
	}

	public void setActor(Actor actor) {
		content.setActor(actor);
	}

	public Container<Actor> getContainer() {
		return content;
	}

	@Override
	public void setWidth(float width) {
		// this.width = width;
		setSize(width, height);
		// super.setWidth(width);
	}

	@Override
	public void setHeight(float height) {
		// this.height = height;
		setSize(width, height);
		// super.setHeight(height);
	}

	public float getContainerWidth() {
		return width;
	}

	public float getContainerHeight() {
		return height;
	}

	@Override
	public void setSize(float width, float height) {
		this.width = width;
		this.height = height;
		getCells().get(1).width(width);
		getCells().get(3).height(height);
		getCells().get(4).width(width).height(height);
		getCells().get(5).height(height);
		getCells().get(7).width(width);
	}

}
