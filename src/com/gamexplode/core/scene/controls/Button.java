package com.gamexplode.core.scene.controls;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.gamexplode.core.Resource;

public class Button extends Group {

	public final static int BUTTON = 1;
	public final static int SWITCH = 2;

	private boolean pSwitchValue;
	private int pType;
	private Image pImgUp;
	private Image pImgDown;
	private ClickListener pListener;

	public Button(TextureRegion up) {
		this(up, null);
	}

	public Button(TextureRegion up, TextureRegion down) {
		this(up, down, BUTTON);
	}

	public Button(TextureRegion up, TextureRegion down, int type) {
		this(new Image(up), down == null ? null : new Image(down), type);
	}

	public Button(Image up) {
		this(up, null, BUTTON);
	}

	public Button(Image up, Image down, int type) {
		pImgUp = up;
		pImgDown = down;
		pType = type;
		pSwitchValue = false;
		create();
	}

	public void setSwitchValue(boolean val) {
		pSwitchValue = val;
		update();
	}

	private void update() {
		pImgUp.setVisible(pSwitchValue);
		pImgDown.setVisible(!pSwitchValue);
	}
	
	public void click() {
		pListener.clicked(new InputEvent(), this.getX(), this.getY());
	}
	
	public boolean addClickListener(ClickListener listener) {
		pListener = listener;
		return super.addListener(listener);
	}
	
	public void enableListener() {
		super.addListener(pListener);
	}
	
	public void disableListener() {
		super.removeListener(pListener);
	}

	@Override
	public void setWidth(float width) {
		super.setWidth(width);
		pImgUp.setX((getWidth() / 2) - (pImgUp.getWidth() / 2));
	}

	@Override
	public void setHeight(float height) {
		super.setHeight(height);
		pImgUp.setY((getHeight() / 2) - (pImgUp.getHeight() / 2));
	}

	private void create() {
		setWidth(pImgUp.getWidth());
		setHeight(pImgUp.getHeight());
		addActor(pImgUp);
		if (pImgDown != null) {
			addActor(pImgDown);
			pImgDown.setVisible(false);
			pImgDown.setOrigin(Align.center);
		}
		setOrigin(Align.center);
		pImgUp.setOrigin(Align.center);
		setListener();
	}
	
	private void setListener() {
		addListener(new InputListener() {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				switch (pType) {
					case SWITCH:
						if (pImgUp.isVisible())
							pImgUp.setScale(0.9f);
						if (pImgDown.isVisible())
							pImgDown.setScale(0.9f);
						break;
					default:
						if (pImgDown == null) {
							pImgUp.setScale(0.9f);
						} else {
							pImgUp.setVisible(false);
							pImgDown.setVisible(true);
						}
						break;
				}
				return true;
			}

			@Override
			public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
				Resource.getSoundManager().playSound("click");
				switch (pType) {
					case SWITCH:
						if (pImgUp.isVisible())
							pImgUp.setScale(1f);
						if (pImgDown.isVisible())
							pImgDown.setScale(1f);
						break;
					default:
						if (pImgDown == null) {
							pImgUp.setScale(1f);
						} else {
							pImgUp.setVisible(true);
							pImgDown.setVisible(false);
						}
						break;
				}
			}
		});
	}

}
