package com.gamexplode.core.scene.controls;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TiledDrawable;
import com.badlogic.gdx.utils.ObjectMap;
import com.gamexplode.core.AtlasManager;
import com.gamexplode.core.Resource;

public class Panel extends Group {

	public static String skinFolder = "panelskins";
	public static final String SKIN_DEFAULT = "default";
	public static final String SKIN_EMPTY = "empty";
	private static ObjectMap<String, TextureRegion[]> textureSkins;

	public static final int RESIZE_PRECISE = 100;
	public static final int RESIZE_GRID = 101;
	public static final int RESIZE_FILL = 102;

	private Image imgC1;
	private Image imgC2;
	private Image imgC3;
	private Image imgC4;
	private Image imgE1;
	private Image imgE2;
	private Image imgE3;
	private Image imgE4;
	private Image imgF1;
	private int pResizeType;
	private boolean pEmpty;

	private Group groupContainer;

	public Panel() {
		this(SKIN_DEFAULT);
	}

	public Panel(String skin) {
		this(textureSkins.get(skin), RESIZE_PRECISE);
	}

	public Panel(String skin, int resizeType) {
		super();
		AtlasManager atlas = Resource.getAtlasManager();
		TextureRegion[] textures = new TextureRegion[9];
		textures[0] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/c1");
		textures[1] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/c2");
		textures[2] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/c3");
		textures[3] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/c4");
		textures[4] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/e1");
		textures[5] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/e2");
		textures[6] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/e3");
		textures[7] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/e4");
		textures[8] = atlas.getRegionFromFile(skinFolder + "/" + skin + "/f1");
		setup(textures, resizeType);
	}

	public Panel(TextureRegion[] textures, int resizeType) {
		super();
		setup(textures, resizeType);
	}

	public void setup(TextureRegion[] textures, int resizeType) {
		pResizeType = resizeType;
		pEmpty = textures == null ? true : false;
		if (!pEmpty) {
			imgC1 = new Image(textures[0]);
			imgC2 = new Image(textures[1]);
			imgC3 = new Image(textures[2]);
			imgC4 = new Image(textures[3]);
			imgE1 = new Image(new TiledDrawable(textures[4]));
			imgE2 = new Image(new TiledDrawable(textures[5]));
			imgE3 = new Image(new TiledDrawable(textures[6]));
			imgE4 = new Image(new TiledDrawable(textures[7]));
			if (pResizeType == RESIZE_FILL) {
				imgF1 = new Image(textures[8]);
			} else {
				imgF1 = new Image(new TiledDrawable(textures[8]));
			}
			super.addActor(imgC1);
			super.addActor(imgC2);
			super.addActor(imgC3);
			super.addActor(imgC4);
			super.addActor(imgE1);
			super.addActor(imgE2);
			super.addActor(imgE3);
			super.addActor(imgE4);
			super.addActor(imgF1);
			setColor(new Color(0.8f, 0.8f, 0.8f, 0.75f));
		}
		groupContainer = new Group();
		super.addActor(groupContainer);
	}

	public static void addSkin(String name, TextureRegion[] textures) {
		if (textureSkins == null) {
			textureSkins = new ObjectMap<String, TextureRegion[]>();
		}
		textureSkins.put(name, textures);
	}

	@Override
	public void setColor(Color color) {
		if (!pEmpty) {
			imgC1.setColor(color);
			imgC2.setColor(color);
			imgC3.setColor(color);
			imgC4.setColor(color);
			imgE1.setColor(color);
			imgE2.setColor(color);
			imgE3.setColor(color);
			imgE4.setColor(color);
			imgF1.setColor(color);
		}
	}

	public void setAlpha(float alpha) {
		if (!pEmpty) {
			imgC1.getColor().a = alpha;
			imgC2.getColor().a = alpha;
			imgC3.getColor().a = alpha;
			imgC4.getColor().a = alpha;
			imgE1.getColor().a = alpha;
			imgE2.getColor().a = alpha;
			imgE3.getColor().a = alpha;
			imgE4.getColor().a = alpha;
			imgF1.getColor().a = alpha;
		}
	}

	@Override
	public void addActor(Actor actor) {
		groupContainer.addActor(actor);
	}

	@Override
	public void addActorAfter(Actor actorAfter, Actor actor) {
		groupContainer.addActorAfter(actorAfter, actor);
	}

	@Override
	public void addActorAt(int index, Actor actor) {
		groupContainer.addActorAt(index, actor);
	}

	@Override
	public void addActorBefore(Actor actorBefore, Actor actor) {
		groupContainer.addActorBefore(actorBefore, actor);
	}

	@Override
	public void setWidth(float width) {
		setSize(width, getHeight());
	}

	@Override
	public void setHeight(float height) {
		setSize(getWidth(), height);
	}

	@Override
	public void clear() {
		groupContainer.clear();
	}

	@Override
	public void setSize(float width, float height) {
		if (pEmpty) {
			super.setSize(width, height);
			groupContainer.setSize(width, height);
		} else {
			if (pResizeType == RESIZE_GRID) {
				// int iWidth = (int) width;
				// int iHeight = (int) height;
			} else {
				super.setSize(width, height);
				imgC1.setPosition(0, getHeight() - imgC1.getHeight());
				imgC2.setPosition(getWidth() - imgC2.getWidth(), getHeight() - imgC1.getHeight());
				imgC3.setPosition(getWidth() - imgC2.getWidth(), 0);
				imgC4.setPosition(0, 0);
				imgE1.setPosition(imgC1.getRight(), imgC1.getY());
				imgE1.setWidth(getWidth() - imgC1.getWidth() - imgC2.getWidth());
				imgE2.setPosition(imgC3.getX(), imgC3.getTop());
				imgE2.setHeight(getHeight() - imgC3.getHeight() - imgC2.getHeight());
				imgE3.setPosition(imgC4.getRight(), 0);
				imgE3.setWidth(getWidth() - imgC4.getWidth() - imgC3.getWidth());
				imgE4.setPosition(0, imgC4.getTop());
				imgE4.setHeight(getHeight() - imgC4.getHeight() - imgC1.getHeight());
				imgF1.setPosition(imgC4.getRight(), imgC4.getTop());
				imgF1.setSize(imgE3.getWidth(), imgE4.getHeight());
				groupContainer.setSize(width, height);
			}
		}
	}

}
