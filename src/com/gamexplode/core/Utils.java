package com.gamexplode.core;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntArray;

public final class Utils {

	public static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	public static String randomString(int length) {
		// String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		StringBuilder sb = new StringBuilder(length);
		for (int i = 0; i < length; i++)
			sb.append(CHARACTERS.charAt(MathUtils.random(CHARACTERS.length() - 1)));
		return sb.toString();
	}

	public static String getString(int[] indexes) {
		StringBuilder sb = new StringBuilder(indexes.length);
		for (int i = indexes.length - 1; i > -1; i--) {
			sb.append(CHARACTERS.charAt(indexes[i]));
		}
		return sb.toString();
	}

	public static String md5(String str) {
		StringBuffer hexString = new StringBuffer();
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
			byte[] hash = messageDigest.digest(str.getBytes());
			for (int i = 0; i < hash.length; i++) {
				if ((0xff & hash[i]) < 0x10) {
					hexString.append("0" + Integer.toHexString((0xFF & hash[i])));
				} else {
					hexString.append(Integer.toHexString(0xFF & hash[i]));
				}
			}
		} catch (NoSuchAlgorithmException e) {
			Gdx.app.error("Error Helper", e.getMessage());
		}
		return hexString.toString();
	}

	public static IntArray splitByDigit(int input) {
		String temp = Integer.toString(input);
		int[] arr = new int[temp.length()];
		for (int i = 0; i < temp.length(); i++) {
			arr[i] = temp.charAt(i) - '0';
		}
		return new IntArray(arr);
	}

	public static int joinIntArray(IntArray arr, String separator) {
		return Integer.parseInt(arr.toString(separator));
	}

	// public static int getIntRight(int number, int size) {
	// float flt = (float) (number * (1 / Math.pow(10, size)));
	// return MathUtils.round((float) ((flt - MathUtils.floor(flt)) * Math.pow(10, size)));
	// }

	// public static int getIntLeft(int number, int size) {
	// float flt = (float) (number * (1 / Math.pow(10, size)));
	// return MathUtils.floor((float) ((flt - MathUtils.floor(flt)) * Math.pow(10, size)));
	// }

	/*
	 * Starting from right to left
	 */
	public static int getDigit(int number, int n) {
		return getDigit(number, 10, n);
	}

	public static int getDigit(int number, int base, int n) {
		return (int) ((number / Math.pow(base, n - 1)) % base);
	}

	public static Vector2 convertIndexToVector2(int index, int width, boolean fix) {
		if (fix && index < 0) {
			return new Vector2(-1, -1);
		}
		return new Vector2(index % width, MathUtils.floor(index / width));
	}

	public static int convertVector2ToIndex(Vector2 vector, int width, boolean fix) {
		if (fix && (!(-1 < vector.x && vector.x < width) || vector.y < 0)) {
			return -1;
		}
		return (int) (vector.y * width + vector.x);
	}

	public static float getWidth(float baseWidth, float baseHeight, float height) {
		return height / baseHeight * baseWidth;
	}

	public static float getHeight(float baseWidth, float baseHeight, float width) {
		return width / baseWidth * baseHeight;
	}

	// public static void resizeActorByWidth(Actor actor, float width) {
	// float baseWidth = actor.getWidth();
	// float baseHeight = actor.getHeight();
	// actor.setWidth(width);
	// actor.setHeight(getHeight(baseWidth, baseHeight, actor.getWidth()));
	// }
	//
	// public static void resizeActorByHeight(Actor actor, float height) {
	// float baseWidth = actor.getWidth();
	// float baseHeight = actor.getHeight();
	// actor.setHeight(height);
	// actor.setWidth(getWidth(baseWidth, baseHeight, actor.getHeight()));
	// }

	public static String xorString(String subject, String key) {
		try {
			if (subject == null || key == null)
				return null;
			char[] keys = key.toCharArray();
			char[] subj = subject.toCharArray();
			int sl = subj.length;
			int kl = keys.length;
			char[] conv = new char[sl];
			for (int i = 0; i < sl; i++) {
				conv[i] = (char) (subj[i] ^ keys[i % kl]);
			}
			subj = null;
			keys = null;
			return new String(conv);
		} catch (Exception e) {
			return null;
		}
	}

}
