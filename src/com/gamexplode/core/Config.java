package com.gamexplode.core;

import com.badlogic.gdx.Application;

public final class Config {

	/**
	 * Required Configuration
	 */

	public final static String GAME_NAME = "Puzzotate";
	public final static String GAME_VERSION = "1.2.3";

	public final static int LOG_LEVEL = Application.LOG_NONE; // TODO Change for production
	public final static boolean DEVEL = false; // TODO Change for production
	public final static boolean PROD = true; // TODO Change for production

	public final static String URI_ANDROID = "https://play.google.com/store/apps/details?id=com.gamexplode.puzzotate.android";
	public final static String URI_IOS = "https://itunes.apple.com/app/id1013044803";
	public final static String URI_OTHER = "";

	public final static String SETTINGS_NAME = "com.gamexplode.puzzotate.xml";
	public final static String KEY_FILENAME = "randomkey";

	public final static String TEXTURE_FILENAME = "texture/puzzotate-v1";

	public final static float WIDTH = 720;
	public final static float HEIGHT = 1280;

	public final static String DIR_FONTS = "fonts";
	public final static String DIR_SOUNDS = "sounds";
	public final static String DIR_PUZZLES = "puzzles";

	/**
	 * Custom configuration
	 */
	
	public final static float ROTATION_DURATION = 0.07f;

	public final static String FILE_PUZZLES = "puzzles.json";
	public final static String FILE_DEMO_PUZZLES = "demo.json";
	public final static String FB_APP_ID = "447699735377359";
	public final static String SETTINGS_ARCADE = "com.gamexplode.puzzotate.arcade.xml";
	public final static String SETTINGS_EXTREME = "com.gamexplode.puzzotate.extreme.xml";

	public final static String GOOGLE_STORE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAj0GdeIXzY3jd3x9H0d0ybnhX1btjTJyz+hkMEbwP25pDPYcqgzMV6lrSaaG1ZmMoAudPw80DsUMAEB8x/O7Ir4axJ8hqN1++gjloJnUGdVbC1h6SpJ8DCTCeojHeLuIbm5pciabdje0K6xrB/WVdvBoyBv8PAJTWfb+syD0wgzfa4TlQxD5M+hZ1922PdhetzVDcQfNK26MyX0sz+axMUHYzDVtO+donk2nweT2dZQwJw3DXn4EWFfN1hOzR0G9y/RPZvcwTMy8vNHQsy0SzxXKlB4kHorKw9pYXRrmFzWLKkp5YSHlkvPrnO64hBRhU8Cwe9DOm9ZgRrTH5UD10JwIDAQAB";
	public final static String GOOGLE_PRODUCT_ID_UPGRADE_A = "com.gamexplode.puzzotate.android.upgradea";
	public final static String GOOGLE_PRODUCT_ID_UPGRADE_B = "com.gamexplode.puzzotate.android.upgradeb";
	
	public final static String APPLE_SKU = "875762237557";
	public final static String APPLE_PRODUCT_ID_UPGRADE_A = "com.gamexplode.puzzotate.ios.upgradea";
	public final static String APPLE_PRODUCT_ID_UPGRADE_B = "com.gamexplode.puzzotate.ios.upgradeb";

	public final static String GOOGLE_TEST_PURCHASED = "android.test.purchased";
	public final static String GOOGLE_TEST_CANCELED = "android.test.canceled";
	public final static String GOOGLE_TEST_REFUNDED = "android.test.refunded";
	public final static String GOOGLE_TEST_UNAVAILABLE = "android.test.item_unavailable";

}
