package com.gamexplode.core;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.gamexplode.core.facebook.IFacebook;
import com.gamexplode.core.gameservice.GameService;

public final class Resource {

	private static GameService GAMESERVICE;
	private static AtlasManager ATLAS_MANAGER;
	private static SoundManager SOUND_MANAGER;
	// private static Settings SETTINGS;
	private static IFacebook FACEBOOK;
	private static HttpClient HTTP_CLIENT;
	private static ObjectMap<String, BitmapFont> MAP_BITMAP_FONTS;
	private static ObjectMap<String, Object> MAP_INTERFACES;
	private static ObjectMap<String, Settings> MAP_SETTINGS;
	private static Logger LOG;

	public static void initialize() {
		// Interfaces container
		MAP_INTERFACES = new ObjectMap<String, Object>();
		MAP_SETTINGS = new ObjectMap<String, Settings>();
	}

	public static void create() {

		// Enable file logging on development mode
		if (Config.DEVEL) {
			// LOG = new Logger("log_" + Config.GAME_NAME.toLowerCase() + ".txt");
		}

		// Atlas
		ATLAS_MANAGER = new AtlasManager(Config.TEXTURE_FILENAME + ".pack");
		ATLAS_MANAGER.setFilter(TextureFilter.Linear, TextureFilter.Linear);

		// Fonts
		readFonts();

		// Settings
		// SETTINGS = new Settings();
		// SETTINGS.use(Config.SETTINGS_NAME);

		// Creating game service instance
		GAMESERVICE = new GameService();

		// Sounds
		SOUND_MANAGER = new SoundManager();

	}

	public static void dispose() {
		SOUND_MANAGER.dispose();
		ATLAS_MANAGER.dispose();
	}

	public static void save() {

	}

	public static void addInterface(String name, Object obj) {
		MAP_INTERFACES.put(name, obj);
	}

	public static Object getInterface(String name) {
		return MAP_INTERFACES.get(name);
	}

	public static boolean hasInterface(String name) {
		return MAP_INTERFACES.containsKey(name);
	}

	public static void setFacebook(IFacebook facebook) {
		FACEBOOK = facebook;
	}

	public static IFacebook getFacebook() {
		return FACEBOOK;
	}

	private static void readFonts() {
		// Birmap Fonts
		MAP_BITMAP_FONTS = new ObjectMap<String, BitmapFont>();
		FileHandle[] files;
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			files = Gdx.files.internal("./bin/" + Config.DIR_FONTS + "/").list(".fnt");
		} else {
			files = Gdx.files.internal(Config.DIR_FONTS + "/").list(".fnt");
		}
		for (FileHandle file : files) {
			BitmapFont font = new BitmapFont(Gdx.files.internal(file.path()));
			font.getData().setLineHeight(40f);
			font.getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
			MAP_BITMAP_FONTS.put(file.nameWithoutExtension(), font);
		}
	}

	public static BitmapFont getFont(String name) {
		BitmapFont font = MAP_BITMAP_FONTS.get(name);
		font.getData().setScale(1);
		return font;
	}

	public static LabelStyle getLabelStyle(String name) {
		LabelStyle ls = new LabelStyle();
		ls.font = Resource.getFont(name);
		return ls;
	}

	public static AtlasManager getAtlasManager() {
		return ATLAS_MANAGER;
	}

	public static Settings getSettings() {
		return getSettings(Config.SETTINGS_NAME);
	}

	public static Settings getSettings(String name) {
		if (!MAP_SETTINGS.containsKey(name)) {
			Settings settings = new Settings(name);
			MAP_SETTINGS.put(name, settings);
			return settings;
		} else {
			return MAP_SETTINGS.get(name);
		}
	}

	public static void clearSettings() {
		for (Entry<String, Settings> entry : MAP_SETTINGS) {
			entry.value.clear();
		}
	}

	public static GameService getGameService() {
		return GAMESERVICE;
	}

	public static HttpClient getHttpClient() {
		return HTTP_CLIENT == null ? HTTP_CLIENT = new HttpClient() : HTTP_CLIENT;
	}

	public static SoundManager getSoundManager() {
		return SOUND_MANAGER;
	}

	public static Logger getLogger() {
		return LOG;
	}

	public static void log(String tag, String message) {
		if (LOG != null) {
			LOG.write(tag, message);
		}
	}

}
