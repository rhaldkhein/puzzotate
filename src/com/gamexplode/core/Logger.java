package com.gamexplode.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class Logger {

	private FileHandle fileLog;

	public Logger(String filename) {
		if (Gdx.files.isExternalStorageAvailable()) {
			fileLog = Gdx.files.external(filename);
			fileLog.writeString("", false);
		}
	}

	public void write(String tag, String msg) {
		if (fileLog != null) {
			fileLog.writeString(tag + ": " + msg + " ", true);
		}
	}

}
