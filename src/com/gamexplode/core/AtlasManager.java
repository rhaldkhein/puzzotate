package com.gamexplode.core;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.ObjectMap;

public class AtlasManager implements Disposable {

	private AssetManager assets;
	private TextureAtlas atlas;
	private ObjectMap<String, AtlasRegion> cachedAtlasTextures;
	private ObjectMap<String, TextureRegion> cachedFileTextures;

	public AtlasManager(String filename) {
		assets = new AssetManager();
		assets.load(filename, TextureAtlas.class);
		assets.finishLoading();
		atlas = assets.get(filename);
		// cachedAtlasTextures = new ObjectMap<String, TextureAtlas.AtlasRegion>();
		create();
	}

	public AtlasManager(TextureAtlas atlas) {
		this.atlas = atlas;
		// cachedAtlasTextures = new ObjectMap<String, TextureAtlas.AtlasRegion>();
		create();
	}

	private void create() {
		cachedAtlasTextures = new ObjectMap<String, TextureAtlas.AtlasRegion>();
		cachedFileTextures = new ObjectMap<String, TextureRegion>();
	}

	public AtlasRegion getRegion(String name) {
		AtlasRegion region = cachedAtlasTextures.get(name);
		if (region == null) {
			region = atlas.findRegion(name);
			if (region != null)
				cachedAtlasTextures.put(name, region);
		}
		return region;
	}

	public TextureRegion getRegionFromFile(String path) {
		return getRegionFromFile(path, "png");
	}

	public TextureRegion getRegionFromFile(String path, String ext) {
		path = path + "." + ext;
		TextureRegion tr = cachedFileTextures.get(path);
		if (tr == null) {
			Texture tx = new Texture(path);
			tx.setFilter(TextureFilter.Linear, TextureFilter.Linear);
			tr = new TextureRegion(tx);
		}
		return tr;
	}

	public void setFilter(Texture.TextureFilter min, Texture.TextureFilter mag) {
		for (Texture texture : atlas.getTextures()) {
			texture.setFilter(min, mag);
		}
	}

	@Override
	public void dispose() {
		atlas.dispose();
		assets.dispose();
		cachedAtlasTextures.clear();
		cachedFileTextures.clear();
	}

}
