package com.gamexplode.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

public class SoundManager {

	private boolean pEnabled;
	private ObjectMap<String, Sound> pSounds;

	public SoundManager() {
		pSounds = new ObjectMap<String, Sound>();
		FileHandle[] files;
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			files = Gdx.files.internal("./bin/" + Config.DIR_SOUNDS + "/").list(".wav");
		} else {
			files = Gdx.files.internal(Config.DIR_SOUNDS + "/").list(".wav");
		}
		for (FileHandle file : files) {
			Sound sound = Gdx.audio.newSound(Gdx.files.internal(file.path()));
			pSounds.put(file.nameWithoutExtension(), sound);
		}
		syncSettings();
	}

	private void syncSettings() {
		Settings settings = Resource.getSettings();
		if (!settings.contains("sound")) {
			settings.setBoolean("sound", true);
		}
		pEnabled = settings.getBoolean("sound");
	}

	public Sound getSound(String name) {
		return pSounds.get(name);
	}

	public void playSound(String name) {
		playSound(name, 1);
	}

	public void playSound(String name, float pitch) {
		if (pEnabled) {
			if (pitch == 1) {
				pSounds.get(name).play();
			} else {
				Sound sound = pSounds.get(name);
				sound.setPitch(sound.play(), pitch);
			}
		}
	}

	public boolean isEnabled() {
		return pEnabled;
	}

	public void setEnabled(boolean enable) {
		Resource.getSettings().setBoolean("sound", enable);
		syncSettings();
	}

	public void dispose() {
		for (Entry<String, Sound> entry : pSounds) {
			entry.value.dispose();
		}
	}

	public void toggleEnabled() {
		setEnabled(!isEnabled());
	}

}
