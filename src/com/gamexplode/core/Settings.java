package com.gamexplode.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Base64Coder;

public class Settings {

	private final static int KEY_LENGTH = 32;
	private boolean pSecure;
	private Preferences pSettings;
	private String pKey;

	public Settings(String name) {
		pSettings = Gdx.app.getPreferences(name);
		// Set default key if random file is not available
		// Check if we can secure data by getting the random generated key
		pSecure = Gdx.files.isLocalStorageAvailable();
		// Read random file
		pKey = readRandomFile();
	}

	public void save() {
		pSettings.flush();
	}

	private String writeRandomFile() {
		if (pSecure) {
			try {
				String key = Utils.randomString(KEY_LENGTH);
				FileHandle file = Gdx.files.local(Config.KEY_FILENAME);
				file.writeString(key, false);
				return key;
			} catch (Exception e) {
				return getDefaultKey();
			}
		}
		return getDefaultKey();
	}

	private String readRandomFile() {
		String defaultKey = getDefaultKey();
		if (pSecure) {
			FileHandle file = Gdx.files.local(Config.KEY_FILENAME);
			String fileKey = file.exists() ? file.readString() : writeRandomFile();
			return Utils.md5(Utils.xorString(defaultKey, fileKey));
		}
		return defaultKey;
	}

	private void setData(String name, String value) {
		String key = Utils.md5(Utils.xorString(pKey, name));
		if (value.length() < key.length()) {
			// Pad value
			value = extendVal(value, Utils.randomString(key.length()));
		} else if (value.length() > key.length()) {
			// Pad key
			key = extendKey(key, value.length());
		} else {
			if (value.contains("%")) {
				value = value.replace("%", " ");
			}
		}
		if (value.length() != key.length()) {
			throw new Error("String value and key must be in same length.");
		}
		pSettings.putString(name, Base64Coder.encodeString(Utils.xorString(value, key)));
		pSettings.flush();
	}

	private String getData(String name) {
		try {
			try {
				String xor = Base64Coder.decodeString(pSettings.getString(name));
				String key = Utils.md5(Utils.xorString(pKey, name));
				if (xor.length() > key.length()) {
					// Key was extended and is not the same
					return Utils.xorString(xor, extendKey(key, xor.length()));
				} else {
					// Value (maybe with pad) and key is already the same length
					String[] arr = Utils.xorString(xor, key).split("%");
					return arr[0];
				}

			} catch (Exception e) {
				throw new Error("Invalid settings value.");
			}
		} catch (Exception e) {
			throw new Error("Invalid settings value.");
		}
	}

	private String extendVal(String str, String pad) {
		char[] conv;
		int sl = str.length();
		int pl = pad.length();
		if (sl < pl) {
			char[] sa = str.toCharArray();
			char[] pa = pad.toCharArray();
			conv = new char[pl];
			for (int i = 0; i < pl; i++) {
				if (i < sl) {
					conv[i] = sa[i];
				} else if (i == sl) {
					conv[i] = '%';
				} else {
					conv[i] = pa[i];
				}
			}
			return new String(conv);
		}
		return str;
	}

	private String extendKey(String key, int count) {
		char[] conv = new char[count];
		char[] sa = key.toCharArray();
		for (int a = count / key.length(), b = 0, c = 0; a >= 0; a--) {
			sa[0] = (char) (122 - a);
			char[] na = Utils.md5(new String(sa)).toCharArray();
			for (b = 0; c < count && b < na.length; b++) {
				conv[c] = na[b];
				c++;
			}
		}
		return new String(conv);
	}

	public void clear() {
		pSettings.clear();
		pSettings.flush();
	}

	public boolean contains(String key) {
		return pSettings.contains(key);
	}

	public void remove(String key) {
		pSettings.remove(key);
		pSettings.flush();
	}

	// Boolean
	public void setBoolean(String key, boolean value) {
		setData(key, value ? "true" : "false");
	}

	public boolean getBoolean(String key) {
		if (!pSettings.contains(key)) {
			// setBoolean(key, false);
			return false;
		}
		String value = getData(key);
		boolean retValue = false;
		if (value != null)
			retValue = value.equals("true") ? true : false;
		return retValue;
	}

	// Float
	public void setFloat(String key, float value) {
		setData(key, Float.toString(value));
	}

	public float getFloat(String key) {
		if (!pSettings.contains(key)) {
			// setFloat(key, 0);
			return 0;
		}
		String value = getData(key);
		float retValue = 0;
		if (value != null)
			retValue = Float.parseFloat(value);
		return retValue;
	}

	// Integer
	public void setInteger(String key, int value) {
		setData(key, Integer.toString(value));
	}

	public int getInteger(String key) {
		if (!pSettings.contains(key)) {
			// setInteger(key, 0);
			return 0;
		}
		String value = getData(key);
		int retValue = 0;
		if (value != null)
			retValue = Integer.parseInt(value);
		return retValue;
	}

	// Long
	public void setLong(String key, long value) {
		setData(key, Long.toString(value));
	}

	public long getLong(String key) {
		if (!pSettings.contains(key)) {
			// setLong(key, 0);
			return 0;
		}
		String value = getData(key);
		long retValue = 0;
		if (value != null)
			retValue = Long.parseLong(value);
		return retValue;
	}

	// String
	public void setString(String key, String value) {
		setData(key, value);
	}

	public String getString(String key) {
		if (!pSettings.contains(key)) {
			// setString(key, "");
			return "";
		}
		return getData(key);
	}

	private String getDefaultKey() {
		return Utils.getString(new int[] {
				60,
				8,
				54,
				5,
				48,
				54,
				14,
				28,
				8,
				34,
				15,
				12,
				58,
				12,
				19,
				10,
				5,
				38,
				30,
				41,
				44,
				24,
				55,
				20,
				0,
				51,
				7,
				36,
				50,
				55,
				30,
				29 });
	}

}
