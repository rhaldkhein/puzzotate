package com.gamexplode.core;

import java.util.Date;

public interface IAdMob extends IBase {
	public abstract void setGender(String gender);
	public abstract void setBirthday(Date birthday);
	public abstract void showInterstitial();
	public abstract void showBanner();
	public abstract void loadAds();
	public abstract void removeAds();
}
